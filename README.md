Spanksoccer 1.1 (2012) - Mario Leone, Davide Compagnin

Il simulatore è stato sviluppato in linguaggio Java per la parte
di visualizzazione e linguaggio Ada per la parte core.

Correzioni introdotte rispetto alla versione 1.0
- Corretta l'utilizzo della variabile Clock che nasconde l'omonima primitiva del 
linguaggio
- Corretta la scala dei valori di velocità dei giocatori



Compilazione ed esecuzione in ambiente GNU/LINUX

Per la compilazione e l'esecuzione del progetto sono necessari i seguenti
requisiti:

 - OpenJDK 6.0 o superiore;
 - PolyORB 2011;
 - XMLAda 4.1;
 - Compilatore gnat 2011.

Prima di compilare verificare il path in cui sono stati installati PolyORB e 
XMLAda nel file "soccer.gpr"

with "<PATH PolyORB>";
with "<PATH XMLAda>";

E' fornito uno script automatizzato per la compilazione dell'intero progetto.

$ chmod +x build_all.sh
$ ./build_all.sh

Una volta che la fase di compilazione ha avuto successo eseguire lo script di
avvio.

chmod +x start_all.sh
./start_all.sh

Per ripulire dalla compilazione.

chmod +x clean_all.sh
./clean_all.sh
