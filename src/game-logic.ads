--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Strings.Unbounded;

-- Questo package definisce tipi e costanti globali utili ai fini della logica
-- applicativa, separa quindi il piano della logica da quello dell'architettura
-- concorrente. Molti dei tipi e delle costanti in questo package devono essere
-- visibili sia dall'intermediario che dai giocatori, veicolano l'informazione
-- tra le due parti.
package Game.Logic is

   -- TIPI GLOBALI -------------------------------------------------------------

   -- Definizione del tipo ruolo del giocatore
   type Role_T is (Goalkeeper, Defender, Midfielder, Forward);

   -- Definizione del tipo team
   type Team_T is (Team1, Team2);

   -- Definizione del tipo schema di gioco, per informazioni piu' precise si
   -- veda la funzione Player_For_Role in Game.Logic.Mediator
   type Scheme_T is (Stdr, Defn, Offn);

   -- Definizione del tipo che indica lo stato corrente del gioco
   type Game_Status_Type_T is
     (Normal, Reposition_For_Foul, Reposition_For_Goal, Reposition_For_Out);

   -- Definizione del tipo posizione (X,Y).
   -- Si noti che, in riferimento al pannello di gioco, l'origine e' posta
   -- nell'angolo in alto a sinistra
   type Position_T is
      record
         X, Y: Integer := 0;
      end record;

   -- Definizione del tipo spostamento.
   -- Un valore di tipo spostamento e' assoluto rispetto agli assi x e y,
   -- quindi se sommato ad un valore posizione, si ottiene un valore posizione
   type Displacement_T is
      record
         X, Y: Integer := 0;
      end record;

   -- Definizione del tipo compasso.
   -- Indica la distanza massima percorribile da un giocatore in ogni direzione
   -- dalla propria posizione e dato il raggio d'azione
   type Compass_T is
      record
         N, S, E ,W : Integer := 0;
      end record;

   -- Tipo array che definisce un insieme di posizioni target. E' utilizzato
   -- in diversi contesti, ma principalmente per il passaggio della palla
   type Targets_T is array (1 .. 3) of Position_T;

   -- Definizione dello stato di gioco come visto da un giocatore.
   -- Questo oggetto veicola l'informazione necessaria al giocatore a
   -- calcolare la propria mossa secondo la logica fornita in questa
   -- simulazione. E' calcolato ad ogni turno del giocatore perci� genera un
   -- sensibile overhead ma permette il disaccoppiamento tra i giocatori
   -- e lo stato globale.
   type Game_Status_T is
      record
         -- Il proprio Id
         My_Id: Player_ID_T;

         -- Sono in possesso della palla
         Ball_Is_My : Boolean;

         -- Posizione della palla
         Ball_Position : Position_T;

         -- Posessore della palla, 0 altrimenti
         Ball_Possesor : Natural;

         -- Vale True se sono il giocatore della mia squadra piu' vicino alla
         -- palla, False altrimenti
         Nearest_To_Ball : Boolean;

         -- La mia posizione attuale
         My_Position : Position_T;

         -- La mia posizione di default
         My_Init_Position : Position_T;

         -- Target all'interno del raggio di visibilita'
         Near_Targets : Targets_T;
      end record;

   -- Definizione del tipo wrapper giocatore per il tipo definito in IDL
   -- Utilizzato per collezionare informazioni rilevanti sui giocatori e
   -- distribuirle ai pannelli grafici.
   type Player_Wrapper_T is
      record
         ID : Positive;
         Name : Ada.Strings.Unbounded.Unbounded_String;
         Energy : Natural;
         Foul : Natural;
         Distance : Float;
         Position: Position_T;
      end record;

   -- Array di tipo Player_Wrapper_T
   type Player_Sequence_Wrapper_T is
     array (Positive range <>) of Player_Wrapper_T;

   -- COSTANTI GLOBALI ---------------------------------------------------------

   -- Dimensione del campo di gioco rispetto alll'asse X
   Board_Size_X : constant Natural := 800;

   -- Dimensione del campo di gioco rispetto alll'asse Y
   Board_Size_Y : constant Natural := 460;

   -- Direzione di avanzamento dell'asse X rispetto al Team1
   Direction_Team1 : constant Integer := 1;

   -- Direzione di avanzamento dell'asse X rispetto al Team2
   Direction_Team2 : constant Integer := -1;

   -- Posizione delle porte
   Goal_Positions : constant array (Team_T) of Position_T :=
     (Team1 => (Board_Size_X,Board_Size_Y/2), Team2 => (0,Board_Size_Y/2));

   -- Dimensione della porta
   Goal_Size : Positive := 60;

   -- Scostamento massimo dei giocatori rispetto alla loro posizione iniziale.
   -- Permette alla squadra di avanzare in fase di attacco e rispetto alla
   -- posizione della palla. I giocatori con raggio d'azione piccolo potrebbero
   -- non riuscire a tirare in porta.
   Max_Offset_Attack_Team : constant Natural := 60;

   -- Indica un intorno del giocatore entro il quale ha senso validare alcune
   -- mosse, come ad esempio la Take_Ball o la Foul.
   Threshold : constant Positive := 2;

   -- Valore che indica la distanza visibile all'arbitro. Determina se l'arbitro
   -- vedra' o meno i falli
   Foul_Threshold : constant Positive := 80;

   -- Indica una soglia entro la quale un giocatore puo' essere considerato
   -- riposizionato in posizione iniziale.
   Reposition_Threshold : constant Positive := 40;

   -- Soglia entro la quale un giocatore a bordo campo rimette la palla in gioco.
   -- Utilizzata anche per limitare la direzione del tiro rispetto all'asse y.
   -- Deve essere maggiore di threshold.
   Kick_Y_Direction_Bound : constant Positive := 20;

   -- Tempo di sospensione dell'arbitro
   Referee_Sleep : constant Positive := 4;

   -- Velocita' massima dell'arbitro
   Referee_Velocity : constant Natural := 32;

   -- Distanza dell'arbitro rispetto alla palla. Determina se l'arbitro
   -- vedra' o meno i falli
   Referee_To_Ball_Offset : constant Positive := 36;

   -- Distanza euclidea tra due posizioni (Float)
   function "-"(A: Position_T; B: Position_T) return Float;

   -- Relazione di equivalenza
   function "="(A: Position_T; B: Position_T) return Boolean;

   -- Distanza tra due posizioni in termini di displacement
   function "-"(A: Position_T; B: Position_T) return Displacement_T;

   -- Crea un oggetto di tipo Game_Status_T
   function Create_Status (My_Id: Player_ID_T;
                           Ball_Is_My : Boolean;
                           Ball_Position : Position_T;
                           Ball_Possesor : Natural;
                           Nearest_To_Ball : Boolean;
                           My_Position : Position_T;
                           My_Init_Position : Position_T;
                           Near_Targets : Targets_T) return Game_Status_T;

   -- Fattore di direzione, serve per calcolare alcune mosse in base al Team.
   -- Per convenzione il Team1 occupa la meta' di campo sinistra, mentre il
   -- Team2 la destra.
   function Forward_Factor(Team : Team_T) return Integer;

   -- Calcola la massima distanza raggiungibile data la velocita'
   function Velocity_To_Coordinate (Velocity : Natural) return Natural;

   -- Converte una distanza di tipo Displacement_T
   function Displacement_To_Distance(Displacement : Displacement_T) return Float;

   -- Data una posizione da raggiungere e quella attuale, calcola il
   -- displacement relativo per raggiungerla, limitato rispetto alla velocita'
   function Follow_Target(Target_position : Position_T;
                          Current_position : Position_T;
                          Velocity: Positive) return Displacement_T;

   -- Calcola un compass (+3,-12,+5,-4) che indica in base alla mia posizione
   -- di quanto posso muovermi per non uscire dalla mia area di influenza.
   function Relative_Compass(Initial_position : Position_T;
                             Actual_position: Position_T;
                             Radius: Positive) return Compass_T;

   -- Indica se un oggetto e' interno all'area sottesa dal parametro Radius
   -- con origine My_Position
   function Is_Visible(My_Position : Position_T;
                       Object_Pos : Position_T;
                       Radius : Positive) return Boolean;

   -- Massimo e minimo tra due Integer
   function Maximum (A, B : Integer) return Integer;
   function Minimum (A, B : Integer) return Integer;

   -- Funzione generatrice di numeri casuali interi con bound (1..MaxValue)
   function Generate_Number (MaxValue : Integer) return Integer;

end Game.Logic;
