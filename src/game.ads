--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

-- Questo package contiene la definizione di tipi e variabili globali

package Game is


   Players : constant Positive := 22;

   -- Permette di assegnare un id univoco ad ogni Player
   type Player_ID_T is range 1 .. Players;

   -- Valore con il quale viene diviso il tempo di sospensione di ogni task
   -- se incrementato porta ad un aumento della velocita' di gioco
   Sleep_Factor : Positive := 20;

   -- Valore che rappresenta l'ampiezza di un iperperiodo in termini di
   -- time-unit, corrisponde all'LCM dei tempi di sospensione di tutti i task
   -- ed � calcolato durante la fase di start del match
   Hyperperiod_Const : Positive := 12;

   -- Valore che indica la durata di un match in termini di time-unit,
   -- inizializzato in fase di start
   Match_Duration : Positive := 90*Hyperperiod_Const;

   -- Calcola il massimo comune divisore tra due interi (ricorsiva)
   function GCD(a, b : Integer) return Integer;

   -- Calcola il minimo comune multiplo tra due interi (usa GCD)
   function LCM(a, b : Integer) return Integer;

end Game;
