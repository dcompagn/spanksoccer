--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game.Clock;
with Game.Logic.Features;
with Ada.Text_IO;
with Ada.Strings.Unbounded;

package body Game.Logic.Mediator is

   function Create(Players_Number : Positive; Players_Feature : Players_Feature_T; Game_Scheme : Scheme_T) return Mediator_T is
      Players_Counter : Players_Counter_T := (Others => 0);
   begin
      Players_Counter(Goalkeeper) := Player_For_Role(Players_Number, Goalkeeper, Game_Scheme);
      Players_Counter(Defender) := Player_For_Role(Players_Number, Defender, Game_Scheme);
      Players_Counter(Midfielder) := Player_For_Role(Players_Number, Midfielder, Game_Scheme);
      Players_Counter(Forward) := Player_For_Role(Players_Number, Forward, Game_Scheme);

      return (Release        => False,
              Game_Mode      => On,
              Attack_Team => Team1,
              Game_Status_Type => Normal,
              Start_Time => Ada.Calendar.Clock,
              Current_Clock  => Game.Clock.Create(1,Hyperperiod_Const),
              Referee_Position => (0,0),
              Ball_Status => Create_Ball_Status,
              Players_Number => Players_Number,
              Player_Activation_Counter => 0,
              ID_Counter => Player_ID_T'First,
              Team1_Players_Counter => Players_Counter,
              Team2_Players_Counter => Players_Counter,
              Game_Scheme => Game_Scheme,
              Active_Task => 0,
              Target_Player => Player_ID_T'First,
              Target_Position => (0,0),
              Players_Active => (Others => False),
              Players_Feature => Players_Feature,
              Players_Position => ( Others => (0,0) ), -- function init formatione
              Players_Init_Position => ( Others => (0,0) ), -- function init formazione
              Players_Ref_Position => ( Others => (0,0) ),
              Start_Signal   => False,
              Deferred_Action => Action_Queue.Empty_List,
              Deferred_Replacement => Action_Queue.Empty_List,
              Stat => Create_Stat);
   end Create;

   procedure Init(Mediator : in out Mediator_T) is
      Team : Team_T;
      Role : Role_T;
      Count : Natural := 0;
   begin
      -- Inizializza il match
      -- Per ogni giocatore e' segnato se dovra' partecipare al match o meno
      -- l'array di bool permettera' di assegnare ad ogni task un ID univoco.
      -- Per convenzione il Team1 occupa gli ID dispari, mentre il Team2 i pari.
      for P in Player_ID_T'Range loop
         Team := Get_Team(Mediator.Players_Feature(P));
         Role := Get_Role(Mediator.Players_Feature(P));
         if Team = Team1 then
            if Mediator.Team1_Players_Counter(Role) > 0 then
               -- Attivo il player
               Mediator.Players_Active(P) := True;
               -- Ricordo il numero di attivazioni per ruolo
               Mediator.Team1_Players_Counter(Role) :=
                 Mediator.Team1_Players_Counter(Role) - 1;
               Count := Count + 1;
            end if;
         else
            if Mediator.Team2_Players_Counter(Role) > 0 then
               Mediator.Players_Active(P) := True;
               Mediator.Team2_Players_Counter(Role) :=
                 Mediator.Team2_Players_Counter(Role) - 1;
               Count := Count + 1;
            end if;
         end if;

         -- Inizializzo il valore di energia per le statistiche
         Mediator.Stat.Players_Stat(P).Energy :=
           Get_Energy(Mediator.Players_Feature(P));

      end loop;

      -- Rilevo eventuali errori sul numero dei giocatori
      if Count /= Mediator.Players_Number then
         Ada.Text_IO.Put_Line
           ("ERROR: Features number don't match to required player number");
      end if;
   end Init;

   function M_LCM(Mediator : Mediator_T) return Integer is
      Temp_LCM : Integer := Referee_Sleep;
   begin
      for P in Player_ID_T'Range loop
         if Mediator.Players_Active(P) then
            Temp_LCM := Game.LCM(Temp_LCM,
                                 Feature_To_Sleep(Mediator.Players_Feature(P)));
         end if;
      end loop;
      return Temp_LCM;
   end M_LCM;

   procedure Get_Player_ID (Mediator : in out Mediator_T;
                            ID : out Player_ID_T) is
   begin
      -- Ottengo un nuovo ID (attivo) ricordando ad ogni richiesta
      -- l'ultimo assegnato
      ID := Mediator.ID_Counter;
      if ID < Player_ID_T'Last then
         while not Mediator.Players_Active(ID) and ID < Player_ID_T'Last loop
            ID := ID + 1;
         end loop;
         if ID < Player_ID_T'Last then
            Mediator.ID_Counter := ID + 1;
         end if;
      end if;
   end Get_Player_ID;


   function Validate(Mediator : Mediator_T; Action : Action_T) return Boolean is
      ID : Player_ID_T := Get_Id(Action);
      Action_Type : Action_Type_T := Get_Action_Type(Action);
      Displacement : Displacement_T;
      Position : Position_T;
      Target_ID : Natural;
   begin
      case Action_Type is

      when Move =>
         -- Controllo che la posizione della mossa sia valida all'interno
         -- del campo da gioco
         Displacement := Get_Displacement(Action);
         Position.X := Mediator.Players_Position(ID).X + Displacement.X;
         Position.Y := Mediator.Players_Position(ID).Y + Displacement.Y;
         if (Position.X < 0 or Position.X > Board_Size_X)
           and (Position.Y < 0 or Position.Y > Board_Size_Y) then
            return False;
         end if;

      when Take_Ball =>
         -- Verifico che il giocatore sia in prossimita' della palla
         -- e che la palla sia libera
         if Mediator.Ball_Status.Player_With_Ball /= 0
           or not Is_Visible(Mediator.Players_Position(ID),
                             Mediator.Ball_Status.Ball_Position,
                             Threshold)
         then
            return False;
         end if;

      when Tackle =>
         -- Verifico che il target del tackle sia in prossimita' del giocatore
         Target_ID := Get_Target_Id(Action);
         if Target_ID = 0
           and not Is_Visible(Mediator.Players_Position(ID),
                              Mediator.Ball_Status.Ball_Position,
                              Threshold)
         then
            return False;
         end if;
         if Target_ID /= 0
           and then not Is_Visible(Mediator.Players_Position(ID),
                                   Mediator.Players_Position(Player_ID_T(Target_ID)),
                                   Threshold)
         then
            return False;
         end if;

         when Foul =>
            -- Verifico che il target del fallo sia in prossimita' del giocatore
            Target_ID := Get_Target_Id(Action);
            if Target_ID = 0 then
               return False;
            end if;
            if Target_ID /= 0
              and then not Is_Visible(Mediator.Players_Position(ID),
                                      Mediator.Players_Position(Player_ID_T(Target_ID)),
                                      Threshold)
            then
               return False;
            end if;

         when Kick =>
            -- Verifico che il giocatore abbia la palla
            return Natural(ID) = Mediator.Ball_Status.Player_With_Ball;
         when Others =>
            null;
      end case;

      return True;
   end;

   procedure Perform_Action(Mediator : in out Mediator_T; Action : in Action_T) is
      ID : Player_ID_T := Get_Id(Action);
      Action_Type : Action_Type_T := Get_Action_Type(Action);
      Displacement : Displacement_T;
      Distance : Float;
      Max_Distance : Float;
      Target_ID : Natural;
   begin
      case Action_Type is
         when Move =>
            -- Cambia la posizione del giocatore verificando che il movimento
            -- non sia ostacolato, in tal caso la posizione diventa quella
            -- immediatamente precedente al punto di scontro (nella stessa direzione)
            Displacement := Get_Displacement(Action);
            if Mediator.Game_Status_Type = Normal then
               Displacement := Player_Overlap_Detection(Mediator, ID, Displacement);
            end if;
            Distance := Displacement_To_Distance(Displacement);
            Max_Distance := Float(Velocity_To_Coordinate(
              Get_Velocity(Mediator.Players_Feature(ID))));
            -- Calcolo l'energia spesa per il movimento
            if Mediator.Game_Mode = Off then
               -- Durante il riposizionamento il giocatore si riposa
               Mediator.Players_Position(ID).X :=
                 Mediator.Players_Position(ID).X + Displacement.X;
               Mediator.Players_Position(ID).Y :=
                 Mediator.Players_Position(ID).Y + Displacement.Y;
               Inc_Player_Energy(Mediator,ID);
               Add_Player_Distance(Mediator,ID,Distance);
            else
               if (Distance > 0.66 * Max_Distance) then
                  -- Uno spostamento maggiore del 66% della velocita' consuma
                  -- un punto energia
                  if not Is_Empty_Player_Energy(Mediator,ID) then
                     Mediator.Players_Position(ID).X :=
                       Mediator.Players_Position(ID).X + Displacement.X;
                     Mediator.Players_Position(ID).Y :=
                       Mediator.Players_Position(ID).Y + Displacement.Y;
                     Dec_Player_Energy(Mediator,ID);
                     Add_Player_Distance(Mediator,ID,Distance);
                  else
                     -- Se l'enrgia e' nulla il giocatore riposa un turno
                     Inc_Player_Energy(Mediator,ID);
                  end if;
               else
                  -- Uno spostamento minore incrementa l'energia
                  Mediator.Players_Position(ID).X :=
                    Mediator.Players_Position(ID).X + Displacement.X;
                  Mediator.Players_Position(ID).Y :=
                    Mediator.Players_Position(ID).Y + Displacement.Y;
                  Add_Player_Distance(Mediator,ID,Distance);
                  Inc_Player_Energy(Mediator,ID);
               end if;
            end if;
            if Have_Ball(Mediator, ID) then
               -- Sposto la palla
               Mediator.Ball_Status.Ball_Position :=
                 Mediator.Players_Position(ID);
            end if;
         when Kick =>

            -- Rilascio la palla aggiornando lo stato
            Mediator.Ball_Status.Ball_Target := Get_Target_Pos(Action);
            Mediator.Ball_Status.Energy := Get_Strenght(Action);
            Release_Ball(Mediator);

         when Take_Ball =>

            -- Prendo possesso della palla
            Take_Ball(Mediator,ID);

         when Tackle =>
            Target_ID := Get_Target_Id(Action);

            -- Tackle a palla libera equivale a Take_Ball
            -- il controllo � effettuato nella Validate
            if Target_ID = 0 then
               Take_Ball(Mediator,ID);
            else

               -- Calcolo le probabilita' di successo del tackle utilizzando il
               -- valore di tackle dei due giocatori coinvolti
               declare
                  Success_Prob : Float;
                  Tackle1: Positive :=
                    Get_Tackle(Mediator.Players_Feature(ID));
                  Tackle2: Positive :=
                    Get_Tackle(Mediator.Players_Feature(Player_ID_T(Target_ID)));
               begin
                  if Tackle1 > Tackle2 then
                     Success_Prob :=
                       ((1.0 - Float(Tackle2) / Float(Tackle1)) / 2.0) * 100.0;
                     if Generate_Number(100) < 50 + Natural(Success_Prob) then
                        Take_Ball(Mediator,ID);
                     end if;
                  else
                     Success_Prob :=
                       ((1.0 - Float(Tackle1) / Float(Tackle2)) / 2.0) * 100.0;
                     if Generate_Number(100) > 50 + Natural(Success_Prob) then
                        Take_Ball(Mediator,ID);
                     end if;
                  end if;
               end;

            end if;
         when Foul =>

            -- Calcolo le probabilita' di successo del fallo utilizzando il
            -- valore di tackle dei due giocatori coinvolti
            Target_ID := Get_Target_Id(Action);
            declare
               Success_Prob : Float;
               Tackle1: Positive :=
                 Get_Tackle(Mediator.Players_Feature(ID));
               Tackle2: Positive :=
                 Get_Tackle(Mediator.Players_Feature(Player_ID_T(Target_ID)));
            begin
               if Tackle1 > Tackle2 then
                  Success_Prob :=
                    ((1.0 - Float(Tackle2) / Float(Tackle1)) / 2.0) * 100.0;
                  if Generate_Number(100) < 50 + Natural(Success_Prob) then
                     Insert_To_Deferred_Action(Mediator, Action);
                  end if;
               else
                  Success_Prob :=
                    ((1.0 - Float(Tackle1) / Float(Tackle2)) / 2.0) * 100.0;
                  if Generate_Number(100) > 50 + Natural(Success_Prob) then
                     Insert_To_Deferred_Action(Mediator, Action);
                  end if;
               end if;
            end;
         when Replace_Player =>

            -- Inserisco l'azione sostituzione in coda sullo stato
            Insert_To_Deferred_Action(Mediator, Action);

      end case;
   end Perform_Action;

   procedure Insert_To_Deferred_Replacement(Mediator : in out Mediator_T;
                                            Action : in Action_T) is
   begin
      Mediator.Deferred_Replacement.Append(Action);
   end Insert_To_Deferred_Replacement;

   procedure Insert_To_Deferred_Action(Mediator : in out Mediator_T;
                                       Action : in Action_T) is
   begin
      Mediator.Deferred_Action.Append(Action);
   end Insert_To_Deferred_Action;

   procedure Change_Mode_To_On (Mediator : in out Mediator_T) is
   begin
      Mediator.Game_Status_Type := Normal;
      Mediator.Game_Mode := On;
      Mediator.Release := True;
   end Change_Mode_To_On;

   procedure Change_Mode_To_Off (Mediator : in out Mediator_T) is
   begin
      Mediator.Game_Mode := Off;
      Mediator.Release := False;
   end Change_Mode_To_Off;

   procedure Take_Ball(Mediator     : in out Mediator_T;
                       Player_Id    : in Player_ID_T) is
   begin
      Mediator.Attack_Team := Get_Team(Mediator.Players_Feature(Player_Id));
      Mediator.Ball_Status.Ball_Position := Mediator.Players_Position(Player_Id);
      Mediator.Ball_Status.Ball_Release := False;
      Mediator.Ball_Status.Player_With_Ball := Standard.Integer(Player_Id);
   end Take_Ball;

   procedure Release_Ball(Mediator  : in out Mediator_T) is
   begin
      Mediator.Ball_Status.Ball_Release := True;
      Mediator.Ball_Status.Player_With_Ball := 0;
      Move_Ball(Mediator, True);
   end Release_Ball;

   -- Inizializza lo stato della palla e la posiziona nella posizione desiderata
   procedure Place_Ball(Mediator : in out Mediator_T; Position : in Position_T) is
   begin
      Mediator.Ball_Status := Create_Ball_Status;
      Mediator.Ball_Status.Ball_Position := Position;
      Mediator.Ball_Status.Ball_Target := Position;
   end Place_Ball;

   procedure Move_Ball(Mediator : in out Mediator_T; Kicked : in Boolean) is
      Displacement, Displacement_After_Overlap : Displacement_T := (0,0);
   begin
      -- Se la palla ha energia maggiore di 0 e non e' arrivata' al proprio
      -- target viene calcolato lo spostamento.
      if Mediator.Ball_Status.Energy > 0
        and (Mediator.Ball_Status.Ball_Position.X
             /= Mediator.Ball_Status.Ball_Target.X
             or Mediator.Ball_Status.Ball_Position.Y
             /= Mediator.Ball_Status.Ball_Target.Y)
      then
         Displacement :=
           Follow_Target(Target_position => Mediator.Ball_Status.Ball_Target,
                         Current_position => Mediator.Ball_Status.Ball_Position,
                         Velocity => Mediator.Ball_Status.Energy);

         Displacement_After_Overlap := Displacement;

         -- Per evitare che, una volta calciata, la palla subisca l'interferenza
         -- da parte del giocatore che esegue la mossa, dovuta al fatto che
         -- entrambi occupano la medesima posizione (si veda Angle_Left),
         -- discrimino il caso in cui la palla sia appena stata calciata dal
         -- caso in cui sia gia' in movimento.
         if not Kicked then
            -- Calcolo il displacement relativo all'ostacolo piu' vicino
            -- se esiste.
            Displacement_After_Overlap :=
              Ball_Overlap_Detection(Mediator, Displacement);
         end if;

         Mediator.Ball_Status.Ball_Position.X :=
           Mediator.Ball_Status.Ball_Position.X + Displacement_After_Overlap.X;
         Mediator.Ball_Status.Ball_Position.Y :=
           Mediator.Ball_Status.Ball_Position.Y + Displacement_After_Overlap.Y;

         -- Se la palla ha incontrato un ostacolo, l'energia e' azzerata
         if Displacement_After_Overlap.X /= Displacement.X
           or Displacement_After_Overlap.Y /= Displacement.Y then
            Mediator.Ball_Status.Ball_Target :=
              Mediator.Ball_Status.Ball_Position;
            Mediator.Ball_Status.Energy := 0;
         else
            Mediator.Ball_Status.Energy :=
              Natural(0.9 * Float(Mediator.Ball_Status.Energy));
         end if;

         Ada.Text_IO.Put("MOVE");
      end if;
   end Move_Ball;

   procedure Move_Init_Position(Mediator : in out Mediator_T;
                                Player_Id : in Player_ID_T) is
      Ball_Offset : Integer :=
        Mediator.Ball_Status.Ball_Position.X - (Board_Size_X / 2);
   begin
      -- Muovo la posizione iniziale, tranne quella dei portieri
      if Get_Role(Mediator.Players_Feature(Player_Id)) /= Goalkeeper then

         -- L'offset relativo viene calcolato in base alla posizione corrente
         -- della palla, ed e' sommato alla posizione di riferimento di ogni
         -- giocatore
         if Get_Team(Mediator.Players_Feature(Player_Id)) = Team1 then
            if Ball_Offset > 0 then
               if Ball_Offset < Max_Offset_Attack_Team then
                  Mediator.Players_Init_Position(Player_Id).X :=
                    Mediator.Players_Ref_Position(Player_Id).X
                    + Ball_Offset + (Board_Size_X / 5);  -- per avere gli attaccanti davanti alla palla
               else
                  Mediator.Players_Init_Position(Player_Id).X :=
                    Mediator.Players_Ref_Position(Player_Id).X
                    + Max_Offset_Attack_Team + (Board_Size_X / 5);
               end if;
            else
               Mediator.Players_Init_Position(Player_Id).X :=
                 Mediator.Players_Ref_Position(Player_Id).X;
            end if;
         else
            if Ball_Offset < 0 then
               if Ball_Offset > -Max_Offset_Attack_Team then
                  Mediator.Players_Init_Position(Player_Id).X :=
                    Mediator.Players_Ref_Position(Player_Id).X
                    + Ball_Offset - (Board_Size_X / 5);
               else
                  Mediator.Players_Init_Position(Player_Id).X :=
                    Mediator.Players_Ref_Position(Player_Id).X
                    - Max_Offset_Attack_Team - (Board_Size_X / 5);
               end if;
            else
               Mediator.Players_Init_Position(Player_Id).X :=
                 Mediator.Players_Ref_Position(Player_Id).X;
            end if;
         end if;
      end if;
   end Move_Init_Position;


   procedure Update_Current_Clock(Mediator : in out Mediator_T;
                                  Clock    : in Logic_Clock_T) is
   begin
      -- Aggiorna il clock logico dello stato se e' inferiore rispetto a quello
      -- del giocatore in esecuzione sulla risorsa. (Current_Clock <-- Clock)
      if Get_Absolute_Counter(Clock)
        > Get_Absolute_Counter(Mediator.Current_Clock) then
         Align(Mediator.Current_Clock,Clock);
      end if;
   end;

   procedure Update_My_Clock(Mediator : in Mediator_T;
                             Clock    : in out Logic_Clock_T) is
   begin
      -- Aggiorna il clock del giocatore in esecuzione se e' inferiore rispetto
      -- a quello dello stato. (Clock <-- Current_Clock)
      if Get_Absolute_Counter(Mediator.Current_Clock)
        > Get_Absolute_Counter(Clock) then
         Align(Clock,Mediator.Current_Clock);
      end if;
   end;

   function Create_Ball_Status return Ball_Status_T is
   begin
      return (Ball_Release => True,
              Player_With_Ball => 0,
              Ball_Position => (0,0),
              Ball_Target => (0,0),
              Energy => 0);
   end Create_Ball_Status;



   function Get_Near_Target_Of(Mediator : Mediator_T;
                               ID_Player : Player_ID_T) return Targets_T is

      Position : Position_T := Mediator.Players_Position(ID_Player);
      Team : Team_T := Get_Team(Mediator.Players_Feature(ID_Player));
      Role : Role_T := Get_Role(Mediator.Players_Feature(ID_Player));
      Near_Target : Targets_T := (Others => Position);
      View_Distance : Positive :=
        Get_Radius(Mediator.Players_Feature(ID_Player));
      Shoot_Distance : Positive :=
        Get_Shoot_power(Mediator.Players_Feature(ID_Player));
      Count : Positive := Near_Target'First;
      Relative_Goal_Position : Position_T :=
        Goal_Positions(Get_Team(Mediator.Players_Feature(ID_Player)));
      R_Number : Positive;
      Target_Offset : Integer := 0;

   begin

      case Mediator.Game_Mode is
         when Off =>
            -- Nel caso lo stato di gioco sia Off i giocatori
            -- devono riposizionarsi, quindi il target per il giocatore
            -- e' la posizione in cui si deve posizionare
            if ID_Player = Mediator.Target_Player then
               Near_Target := (Others => Mediator.Target_Position);
            else
               -- Importante! Distanzia il giocatore alla distanza minima
               -- in accordo con la condizione di requeue su entry
               -- "Player_Mode_Off_Without_Target"
               -- Capita se l'init position di un giocatore � vicina al
               -- punto in cui � avvenuto il fallo (causa starvation)
               if Is_Visible(Mediator.Target_Position,
                             Mediator.Players_Init_Position(ID_Player),
                             Reposition_Threshold)
               then
                  -- Sufficiente a garantire l'invariante sopra
                  Target_Offset := 2 * Reposition_Threshold;
               end if;
               Near_Target :=
                 (Others => (Mediator.Players_Init_Position(ID_Player).X
                             + Target_Offset,
                             Mediator.Players_Init_Position(ID_Player).Y
                             + Target_Offset));
            end if;
         when On =>
            -- Nel caso lo stato di gioco sia On i target per il giocatore
            -- sono ad esempio le posizioni dei compagni o della porta
            -- avversaria.
            if Mediator.Ball_Status.Player_With_Ball = Natural(ID_Player) then
               -- Calcolo alcune posizioni casuali dove i giocatori
               -- posso calciare nel caso non abbiano target rilevanti
               -- nella loro zona di influenza
               for I in Near_Target'Range loop
                  R_Number := Generate_Number(Shoot_Distance);
                  if Team = Team1 then
                    Near_Target(I).X := Near_Target(I).X + R_Number;
                  else
                     Near_Target(I).X := Near_Target(I).X - R_Number;
                  end if;
                  if R_Number < R_Number / 3 then
                     Near_Target(I).Y :=
                       Near_Target(I).Y + Generate_Number(Kick_Y_Direction_Bound);
                  elsif R_Number < 2 * R_Number / 3  then
                     Near_Target(I).Y :=
                       Near_Target(I).Y - Generate_Number(Kick_Y_Direction_Bound);
                  else
                     null;
                  end if;
               end loop;
               -- Se il goal e' a distanza sufficiente lo inserisco come target
               if Is_Visible(Position, Relative_Goal_Position, Shoot_Distance) then
                  Near_Target := (Others => Relative_Goal_Position);
                  Count := Count + 1;
               end if;
               -- Se c'e' qualche altro target (compagno) lo inserisco.
               -- Per permettere la rimessa da fondo campo il portiere ha sempre
               -- come target un compagno, anche se oltre il suo raggio di
               -- visibilita'.
               if Count > Near_Target'Last then
                  return Near_Target;
               else
                  for I in Player_ID_T'Range loop
                     if (ID_Player /= I)
                       and Mediator.Players_Active(I)
                       and (Get_Team(Mediator.Players_Feature(I)) = Team)
                       and (Is_Visible(Position,
                                       Mediator.Players_Position(I),
                                       View_Distance)
                            or Role = Goalkeeper)
                     then
                        Near_Target(Count) := Mediator.Players_Position(I);
                        Count := Count + 1;
                        if Count > Near_Target'Last then
                           return Near_Target;
                        end if;
                     end if;
                  end loop;
               end if;
            end if;
      end case;

      return Near_Target;
   end;

   function Get_Game_Status_For(Mediator : Mediator_T;
                                ID_Player : Player_ID_T) return Game_Status_T is

      Team : Team_T := Get_Team(Mediator.Players_Feature(ID_Player));
      Init_Position : Position_T;

   begin

      if Get_Role(Mediator.Players_Feature(ID_Player)) = Goalkeeper then
         Init_Position := Mediator.Players_Ref_Position(ID_Player);
      else
         Init_Position := Mediator.Players_Init_Position(ID_Player);
      end if;
      -- Costruisco l'oggetto stato per il giocatore, affinche' possa calcolare
      -- la proprioa mossa
      return
        Create_Status(My_Id => ID_Player,
                      Ball_Is_My => Have_Ball(Mediator, ID_Player),
                      Ball_Position => Mediator.Ball_Status.Ball_Position,
                      Ball_Possesor => Mediator.Ball_Status.Player_With_Ball,
                      Nearest_To_Ball => Nearest_To_Ball(Mediator, ID_Player),
                      My_Position => Mediator.Players_Position(ID_Player),
                      My_Init_Position => Init_Position,
                      Near_Targets => Get_Near_Target_Of(Mediator, ID_Player));
   end;

   function Player_For_Role(Players : Positive;
                            Role: Role_T;
                            Scheme: Scheme_T) return Natural is
   begin
      if Players = 10 then
         if Scheme = Defn then
            case Role is
            when Goalkeeper => return 1;
            when Defender   => return 3;
            when Midfielder => return 0;
            when Forward    => return 1;
            end case;
         else
            case Role is
            when Goalkeeper => return 1;
            when Defender   => return 2;
            when Midfielder => return 0;
            when Forward    => return 2;
            end case;
         end if;
      else
         if Players = 14 then
            case Scheme is
               when Stdr =>
                  case Role is
                  when Goalkeeper => return 1;
                  when Defender   => return 2;
                  when Midfielder => return 2;
                  when Forward    => return 2;
                  end case;
               when Defn =>
                  case Role is
                  when Goalkeeper => return 1;
                  when Defender   => return 3;
                  when Midfielder => return 2;
                  when Forward    => return 1;
                  end case;
               when Offn =>
                  case Role is
                  when Goalkeeper => return 1;
                  when Defender   => return 2;
                  when Midfielder => return 3;
                  when Forward    => return 1;
                  end case;
            end case;
         else
            case Scheme is
            when Stdr =>
               case Role is
               when Goalkeeper => return 1;
               when Defender   => return 3;
               when Midfielder => return 4;
               when Forward    => return 3;
               end case;
               when Defn =>
                  case Role is
                  when Goalkeeper => return 1;
                  when Defender   => return 4;
                  when Midfielder => return 4;
                  when Forward    => return 2;
                  end case;
               When Offn =>
                  case Role is
                  when Goalkeeper => return 1;
                  when Defender   => return 2;
                  when Midfielder => return 5;
                  when Forward    => return 3;
                  end case;
            end case;
         end if;
      end if;
   end Player_For_Role;

   procedure Move_Referee(Mediator : in out Mediator_T) is
      Displacement : Displacement_T := (0,0);
   begin

      Displacement :=
        Follow_Target
          (Target_position => (Mediator.Ball_Status.Ball_Position.X
                               + Referee_To_Ball_Offset,
                               Mediator.Ball_Status.Ball_Position.Y
                               + Referee_To_Ball_Offset),
           Current_position => Mediator.Referee_Position,
           Velocity => Referee_Velocity);

      Mediator.Referee_Position.X :=
        Mediator.Referee_Position.X + Displacement.X;
      Mediator.Referee_Position.Y :=
        Mediator.Referee_Position.Y + Displacement.Y;

   end Move_Referee;

   procedure Place_Object(Mediator : in out Mediator_T) is
   begin
      -- Posiziono giocatori
      Place_Players(Mediator);

      -- Posiziono arbitro
      Mediator.Referee_Position := ((Board_Size_X/2) - Referee_To_Ball_Offset,
                                    (Board_Size_Y/2) - Referee_To_Ball_Offset);

      -- Posiziono palla
      Place_Ball(Mediator, (Board_Size_X/2, Board_Size_Y/2));
   end Place_Object;

   -- Usata in Start_Match
   procedure Place_Players(Mediator : in out Mediator_T) is
   begin
      Set_Ref_Player_Position(Mediator);
      Set_Init_Player_Position(Mediator);
      Set_Player_Position(Mediator);
   end Place_Players;

   procedure Set_Ref_Player_Position(Mediator : in out Mediator_T) is
      Player : Player_ID_T;
      Team : Team_T;
      Role : Role_T;
      Counters_Team1 : Players_Counter_T := (others => 0);
      Counters_Team2 : Players_Counter_T := (others => 0);
      Y_Off_Set : Players_Counter_T := (others => 0);
      X_Off_Set : Positive := Board_Size_X / 10;
   begin

      -- Calcolo la posizione dove saranno disposti i giocatori

      for R in Role_T'Range loop
         Y_Off_Set(R) :=
           Board_Size_Y / ((Player_For_Role
             (Mediator.Players_Number, R, Mediator.Game_Scheme)) + 1);
      end loop;

      for P in Mediator.Players_Active'Range loop
         if Mediator.Players_Active(P) then
            Player := P;
            Role := Get_Role(Mediator.Players_Feature(Player));
            Team := Get_Team(Mediator.Players_Feature(Player));
            if Team = Team1 then
               case Role is
                  when Goalkeeper =>
                     Mediator.Players_Ref_Position(Player).X := 1 * X_Off_Set;
                  when Defender   =>
                     Mediator.Players_Ref_Position(Player).X := 2 * X_Off_Set;
                  when Midfielder =>
                     Mediator.Players_Ref_Position(Player).X := 3 * X_Off_Set;
                  when Forward    =>
                     Mediator.Players_Ref_Position(Player).X := 4 * X_Off_Set;
               end case;
               Counters_Team1(Role) := Counters_Team1(Role) + 1;
               Mediator.Players_Ref_Position(Player).Y :=
                 Y_Off_Set(Role) * Counters_Team1(Role);
            else
               case Role is
                  when Goalkeeper =>
                     Mediator.Players_Ref_Position(Player).X :=
                       Board_Size_X - (1 * X_Off_Set);
                  when Defender   =>
                     Mediator.Players_Ref_Position(Player).X :=
                       Board_Size_X - (2 * X_Off_Set);
                  when Midfielder =>
                     Mediator.Players_Ref_Position(Player).X :=
                       Board_Size_X - (3 * X_Off_Set);
                  when Forward    =>
                     Mediator.Players_Ref_Position(Player).X :=
                       Board_Size_X - (4 * X_Off_Set);
               end case;

               Counters_Team2(Role) := Counters_Team2(Role) + 1;
               Mediator.Players_Ref_Position(Player).Y :=
                 Y_Off_Set(Role) * Counters_Team2(Role);

            end if;
         end if;

      end loop;
   end Set_Ref_Player_Position;

   procedure Set_Init_Player_Position(Mediator : in out Mediator_T) is
   begin
      for P in Mediator.Players_Active'Range loop
         if Mediator.Players_Active(P) then
            Mediator.Players_Init_Position(P) :=
              Mediator.Players_Ref_Position(P);
         end if;
      end loop;
   end Set_Init_Player_Position;


   procedure Set_Player_Position(Mediator : in out Mediator_T) is
   begin
      for P in Mediator.Players_Active'Range loop
         if Mediator.Players_Active(P) then
            Mediator.Players_Position(P) :=
              Mediator.Players_Ref_Position(P);
         end if;
      end loop;
   end Set_Player_Position;

   function Have_Ball(Mediator : Mediator_T; ID_Player : Player_ID_T) return Boolean is
   begin
      return Natural(ID_Player) = Mediator.Ball_Status.Player_With_Ball;
   end Have_Ball;

   -- Controlla se ci sono falli pendenti da "fischiare", considera soltanto il
   -- primo in presenza di molti, pulisce la lista al termine.
   procedure Check_For_Foul(Mediator : in out Mediator_T) is
      Action : Action_T;
      Foul_Position : Position_T;
      Foul_Seen : Boolean := False;
   begin
      -- Scorro la lista
      while not Action_Queue.Is_Empty(Mediator.Deferred_Action) loop
         Action := Action_Queue.First_Element(Mediator.Deferred_Action);
         -- Se e' un fallo
         if Get_Action_Type(Action) = Game.Logic.Action.Foul then
            if Foul_Seen = False then
               Foul_Position := Get_Target_Pos(Action);
               if Is_Visible(Mediator.Referee_Position,
                             Foul_Position,
                             Foul_Threshold)
               then
                  Mediator.Target_Player := Player_ID_T(Get_Target_Id(Action));
                  Mediator.Target_Position := Foul_Position;
                  Mediator.Ball_Status.Ball_Position :=
                    (Foul_Position.X + 1, Foul_Position.Y + 1);
                  Mediator.Ball_Status.Ball_Target :=
                    Mediator.Ball_Status.Ball_Position;
                  Mediator.Ball_Status.Energy := 0;
                  Mediator.Ball_Status.Player_With_Ball := 0;
                  Mediator.Game_Status_Type := Reposition_For_Foul;
                  Release_Ball(Mediator);
                  -- Cambio il modo di gioco. Ha effetto sulle guardie
                  -- dei giocatori
                  Change_Mode_To_Off(Mediator);
                  Inc_Player_Foul(Mediator,Get_Id(Action));
                  Foul_Seen := True;
               end if;
            end if;
         end if;
         -- Elimino in ogni caso l'Action
         Action_Queue.Delete_First(Mediator.Deferred_Action);
      end loop;

   end Check_For_Foul;

   procedure Check_For_Player_Replacement (Mediator : in out Mediator_T;
                                           Player_ID : in Player_ID_T;
                                           Clock : in out Logic_Clock_T) is

      Position : Action_Queue.Cursor :=
        Action_Queue.First(Mediator.Deferred_Replacement);
      Found : Boolean := False;
      Action : Action_T;
      Target_ID : Player_ID_T;

   begin

      -- Scorro la lista, se il giocatore Player_ID e' interessato alla
      -- sostituzione, prima della ripresa del gioco, e' sostituito.
      while not Found and Action_Queue.Has_Element(Position) loop
         Action := Action_Queue.Element(Position);
         if Get_Action_Type(Action) = Game.Logic.Action.Replace_Player then
            Target_ID := Player_ID_T(Get_Target_Id(Action));
            if Target_ID = Player_ID then
               -- Solo per dimostrazione viene mostrato com'e' possibile
               -- sostituire il giocatore cambiandone l'oggetto corrispondente
               -- e costruendo un nuovo clock logico secondo il nuovo
               -- valore di velocita'. Il clock poi andra' a modificare il
               -- tempo di sospensione del task.
               -- La cosa importante e' che non vi e' alcuna terminazione o
               -- avvio di task, quindi la popolazione rimane invariata.
               Mediator.Players_Feature(Target_ID) :=
                 Game.Logic.Player.Create
                   (Name => Ada.Strings.Unbounded.To_Unbounded_String("SUB"),
                    Team => Get_Team(Mediator.Players_Feature(Target_ID)),
                    Role => Get_Role(Mediator.Players_Feature(Target_ID)),
                    Abilities => Create_Abilities
                      (Shoot_power => 64,
                       Velocity => 4,
                       Tackle => 8,
                       Pass_Probability => 80),
                    Energy => 10,
                    Influence_Zone_Radius => 100);
               -- Il clock e' aggiornato secondo le features del nuovo player
               Clock := Game.Clock.Create
                 (Feature_To_Sleep(Mediator.Players_Feature(Target_ID)),
                  Hyperperiod_Const);
               Found := True;
               -- Elimino l'Action
               Action_Queue.Delete(Mediator.Deferred_Replacement, Position);
            end if;
         end if;
         Position := Action_Queue.Next(Position);
      end loop;
   end Check_For_Player_Replacement;

   function Player_Overlap_Detection(Mediator : Mediator_T;
                                     ID_Player : Player_ID_T;
                                     Displacement : Displacement_T)
                                     return Displacement_T is
      Start_P : Position_T := Mediator.Players_Position(ID_Player);
      Min_Displacement: Displacement_T := Displacement;
      Overlap : Boolean := False;
   begin

      -- Assumiamo che il giocatore possa sovrapporsi alla palla, e all'arbitro
      -- decommentare altrimenti
      --if Check_Overlap(Start_P, Displacement, Mediator.Referee_Position) then
      --   Min_Displacement := Start_P - Mediator.Referee_Position;
      --   Overlap := True;
      --end if;

      for P in Player_ID_T'Range loop
         if P /= ID_Player and Mediator.Players_Active(P)  then
            if Check_Overlap(Start_P,
                             Min_Displacement,
                             Mediator.Players_Position(P))
            then
               Min_Displacement := Start_P - Mediator.Players_Position(P);
               Overlap := True;
            end if;
         end if;
      end loop;

      if Overlap then
         -- Vale l'invariante che Pred_Displacement ritorna SEMPRE una posizione
         -- Min_Displacement contiene il punto di scontro piu' vicino al giocatore
         return Pred_Displacement(Min_Displacement);
      else
         return Displacement;
      end if;

   end Player_Overlap_Detection;

   function Ball_Overlap_Detection (Mediator : Mediator_T;
                                    Displacement : Displacement_T)
                                    return Displacement_T is
      Start_P : Position_T := Mediator.Ball_Status.Ball_Position;
      Min_Displacement: Displacement_T := Displacement;
      Overlap : Boolean := False;
   begin

      -- Assumiamo che l'arbitro evita sempre la palla, deccomentare altrimenti
      --if Overlap(Start_P, Displacement, Mediator.Referee_Position) then
      --   Min_Displacement := Start_P - Mediator.Referee_Position;
      --   Overlap := True;
      --end if;

      for P in Player_ID_T'Range loop
         if Mediator.Players_Active(P) then
            if Check_Overlap(Start_P,
                             Min_Displacement,
                             Mediator.Players_Position(P))
            then
               Min_Displacement := Start_P - Mediator.Players_Position(P);
               Overlap := True;
            end if;
         end if;
      end loop;

      if Overlap then
         -- Vale l'invariante che Pred_Displacement ritorna SEMPRE una posizione
         -- Min_Displacement contiene il punto di scontro piu' vicino al giocatore
         return Pred_Displacement(Min_Displacement);
      else
         return Displacement;
      end if;

   end Ball_Overlap_Detection;


   function Referee_Overlap_Detection (Mediator : Mediator_T; Displacement : Displacement_T) return Displacement_T is
      Start_P : Position_T := Mediator.Referee_Position;
      Min_Displacement: Displacement_T := Displacement;
      Overlap : Boolean := False;
   begin

      -- Assumiamo che l'arbitro evita sempre la palla, decommentare altrimenti
      --if Overlap(Start_P, Displacement, Mediator.Ball_Status.Ball_Position) then
      --   Min_Displacement := Start_P - Mediator.Ball_Status.Ball_Position;
      --   Overlap := True;
      --end if;

      for P in Player_ID_T'Range loop
         if Mediator.Players_Active(P) then
            if Check_Overlap(Start_P,
                             Min_Displacement,
                             Mediator.Players_Position(P))
            then
               Min_Displacement := Start_P - Mediator.Players_Position(P);
               Overlap := True;
            end if;
         end if;
      end loop;

      if Overlap then
         -- Vale l'invariante che Pred_Displacement ritorna SEMPRE una posizione
         -- Min_Displacement contiene il punto di scontro piu' vicino al giocatore
         return Pred_Displacement(Min_Displacement);
      else
         return Displacement;
      end if;

   end Referee_Overlap_Detection;

   function Angle_Left(P0 : Position_T; P1 : Position_T; P2 : Position_T)
                       return Integer is
   begin
      return (P1.X - P0.X) * (P2.Y - P0.Y) - (P2.X - P0.X) * (P1.Y - P0.Y);
   end Angle_Left;

   function Check_Overlap(P0 : Position_T; D : Displacement_T; P2 : Position_T)
                          return Boolean is

      P1 : Position_T := (P0.X + D.X, P0.Y + D.Y);
      D1, D2, D3 : Integer; -- determinanti

   begin
      D1 := Angle_Left(P2,P2,P0);
      D2 := Angle_Left(P2,P2,P1);
      D3 := Angle_Left(P0,P1,P2);
      if D1 = 0 and D2 = 0 and D3 = 0 then -- collineari
         return ((P1.X-P2.X)*(P0.X-P2.X)<=0 and (P1.Y-P2.Y)*(P0.Y-P2.Y)<=0) or
           ((P2.X-P0.X)*(P2.X-P0.X)<=0 and (P2.Y-P0.Y)*(P2.Y-P0.Y)<=0);
      else --non collineari
         return False;
      end if;
   end Check_Overlap;


   function Pred_Displacement (Displacement : Displacement_T)
                               return Displacement_T is
   begin
      if Displacement.X > 0 then
         if Displacement.Y > 0 then
            return (Displacement.X - 1, Displacement.Y - 1);
         else
            return (Displacement.X - 1, Displacement.Y + 1);
         end if;
      else
         if Displacement.Y > 0 then
            return (Displacement.X + 1, Displacement.Y - 1);
         else
            return (Displacement.X + 1, Displacement.Y + 1);
         end if;
      end if;
   end Pred_Displacement;

   function Create_Player_Stat return Player_Stat_T is
   begin
      return (Distance => 0.0,
              Foul => 0,
              Energy => 0);
   end Create_Player_Stat;

   function Create_Stat return Stat_T is
   begin
      return (Goal_Team1 => 0,
              Goal_Team2 => 0,
              Players_Stat => (others => Create_Player_Stat));
   end Create_Stat;

   procedure Check_Ball_Event(Mediator : in out Mediator_T) is
      Ball : Position_T := Mediator.Ball_Status.Ball_Position;
      Min_Goal_Y : Positive := (Board_Size_Y / 2) - (Goal_Size / 2);
      Max_Goal_Y : Positive := (Board_Size_Y / 2) + (Goal_Size / 2);
   begin
      if Ball.X <= 0 or Ball.X >= Board_Size_X then
         if Ball.Y >= Min_Goal_Y and Ball.Y <= Max_Goal_Y then
            -- La palla e' entrata in GOAL
            if Ball.X <= 0 then
               Mediator.Stat.Goal_Team2 := Mediator.Stat.Goal_Team2 + 1;
               Ada.Text_IO.Put("GOAL FOR TEAM 2");
            else
               Mediator.Stat.Goal_Team1 := Mediator.Stat.Goal_Team1 + 1;
               Ada.Text_IO.Put("GOAL FOR TEAM 1");
            end if;
            Place_Ball(Mediator, (Board_Size_X/2, Board_Size_Y/2));
            Mediator.Game_Status_Type := Reposition_For_Goal;
            Mediator.Target_Player := Player_ID_T'First;
            Mediator.Target_Position :=
              Mediator.Players_Ref_Position(Mediator.Target_Player);
            -- Scelgo un giocatore come target per battere la palla dopo il GOAL
            for P in Player_ID_T'Range loop
               if Mediator.Players_Active(P)
                 and Get_Team(Mediator.Players_Feature(P)) /= Mediator.Attack_Team
                 and Get_Role(Mediator.Players_Feature(P)) = Forward
               then
                  Mediator.Target_Player := P;
                  Mediator.Target_Position := Mediator.Ball_Status.Ball_Position;
                  exit;
               end if;
            end loop;
            Change_Mode_To_Off(Mediator);
         else
            -- La palla e' uscita a fondo campo
            if Ball.X <= 0 then
               Place_Ball
                 (Mediator, (Board_Size_X / 20, Board_Size_Y / 2));
            else
               Place_Ball
                 (Mediator, (Board_Size_X - Board_Size_X / 20, Board_Size_Y / 2));
            end if;
            Mediator.Game_Status_Type := Reposition_For_Out;
            Mediator.Target_Player := Player_ID_T'First;
            Mediator.Target_Position :=
              Mediator.Players_Ref_Position(Mediator.Target_Player);
            -- Il target per battere la palla sara' il portiere
            for P in Player_ID_T'Range loop
               if Mediator.Players_Active(P)
                 and Get_Team(Mediator.Players_Feature(P)) /= Mediator.Attack_Team
                 and Get_Role(Mediator.Players_Feature(P)) = Goalkeeper
               then
                  Mediator.Target_Player := P;
                  Mediator.Target_Position := Mediator.Ball_Status.Ball_Position;
                  exit;
               end if;
            end loop;
            Change_Mode_To_Off(Mediator);
         end if;
      else
         if Ball.Y <= 0 or Ball.Y >= Board_Size_Y then
            -- La palla e' uscita lateralmente dal campo
            if Ball.Y <= 0 then
               Place_Ball(Mediator, (Ball.X, Threshold));
            else
               Place_Ball(Mediator, (Ball.X, Board_Size_Y - Threshold));
            end if;
            Mediator.Game_Status_Type := Reposition_For_Out;
            Mediator.Target_Player := Player_ID_T'First;
            Mediator.Target_Position :=
              Mediator.Players_Ref_Position(Mediator.Target_Player);
            -- Scelgo un giocatore che faccia la rimessa, che non sia il portiere
            for P in Player_ID_T'Range loop
               if Mediator.Players_Active(P)
                 and Get_Team(Mediator.Players_Feature(P)) /= Mediator.Attack_Team
                 and Get_Role(Mediator.Players_Feature(P)) /= Goalkeeper then
                  --Uso il target per riposizionare il giocatore attraverso
                  --la Near_Target il modo Off e lo stato Reposition_For_Out
                  Mediator.Target_Player := P;
                  Mediator.Target_Position := Mediator.Ball_Status.Ball_Position;
                  Change_Mode_To_Off(Mediator);
                  exit;
               end if;
            end loop;
         end if;
      end if;
   end Check_Ball_Event;

   function Nearest_To_Ball(Mediator : Mediator_T;
                            ID_Player : Player_ID_T) return Boolean is
      Team : Team_T := Get_Team(Mediator.Players_Feature(ID_Player));
      Distance : Float :=
        Mediator.Players_Position(ID_Player) - Mediator.Ball_Status.Ball_Position;
   begin
      if Mediator.Ball_Status.Player_With_Ball = Natural(ID_Player) then
         return True;
      end if;
      for P in Player_ID_T'Range loop
         if Mediator.Players_Active(P)
           and Get_Team(Mediator.Players_Feature(P)) = Team
           and Mediator.Players_Position(P) - Mediator.Ball_Status.Ball_Position
           < Distance
         then
            return False;
         end if;
      end loop;
      return True;
   end Nearest_To_Ball;

   function Is_Empty_Player_Energy(Mediator : Mediator_T;
                                   ID : Player_ID_T) return Boolean is
   begin
      return Mediator.Stat.Players_Stat(ID).Energy = 0;
   end Is_Empty_Player_Energy;

   procedure Dec_Player_Energy(Mediator : in out Mediator_T;
                               ID : in Player_ID_T) is
   begin
      if Mediator.Stat.Players_Stat(ID).Energy > 0 then
         Mediator.Stat.Players_Stat(ID).Energy :=
           Mediator.Stat.Players_Stat(ID).Energy - 1;
      end if;
   end Dec_Player_Energy;

   procedure Inc_Player_Energy(Mediator : in out Mediator_T;
                               ID : in Player_ID_T) is
      Inc : Integer := 1 + Get_Energy(Mediator.Players_Feature(ID)) / 10;
   begin
      if Mediator.Stat.Players_Stat(ID).Energy + Inc
        < Get_Energy(Mediator.Players_Feature(ID)) then
         Mediator.Stat.Players_Stat(ID).Energy :=
           Mediator.Stat.Players_Stat(ID).Energy + Inc;
      end if;
   end Inc_Player_Energy;

   procedure Add_Player_Distance(Mediator : in out Mediator_T;
                                 ID : in Player_ID_T;
                                 Distance : Float) is
   begin
      Mediator.Stat.Players_Stat(ID).Distance :=
        Mediator.Stat.Players_Stat(ID).Distance +
        (Distance * 120.0 / Float(Game.Logic.Board_Size_X)) ;
   end Add_Player_Distance;

   procedure Inc_Player_Foul(Mediator : in out Mediator_T;
                             ID : in Player_ID_T) is
   begin
      Mediator.Stat.Players_Stat(ID).Foul :=
        Mediator.Stat.Players_Stat(ID).Foul + 1;
   end Inc_Player_Foul;

end Game.Logic.Mediator;
