--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

-- TODO
-- (fatto) Gestione Goal e Out
-- (fatto) Giocatori si incastrano in alcune situazioni
-- (fatto) Nel caso di fallo, requeue se il giocatore ha distanza minima dalla palla
-- (fatto) Agganciare sostituzioni
-- (fatto) Agganciare cambio modulo di gioco
-- (fatto) Rimuovere initialize giocatori
-- (fatto) L'ID e' assegnato internamente
-- Pool di features da cui attingere in init

-- Questo package contiene la procedura Main attraverso la quale il SO e' in
-- grado di avviare la simulazione.

with Ada.Text_IO;

-- L'inclusione di questi package comporta l'elaborazione e l'attivazione
-- concorrente di tutti i task contenuti

with Game.Referee; 	use Game.Referee;
with Game.Ball; 	use Game.Ball;
with Game.Player; 	use Game.Player;
with Game.Test; 	use Game.Test;
with Game.Broker; 	use Game.Broker;


procedure Main is

   -- Il numero di task Player � definito staticamente dalla seguente
   -- dichiarazione
   PA1 : The_Player;
   PB1 : The_Player;
   PC1 : The_Player;
   PD1 : The_Player;
   PE1 : The_Player;
   PF1 : The_Player;
   PG1 : The_Player;
   PH1 : The_Player;
   PI1 : The_Player;
   PL1 : The_Player;
   PM1 : The_Player;

   PA2 : The_Player;
   PB2 : The_Player;
   PC2 : The_Player;
   PD2 : The_Player;
   PE2 : The_Player;
   PF2 : The_Player;
   PG2 : The_Player;
   PH2 : The_Player;
   PI2 : The_Player;
   PL2 : The_Player;
   PM2 : The_Player;

begin

   -- Il task Main e' utilizzato per avviare il server ORB
   Game.Broker.Server;

end Main;

