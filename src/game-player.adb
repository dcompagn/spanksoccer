--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game; use Game;
with Game.Status; use Game.Status;
with Game.Clock; use Game.Clock;
with Ada.Calendar; use Ada.Calendar;
with Ada.Text_IO;
with Game.Logic; use Game.Logic;

-- la terminazione della palla si puo' fare con l'oggetto finalize
package body Game.Player is

   -- La struttura rispetta il modello di task periodico, in grado
   -- di compiere un lavoro e successivamente sospendersi in attesa di un nuovo
   -- periodo. La sospensione assoluta e' determinata rispetto ad un riferimento
   -- temporale globale a tutte le altre entita' coinvolte nella simulazione.
   -- Il lavoro eseguito consiste nel calcolare la mossa migliore rispetto
   -- allo stato attuale di gioco e alle condizioni del giocatore, il quale
   -- percio' deve sincronizzarsi presso la risorsa protetta Status_T, inibendo
   -- altri task l'uso della CPU.
   -- L'utilizzo di un contatore intero permette di esprimere la condizione di
   -- terminazione di un match secondo il numero di unita' di tempo che lo
   -- compongono. La terminazione del task, invece, puo' avvenire soltanto
   -- attraverso il pannello manager attraverso un segnale di shutdown.
   -- Il task in quanto entita' concorrente non dovrebbe avere conoscenza
   -- del lavoro che esso compie, ossia della logica applicativa pertanto il
   -- agisce soltanto come contenitore, o luogo del controllo. Questo spiega
   -- sia il fatto che il numero di task giocatore e' definito staticamente,
   -- sia che non e' significante conoscere a priori quale luogo del controllo
   -- sara' associato ad un certo giocatore (logico), inteso come l'insieme
   -- delle informazioni e della logica che ne descrive il comportamento.
   -- Il legame tra task e giocatore e' realizzato in fase di avvio match,
   -- appena e' disponibile il numero di giocatori scelto, tramite
   -- l'attribuuzione di un identificativo univoco per ogni giocatore.
   -- Definire un valore statico sul numero di task permette al sistema di
   -- avere buone proprieta' di analisi d'uso delle risorse.
   task body The_Player is

      -- Istante temporale unico e globale di avvio partita
      Start_Time : Time;

      -- Rappresenta l'istante temporale al quale il task sara' re-inserito
      -- nella coda dei pronti
      Wake_Up : Time;

      -- Rappresenta il tempo di sospensione del task in unita', e' utilizzato
      -- per calcolare il tempo di sospensione (default 1)
      Sleep : Positive := 1;

      -- Rappresenta il tempo di sopensione effettivo con tipo Duration,
      -- calcolato come il rapporto tra Sleep e Sleep_Factor
      Sleep_Time : Standard.Duration :=
        Standard.Duration(Float(Sleep)/Float(Sleep_Factor));

      -- Rappresenta il clock logico, ad ogni turno e' incrementato
      -- o riallineato, e permette di eliminare il drift cumulativo conseguente
      -- al riallineamento. E' utilizzato per determinare il tempo di
      -- sospensione assoluto del task.
      Logic_Clock : Logic_Clock_T;

      -- Valore che indica se la condizione di terminazione del match � stata
      -- raggiunta, permette di uscire dal loop pi� interno
      End_Match : Boolean := False;

      -- Valore che permette di terminare il task istantaneamente, ottenuto
      -- dallo stato globale, True in caso di chiusura del pannello manager
      Shut_Down : Boolean := False;

      -- Identificativo del giocatore
      ID : Player_ID_T;
   begin

      -- I due loop innestati permettono di riavviare un match qualora sia
      -- giunto a terminazione, simmetricamente a tutti i task
      loop

         End_Match := False;

         -- Il task e' posto in attesa del segnale di start match. Questo e'
         -- un punto di sinconizzazione su Status_T.
         Status_T.Waiting_For_Start_Signal_To_Player(ID,
                                                     Sleep,
                                                     Start_Time,
                                                     Shut_Down);

         if not Shut_Down then

            -- Inizializzazione del clock logico
            Logic_Clock := Create(Sleep, Hyperperiod_Const);

            loop

               -- La terminazione del match avviene non appena il clock logico
               -- assume un valore superiore alla Match_Duration per garantire
               -- lo sblocco delle guardie, ed il rilascio dei giocatori in
               -- attesa su di esse, da parte del Referee.
               if Shut_Down
                 or Get_Absolute_Counter(Logic_Clock) > Game.Match_Duration then

                  -- Output concorrente (rischioso)
                  Ada.Text_IO.Put_Line("PLAYER FINISH : Clock "&
                                       Integer'Image
                                         (Get_Absolute_Counter(Logic_Clock)));

                  End_Match := True;

               else

                  -- Punto di sincronizzazione (avoidance sync). Causa
                  -- potenziale accodamento, quindi prerilascio.
                  -- L'azione del task eseguita in mutua esclusione sullo stato
                  -- globale.
                  -- Semantica: viene selezionata la prima entry se guardia
                  -- associata e' aperta, la seconda altrimenti.
                  select
                     Status_T.Player_Mode_On(ID, Logic_Clock, Shut_Down);
                  else
                     Status_T.Player_Mode_Off(ID, Logic_Clock, Shut_Down);
                  end select;

                  -- Avanzamento dell'orologio
                  Tick(Logic_Clock);

                  -- Calcolo dell'istante assoluto di risveglio del task
                  Wake_Up := Start_Time +
                    Standard.Duration(Get_Absolute_Counter(Logic_Clock)) * Sleep_Time;

                  -- Sospensione (punto di prerilascio)
                  delay until Wake_Up;

               end if;

               -- Condizione di terminazione
               exit when End_Match;
            end loop;

            -- Accodamento sulla barriera di fine match. Rappresenta una
            -- la join di tutti i task che si sincronizzano in questa entry
            Status_T.Ready_To_End(Shut_Down);

         end if;

         -- Condizione di terminazione del task
         exit when Shut_Down;
      end loop;

   end The_Player;

end Game.Player;
