--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game.Status; use Game.Status;
with panel_RI; use panel_RI;
with panel_RI.Viewer_RI; use panel_RI.Viewer_RI;
with Game.Logic; use Game.Logic;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Game.Utils;
with CORBA.ORB; use CORBA.ORB;
with PolyORB.Setup.Client; use PolyORB.Setup.Client;
with PolyORB.Utils.Report; use PolyORB.Utils.Report;
with PortableServer.POA.Helper;
with CORBA.Object;
with CORBA.Impl;
with PortableServer.POAManager;
with control_RI.manager_RI.impl;
with control_RI.subscriber_RI.impl;
with PolyORB.CORBA_P.CORBALOC;
with Ada.Calendar; use Ada.Calendar;

-- Utilizzo del No_Tasking_Server: il middleware non crea ulteriori task per
-- gestire le richieste in entrata
with PolyORB.Setup.No_Tasking_Server;

package body Game.Broker is

   protected body Subscription_Container is

      procedure Add(IOR : Unbounded_String;  Id : out Integer) is
      begin
         IORs.Append(New_Item => IOR);
         Ada.Text_IO.Put_Line("OK");
         Id := IORs.Last_Index;
      end Add;

      function GetElement(index: Integer) return Unbounded_String is
      begin
         return IORs.Element(index);
      end GetElement;

      function GetFirst return Integer is
      begin
         return IORs.First_Index;
      end GetFirst;

      function GetLast return Integer is
      begin
         return IORs.Last_Index;
      end GetLast;

      procedure Delete(ID : Integer) is
      begin
         IORs.Replace_Element (Index => ID, New_Item => Null_String);
         Put_Line("cancellato IOR relativo a ID=" & ID'Img);
      end Delete;

  end Subscription_Container;


   procedure Server is
      Argv           : CORBA.ORB.Arg_List;
      Root_POA       : PortableServer.POA.Local_Ref;
      Ref_manager    : CORBA.Object.Ref;
      Ref_subscriber : CORBA.Object.Ref;

      -- Riferimento all'istanza locale dell'oggetto Manager
      Obj_manager    : constant CORBA.Impl.Object_Ptr :=
        new control_RI.manager_RI.impl.Object;

      -- Riferimento all'istanza locale dell'oggetto Viever
      Obj_subscriber : constant CORBA.Impl.Object_Ptr :=
        new control_RI.subscriber_RI.impl.Object;
   begin

      CORBA.ORB.Init(CORBA.ORB.To_CORBA_String ("ORB"), Argv);

      --  Retrieve Root POA

      Root_POA := PortableServer.POA.Helper.To_Local_Ref
        (CORBA.ORB.Resolve_Initial_References
           (CORBA.ORB.To_CORBA_String ("RootPOA")));

      PortableServer.POAManager.Activate
        (PortableServer.POA.Get_The_POAManager (Root_POA));

      --  Set up new object

      Ref_manager := PortableServer.POA.Servant_To_Reference
        (Root_POA, PortableServer.Servant (Obj_manager));

      Ref_subscriber := PortableServer.POA.Servant_To_Reference
        (Root_POA, PortableServer.Servant (Obj_subscriber));

      --  Output IOR on file

      if Game.Utils.write_to_file
        (CORBA.To_Standard_String(CORBA.Object.Object_To_String(Ref_manager)),
         "IOR_manager.txt") = False
      then
         Ada.Text_IO.Put_Line("deaD");
         raise Program_Error;
      end if;

      if Game.Utils.write_to_file
        (CORBA.To_Standard_String(CORBA.Object.Object_To_String(Ref_subscriber)),
         "IOR_subscriber.txt") = False
      then
         Ada.Text_IO.Put_Line("deaD");
         raise Program_Error;
      end if;

      --  Launch the server
      Ada.Text_IO.Put_Line("Server Started");
      CORBA.ORB.Run;
   end Server;

   task body The_Broker is

      --CLIENT
      Players_pos_RI : panel_RI.PlayerSequence;
      my_remote_object : panel_RI.Viewer_RI.Ref; -- this oggetto remoto pannello
      Status_gathered : panel_RI.GameStatus;  --stato raccolto

      --LOGIC
      Sleep : Positive := 1;
      Sleep_Time : Standard.Duration :=
        Standard.Duration(Float(Sleep)/Float(Sleep_Factor));

      Referee_pos : Position_T;
      Ball_pos    : Position_T;
      Duration_match : Natural;
      Score_team_1 : Natural;
      Score_team_2 : Natural;
      Status : Integer;

      Players_Number : Positive;
      Wake_Up : Time;
      Shut_Down : Boolean := False;
      Match_Clock : Natural;

      First_Pos, Last_Pos : Integer;

      IOR_string : Unbounded_String;

   begin

      loop

         Status_T.Waiting_For_Start_Signal_To_Other(Players_Number,
                                                    Wake_Up,
                                                    Shut_Down);

         if not Shut_Down then

            declare
               Players_pos : Player_Sequence_Wrapper_T(1 .. Players_Number);
            begin

               loop

                  Wake_Up := Wake_Up + Sleep_Time;

                  -- Raccolgo le informazioni dallo Stato (in mutua esclusione)
                  -- e le prepara all'invio
                  Status_T.Broker(Match_Clock,
                                  Ball_pos,
                                  Referee_pos,
                                  Players_pos,
                                  Shut_Down,
                                  Duration_match,
                                  Score_team_1,
                                  Score_team_2,
                                  Status);

                  -- Se Shut_Down = True allora CORBA � down
                  if not Shut_Down then

                     Status_gathered.Players_Position :=
                       Game.Utils.Player_Sequence_Warpper_T_To_RI_PlayerSequence(Players_pos);
                     Status_gathered.Ball_Position :=
                       Game.Utils.Position_T_To_RI_Position(Ball_pos);
                     Status_gathered.Referee_Position :=
                       Game.Utils.Position_T_To_RI_Position(Referee_pos);
                     Status_gathered.Time := CORBA.Long(Match_Clock);
                     Status_gathered.Duration := CORBA.Long(Duration_match);
                     Status_gathered.Score_team_1 := CORBA.Long(Score_team_1);
                     Status_gathered.Score_team_2 := CORBA.Long(Score_team_2);
                     Status_gathered.Control := CORBA.Long(Status);

                     First_Pos := Subscription_Container.GetFirst;
                     Last_Pos := Subscription_Container.GetLast;

                     for I in First_Pos .. Last_Pos loop
                        -- Legge il vettore di ior, prende l'ior iesimo
                        IOR_string := Subscription_Container.GetElement(I);
                        if IOR_string = Null_string then
                           --Ada.Text_IO.Put_Line("Trovato null");
                           null;
                        else
                           -- Se esiste l'ior
                           begin
                              CORBA.ORB.String_To_Object
                                (CORBA.To_CORBA_String
                                   (Ada.Strings.Unbounded.To_String(IOR_string)),
                                 my_remote_object);
                              --  Checking if it worked
                              if panel_RI.Viewer_RI.Is_Nil(my_remote_object)
                              then
                                 Ada.Text_IO.Put_Line
                                   ("main : cannot invoke on a nil reference");
                              else
                                 -- Invio al pannello
                                 panel_RI.Viewer_RI.publish_status
                                   (my_remote_object,Status_gathered);
                              end if;
                           exception
                              when others =>
                                 -- se l'invio da errore non dovrebbe fare nulla
                                 Ada.Text_IO.Put_Line("IOR non trovato");
                           end;
                        end if;
                     end loop;

                     Wake_Up := Wake_Up + Sleep_Time;

                     delay until Wake_Up;
                  end if;

                  -- Esce quando il match e' terminato o arriva il segnale
                  -- di shutdown
                  exit when (Match_Clock >= Game.Match_Duration) or Shut_Down;
               end loop;
            end;

            -- Sincronizzo il Broker sulla medesima entry in attesa che tutti
            -- i task abbiano raggiunto la end match.
            Status_T.Ready_To_End(Shut_Down);

         end if;

         exit when Shut_Down;
      end loop;


   exception
      when E                          : CORBA.Transient =>
         declare
            Memb                      : CORBA.System_Exception_Members;
         begin
            CORBA.Get_Members (E, Memb);
            Put ("received exception transient, minor");
            Put (CORBA.Unsigned_Long'Image (Memb.Minor));
            Put (", completion status : ");
            Put_Line (CORBA.Completion_Status'Image (Memb.Completed));
            End_Report;
         end;
   end The_Broker;


end Game.Broker;
