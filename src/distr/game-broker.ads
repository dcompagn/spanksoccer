--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Containers.Vectors;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Game.Broker is

   -- Vettore contenente gli IOR degli utenti registrati.
   package String_Vectors is new Ada.Containers.Vectors
     (Element_Type => Ada.Strings.Unbounded.Unbounded_String,
      Index_Type => Natural);

   Null_String : Unbounded_String :=
     Ada.Strings.Unbounded.To_Unbounded_String("Null");

   -- Risorsa protetta che permette di sicronizzare lettura e scrittura
   -- concorrente sul vettore di IOR, evitando race condition.
   protected Subscription_Container is
      procedure Add (IOR         : Unbounded_String;  Id : out Integer);
      function GetElement (index : Integer) return Unbounded_String;
      function GetFirst return Integer;
      function GetLast return Integer;
      procedure Delete(ID        : Integer);
   private
      IORs                       : String_Vectors.Vector;
   end Subscription_Container;

   -- Procedura che attiva l'ORB. Il luogo del controllo e' il task Main
   procedure Server;

   -- Task Broker, raccolglie lo stato e lo invia ai pannelli sottoscrittori
   -- remoti, secondo il modello di comunicazione push-based.
   task The_Broker;

end Game.Broker;
