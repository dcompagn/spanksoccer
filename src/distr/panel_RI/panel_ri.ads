pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with PolyORB.Std;
with CORBA;
pragma Elaborate_All (CORBA);
with CORBA.Sequences.Unbounded;

package panel_RI is

   Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI:1.0";

   type Position is
     record
         X : CORBA.Long;
         Y : CORBA.Long;
      end record;

   Position_Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI/Position:1.0";

   type Player_Info is
     record
         ID : CORBA.Unsigned_Short;
         Name : CORBA.String;
         Energy : CORBA.Unsigned_Short;
         Foul : CORBA.Unsigned_Short;
         Distance : CORBA.Float;
         Player_Position : panel_RI.Position;
      end record;

   Player_Info_Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI/Player_Info:1.0";

   package IDL_SEQUENCE_panel_RI_Player_Info is
     new CORBA.Sequences.Unbounded
        (panel_RI.Player_Info);

   type PlayerSequence is
     new panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence;

   PlayerSequence_Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI/PlayerSequence:1.0";

   type GameStatus is
     record
         Players_Position : panel_RI.PlayerSequence;
         Ball_Position : panel_RI.Position;
         Referee_Position : panel_RI.Position;
         Time : CORBA.Long;
         Duration : CORBA.Long;
         Score_team_1 : CORBA.Long;
         Score_team_2 : CORBA.Long;
         Control : CORBA.Long;
      end record;

   GameStatus_Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI/GameStatus:1.0";

end panel_RI;
