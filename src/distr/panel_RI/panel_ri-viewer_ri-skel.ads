pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------

package panel_RI.Viewer_RI.Skel is

   procedure Deferred_Initialization;

end panel_RI.Viewer_RI.Skel;
