pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with Ada.Unchecked_Conversion;
with Ada.Unchecked_Deallocation;
with PolyORB.Std;
with PolyORB.Utils.Strings;
with PolyORB.Utils.Strings.Lists;
with PolyORB.Initialization;

package body panel_RI.Helper is

   
   package body Internals is

      ---------------------------
      -- Get_Aggregate_Element --
      ---------------------------

      function Get_Aggregate_Element
        (Acc : not null access Content_�_Position;
         Tc : PolyORB.Any.TypeCode.Object_Ptr;
         Index : PolyORB.Types.Unsigned_Long;
         Mech : not null access PolyORB.Any.Mechanism)
        return PolyORB.Any.Content'Class
      is
         use type PolyORB.Types.Unsigned_Long;
         use type PolyORB.Any.Mechanism;
         pragma Suppress (Validity_Check);
         pragma Unreferenced (Tc);
      begin
         Mech.all :=
           PolyORB.Any.By_Reference;
         case Index is
            when 0 =>
               return CORBA.Wrap
                 (Acc.V.X'Unrestricted_Access);
            when 1 =>
               return CORBA.Wrap
                 (Acc.V.Y'Unrestricted_Access);
            pragma Warnings (Off);
            when others =>
               raise Constraint_Error;
            pragma Warnings (On);

         end case;
      end Get_Aggregate_Element;

      -------------------------
      -- Get_Aggregate_Count --
      -------------------------

      function Get_Aggregate_Count
        (Acc : Content_�_Position)
        return PolyORB.Types.Unsigned_Long
      is
         pragma Unreferenced (Acc);
      begin
         return 2;
      end Get_Aggregate_Count;

      -------------------------
      -- Set_Aggregate_Count --
      -------------------------

      procedure Set_Aggregate_Count
        (Acc : in out Content_�_Position;
         Count : PolyORB.Types.Unsigned_Long)
      is
      begin
         null;
      end Set_Aggregate_Count;

      ---------------------
      -- Unchecked_Get_V --
      ---------------------

      function Unchecked_Get_V
        (Acc : not null access Content_�_Position)
        return PolyORB.Types.Address
      is
         function To_Address
           is new Ada.Unchecked_Conversion
              (Ptr_�_Position,
               PolyORB.Types.Address);

      begin
         return To_Address
           (Acc.V);
      end Unchecked_Get_V;

      -----------
      -- Clone --
      -----------

      function Clone
        (Acc : Content_�_Position;
         Into : PolyORB.Any.Content_Ptr := null)
        return PolyORB.Any.Content_Ptr
      is
         use type PolyORB.Any.Content_Ptr;
         Target : PolyORB.Any.Content_Ptr;
      begin
         if (Into
            /= null)
         then
            if (Into.all
               not in Content_�_Position)
            then
               return null;
            end if;
            Target :=
              Into;
            Content_�_Position
              (Target.all).V.all :=
              Acc.V.all;
         else
            Target :=
              new Content_�_Position;
            Content_�_Position
              (Target.all).V :=
              new panel_RI.Position'
                 (Acc.V.all);
         end if;
         return Target;
      end Clone;

      --------------------
      -- Finalize_Value --
      --------------------

      procedure Finalize_Value
        (Acc : in out Content_�_Position)
      is
         procedure Free
           is new Ada.Unchecked_Deallocation
              (panel_RI.Position,
               Ptr_�_Position);

      begin
         Free
           (Acc.V);
      end Finalize_Value;

      ----------
      -- Wrap --
      ----------

      function Wrap
        (X : access panel_RI.Position)
        return PolyORB.Any.Content'Class
      is
      begin
         return Content_�_Position'
           (PolyORB.Any.Aggregate_Content with
            V => Ptr_�_Position
              (X));
      end Wrap;

      Position_Initialized : PolyORB.Std.Boolean :=
        False;

      -------------------------
      -- Initialize_Position --
      -------------------------

      procedure Initialize_Position is
         Name_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Position");
         Id_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("IDL:panel_RI/Position:1.0");
         Argument_Name_�_X : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("X");
         Argument_Name_�_Y : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Y");
      begin
         if not Position_Initialized
         then
            Position_Initialized :=
              True;
            panel_RI.Helper.TC_Position :=
              CORBA.TypeCode.Internals.To_CORBA_Object
                 (PolyORB.Any.TypeCode.TC_Struct);
            CORBA.Internals.Add_Parameter
              (TC_Position,
               CORBA.To_Any
                 (Name_�));
            CORBA.Internals.Add_Parameter
              (TC_Position,
               CORBA.To_Any
                 (Id_�));
            CORBA.Internals.Add_Parameter
              (TC_Position,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_Position,
               CORBA.To_Any
                 (Argument_Name_�_X));
            CORBA.Internals.Add_Parameter
              (TC_Position,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_Position,
               CORBA.To_Any
                 (Argument_Name_�_Y));
            CORBA.TypeCode.Internals.Disable_Reference_Counting
              (panel_RI.Helper.TC_Position);
         end if;
      end Initialize_Position;

      ---------------------------
      -- Get_Aggregate_Element --
      ---------------------------

      function Get_Aggregate_Element
        (Acc : not null access Content_�_Player_Info;
         Tc : PolyORB.Any.TypeCode.Object_Ptr;
         Index : PolyORB.Types.Unsigned_Long;
         Mech : not null access PolyORB.Any.Mechanism)
        return PolyORB.Any.Content'Class
      is
         use type PolyORB.Types.Unsigned_Long;
         use type PolyORB.Any.Mechanism;
         pragma Suppress (Validity_Check);
         pragma Unreferenced (Tc);
      begin
         Mech.all :=
           PolyORB.Any.By_Reference;
         case Index is
            when 0 =>
               return CORBA.Wrap
                 (Acc.V.ID'Unrestricted_Access);
            when 1 =>
               return CORBA.Wrap
                 (Acc.V.Name'Unrestricted_Access);
            when 2 =>
               return CORBA.Wrap
                 (Acc.V.Energy'Unrestricted_Access);
            when 3 =>
               return CORBA.Wrap
                 (Acc.V.Foul'Unrestricted_Access);
            when 4 =>
               return CORBA.Wrap
                 (Acc.V.Distance'Unrestricted_Access);
            when 5 =>
               return panel_RI.Helper.Internals.Wrap
                 (Acc.V.Player_Position'Unrestricted_Access);
            pragma Warnings (Off);
            when others =>
               raise Constraint_Error;
            pragma Warnings (On);

         end case;
      end Get_Aggregate_Element;

      -------------------------
      -- Get_Aggregate_Count --
      -------------------------

      function Get_Aggregate_Count
        (Acc : Content_�_Player_Info)
        return PolyORB.Types.Unsigned_Long
      is
         pragma Unreferenced (Acc);
      begin
         return 6;
      end Get_Aggregate_Count;

      -------------------------
      -- Set_Aggregate_Count --
      -------------------------

      procedure Set_Aggregate_Count
        (Acc : in out Content_�_Player_Info;
         Count : PolyORB.Types.Unsigned_Long)
      is
      begin
         null;
      end Set_Aggregate_Count;

      ---------------------
      -- Unchecked_Get_V --
      ---------------------

      function Unchecked_Get_V
        (Acc : not null access Content_�_Player_Info)
        return PolyORB.Types.Address
      is
         function To_Address
           is new Ada.Unchecked_Conversion
              (Ptr_�_Player_Info,
               PolyORB.Types.Address);

      begin
         return To_Address
           (Acc.V);
      end Unchecked_Get_V;

      -----------
      -- Clone --
      -----------

      function Clone
        (Acc : Content_�_Player_Info;
         Into : PolyORB.Any.Content_Ptr := null)
        return PolyORB.Any.Content_Ptr
      is
         use type PolyORB.Any.Content_Ptr;
         Target : PolyORB.Any.Content_Ptr;
      begin
         if (Into
            /= null)
         then
            if (Into.all
               not in Content_�_Player_Info)
            then
               return null;
            end if;
            Target :=
              Into;
            Content_�_Player_Info
              (Target.all).V.all :=
              Acc.V.all;
         else
            Target :=
              new Content_�_Player_Info;
            Content_�_Player_Info
              (Target.all).V :=
              new panel_RI.Player_Info'
                 (Acc.V.all);
         end if;
         return Target;
      end Clone;

      --------------------
      -- Finalize_Value --
      --------------------

      procedure Finalize_Value
        (Acc : in out Content_�_Player_Info)
      is
         procedure Free
           is new Ada.Unchecked_Deallocation
              (panel_RI.Player_Info,
               Ptr_�_Player_Info);

      begin
         Free
           (Acc.V);
      end Finalize_Value;

      ----------
      -- Wrap --
      ----------

      function Wrap
        (X : access panel_RI.Player_Info)
        return PolyORB.Any.Content'Class
      is
      begin
         return Content_�_Player_Info'
           (PolyORB.Any.Aggregate_Content with
            V => Ptr_�_Player_Info
              (X));
      end Wrap;

      Player_Info_Initialized : PolyORB.Std.Boolean :=
        False;

      ----------------------------
      -- Initialize_Player_Info --
      ----------------------------

      procedure Initialize_Player_Info is
         Name_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Player_Info");
         Id_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("IDL:panel_RI/Player_Info:1.0");
         Argument_Name_�_ID : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("ID");
         Argument_Name_�_Name : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Name");
         Argument_Name_�_Energy : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Energy");
         Argument_Name_�_Foul : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Foul");
         Argument_Name_�_Distance : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Distance");
         Argument_Name_�_Player_Position : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Player_Position");
      begin
         if not Player_Info_Initialized
         then
            Player_Info_Initialized :=
              True;
            panel_RI.Helper.TC_Player_Info :=
              CORBA.TypeCode.Internals.To_CORBA_Object
                 (PolyORB.Any.TypeCode.TC_Struct);
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Name_�));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Id_�));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (CORBA.TC_Unsigned_Short));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Argument_Name_�_ID));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (CORBA.TC_String));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Argument_Name_�_Name));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (CORBA.TC_Unsigned_Short));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Argument_Name_�_Energy));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (CORBA.TC_Unsigned_Short));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Argument_Name_�_Foul));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (CORBA.TC_Float));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Argument_Name_�_Distance));
            panel_RI.Helper.Internals.Initialize_Position;
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (panel_RI.Helper.TC_Position));
            CORBA.Internals.Add_Parameter
              (TC_Player_Info,
               CORBA.To_Any
                 (Argument_Name_�_Player_Position));
            CORBA.TypeCode.Internals.Disable_Reference_Counting
              (panel_RI.Helper.TC_Player_Info);
         end if;
      end Initialize_Player_Info;

      ----------------------------------------------------
      -- IDL_SEQUENCE_panel_RI_Player_Info_Element_Wrap --
      ----------------------------------------------------

      function IDL_SEQUENCE_panel_RI_Player_Info_Element_Wrap
        (X : access panel_RI.Player_Info)
        return PolyORB.Any.Content'Class
      is
      begin
         return panel_RI.Helper.Internals.Wrap
           (X.all'Unrestricted_Access);
      end IDL_SEQUENCE_panel_RI_Player_Info_Element_Wrap;

      function Wrap
        (X : access panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence)
        return PolyORB.Any.Content'Class
        renames IDL_SEQUENCE_panel_RI_Player_Info_Helper.Wrap;

      IDL_SEQUENCE_panel_RI_Player_Info_Initialized : PolyORB.Std.Boolean :=
        False;

      --------------------------------------------------
      -- Initialize_IDL_SEQUENCE_panel_RI_Player_Info --
      --------------------------------------------------

      procedure Initialize_IDL_SEQUENCE_panel_RI_Player_Info is
      begin
         if not IDL_SEQUENCE_panel_RI_Player_Info_Initialized
         then
            IDL_SEQUENCE_panel_RI_Player_Info_Initialized :=
              True;
            panel_RI.Helper.Internals.Initialize_Player_Info;
            panel_RI.Helper.TC_IDL_SEQUENCE_panel_RI_Player_Info :=
              CORBA.TypeCode.Internals.Build_Sequence_TC
                 (panel_RI.Helper.TC_Player_Info,
                  0);
            CORBA.TypeCode.Internals.Disable_Reference_Counting
              (panel_RI.Helper.TC_IDL_SEQUENCE_panel_RI_Player_Info);
            IDL_SEQUENCE_panel_RI_Player_Info_Helper.Initialize
              (Element_TC => panel_RI.Helper.TC_Player_Info,
               Sequence_TC => panel_RI.Helper.TC_IDL_SEQUENCE_panel_RI_Player_Info);
         end if;
      end Initialize_IDL_SEQUENCE_panel_RI_Player_Info;

      PlayerSequence_Initialized : PolyORB.Std.Boolean :=
        False;

      -------------------------------
      -- Initialize_PlayerSequence --
      -------------------------------

      procedure Initialize_PlayerSequence is
         Name_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("PlayerSequence");
         Id_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("IDL:panel_RI/PlayerSequence:1.0");
      begin
         if not PlayerSequence_Initialized
         then
            PlayerSequence_Initialized :=
              True;
            panel_RI.Helper.Internals.Initialize_IDL_SEQUENCE_panel_RI_Player_Info;
            panel_RI.Helper.TC_PlayerSequence :=
              CORBA.TypeCode.Internals.Build_Alias_TC
                 (Name => Name_�,
                  Id => Id_�,
                  Parent => panel_RI.Helper.TC_IDL_SEQUENCE_panel_RI_Player_Info);
            CORBA.TypeCode.Internals.Disable_Reference_Counting
              (panel_RI.Helper.TC_PlayerSequence);
         end if;
      end Initialize_PlayerSequence;

      ---------------------------
      -- Get_Aggregate_Element --
      ---------------------------

      function Get_Aggregate_Element
        (Acc : not null access Content_�_GameStatus;
         Tc : PolyORB.Any.TypeCode.Object_Ptr;
         Index : PolyORB.Types.Unsigned_Long;
         Mech : not null access PolyORB.Any.Mechanism)
        return PolyORB.Any.Content'Class
      is
         use type PolyORB.Types.Unsigned_Long;
         use type PolyORB.Any.Mechanism;
         pragma Suppress (Validity_Check);
         pragma Unreferenced (Tc);
      begin
         Mech.all :=
           PolyORB.Any.By_Reference;
         case Index is
            when 0 =>
               return panel_RI.Helper.Internals.Wrap
                 (panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence
                    (Acc.V.Players_Position)'Unrestricted_Access);
            when 1 =>
               return panel_RI.Helper.Internals.Wrap
                 (Acc.V.Ball_Position'Unrestricted_Access);
            when 2 =>
               return panel_RI.Helper.Internals.Wrap
                 (Acc.V.Referee_Position'Unrestricted_Access);
            when 3 =>
               return CORBA.Wrap
                 (Acc.V.Time'Unrestricted_Access);
            when 4 =>
               return CORBA.Wrap
                 (Acc.V.Duration'Unrestricted_Access);
            when 5 =>
               return CORBA.Wrap
                 (Acc.V.Score_team_1'Unrestricted_Access);
            when 6 =>
               return CORBA.Wrap
                 (Acc.V.Score_team_2'Unrestricted_Access);
            when 7 =>
               return CORBA.Wrap
                 (Acc.V.Control'Unrestricted_Access);
            pragma Warnings (Off);
            when others =>
               raise Constraint_Error;
            pragma Warnings (On);

         end case;
      end Get_Aggregate_Element;

      -------------------------
      -- Get_Aggregate_Count --
      -------------------------

      function Get_Aggregate_Count
        (Acc : Content_�_GameStatus)
        return PolyORB.Types.Unsigned_Long
      is
         pragma Unreferenced (Acc);
      begin
         return 8;
      end Get_Aggregate_Count;

      -------------------------
      -- Set_Aggregate_Count --
      -------------------------

      procedure Set_Aggregate_Count
        (Acc : in out Content_�_GameStatus;
         Count : PolyORB.Types.Unsigned_Long)
      is
      begin
         null;
      end Set_Aggregate_Count;

      ---------------------
      -- Unchecked_Get_V --
      ---------------------

      function Unchecked_Get_V
        (Acc : not null access Content_�_GameStatus)
        return PolyORB.Types.Address
      is
         function To_Address
           is new Ada.Unchecked_Conversion
              (Ptr_�_GameStatus,
               PolyORB.Types.Address);

      begin
         return To_Address
           (Acc.V);
      end Unchecked_Get_V;

      -----------
      -- Clone --
      -----------

      function Clone
        (Acc : Content_�_GameStatus;
         Into : PolyORB.Any.Content_Ptr := null)
        return PolyORB.Any.Content_Ptr
      is
         use type PolyORB.Any.Content_Ptr;
         Target : PolyORB.Any.Content_Ptr;
      begin
         if (Into
            /= null)
         then
            if (Into.all
               not in Content_�_GameStatus)
            then
               return null;
            end if;
            Target :=
              Into;
            Content_�_GameStatus
              (Target.all).V.all :=
              Acc.V.all;
         else
            Target :=
              new Content_�_GameStatus;
            Content_�_GameStatus
              (Target.all).V :=
              new panel_RI.GameStatus'
                 (Acc.V.all);
         end if;
         return Target;
      end Clone;

      --------------------
      -- Finalize_Value --
      --------------------

      procedure Finalize_Value
        (Acc : in out Content_�_GameStatus)
      is
         procedure Free
           is new Ada.Unchecked_Deallocation
              (panel_RI.GameStatus,
               Ptr_�_GameStatus);

      begin
         Free
           (Acc.V);
      end Finalize_Value;

      ----------
      -- Wrap --
      ----------

      function Wrap
        (X : access panel_RI.GameStatus)
        return PolyORB.Any.Content'Class
      is
      begin
         return Content_�_GameStatus'
           (PolyORB.Any.Aggregate_Content with
            V => Ptr_�_GameStatus
              (X));
      end Wrap;

      GameStatus_Initialized : PolyORB.Std.Boolean :=
        False;

      ---------------------------
      -- Initialize_GameStatus --
      ---------------------------

      procedure Initialize_GameStatus is
         Name_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("GameStatus");
         Id_� : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("IDL:panel_RI/GameStatus:1.0");
         Argument_Name_�_Players_Position : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Players_Position");
         Argument_Name_�_Ball_Position : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Ball_Position");
         Argument_Name_�_Referee_Position : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Referee_Position");
         Argument_Name_�_Time : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Time");
         Argument_Name_�_Duration : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Duration");
         Argument_Name_�_Score_team_1 : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Score_team_1");
         Argument_Name_�_Score_team_2 : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Score_team_2");
         Argument_Name_�_Control : constant CORBA.String :=
           CORBA.To_CORBA_String
              ("Control");
      begin
         if not GameStatus_Initialized
         then
            GameStatus_Initialized :=
              True;
            panel_RI.Helper.TC_GameStatus :=
              CORBA.TypeCode.Internals.To_CORBA_Object
                 (PolyORB.Any.TypeCode.TC_Struct);
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Name_�));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Id_�));
            panel_RI.Helper.Internals.Initialize_PlayerSequence;
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (panel_RI.Helper.TC_PlayerSequence));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Players_Position));
            panel_RI.Helper.Internals.Initialize_Position;
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (panel_RI.Helper.TC_Position));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Ball_Position));
            panel_RI.Helper.Internals.Initialize_Position;
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (panel_RI.Helper.TC_Position));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Referee_Position));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Time));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Duration));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Score_team_1));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Score_team_2));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (CORBA.TC_Long));
            CORBA.Internals.Add_Parameter
              (TC_GameStatus,
               CORBA.To_Any
                 (Argument_Name_�_Control));
            CORBA.TypeCode.Internals.Disable_Reference_Counting
              (panel_RI.Helper.TC_GameStatus);
         end if;
      end Initialize_GameStatus;

   end Internals;

   --------------
   -- From_Any --
   --------------

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.Position
   is
   begin
      return (X => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            0)),
      Y => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            1)));
   end From_Any;

   ------------
   -- To_Any --
   ------------

   function To_Any
     (Item : panel_RI.Position)
     return CORBA.Any
   is
      Result : CORBA.Any :=
        CORBA.Internals.Get_Empty_Any_Aggregate
           (TC_Position);
   begin
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.X));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Y));
      return Result;
   end To_Any;

   --------------
   -- From_Any --
   --------------

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.Player_Info
   is
   begin
      return (ID => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Unsigned_Short,
            0)),
      Name => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_String,
            1)),
      Energy => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Unsigned_Short,
            2)),
      Foul => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Unsigned_Short,
            3)),
      Distance => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Float,
            4)),
      Player_Position => panel_RI.Helper.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            panel_RI.Helper.TC_Position,
            5)));
   end From_Any;

   ------------
   -- To_Any --
   ------------

   function To_Any
     (Item : panel_RI.Player_Info)
     return CORBA.Any
   is
      Result : CORBA.Any :=
        CORBA.Internals.Get_Empty_Any_Aggregate
           (TC_Player_Info);
   begin
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.ID));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Name));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Energy));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Foul));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Distance));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         panel_RI.Helper.To_Any
           (Item.Player_Position));
      return Result;
   end To_Any;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence
     renames panel_RI.Helper.Internals.IDL_SEQUENCE_panel_RI_Player_Info_Helper.From_Any;

   function To_Any
     (Item : panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence)
     return CORBA.Any
     renames panel_RI.Helper.Internals.IDL_SEQUENCE_panel_RI_Player_Info_Helper.To_Any;

   --------------
   -- From_Any --
   --------------

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.PlayerSequence
   is
      Result : constant panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence :=
        panel_RI.Helper.From_Any
           (Item);
   begin
      return panel_RI.PlayerSequence
        (Result);
   end From_Any;

   ------------
   -- To_Any --
   ------------

   function To_Any
     (Item : panel_RI.PlayerSequence)
     return CORBA.Any
   is
      Result : CORBA.Any :=
        panel_RI.Helper.To_Any
           (panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence
              (Item));
   begin
      CORBA.Internals.Set_Type
        (Result,
         TC_PlayerSequence);
      return Result;
   end To_Any;

   --------------
   -- From_Any --
   --------------

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.GameStatus
   is
   begin
      return (Players_Position => panel_RI.Helper.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            panel_RI.Helper.TC_PlayerSequence,
            0)),
      Ball_Position => panel_RI.Helper.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            panel_RI.Helper.TC_Position,
            1)),
      Referee_Position => panel_RI.Helper.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            panel_RI.Helper.TC_Position,
            2)),
      Time => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            3)),
      Duration => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            4)),
      Score_team_1 => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            5)),
      Score_team_2 => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            6)),
      Control => CORBA.From_Any
        (CORBA.Internals.Get_Aggregate_Element
           (Item,
            CORBA.TC_Long,
            7)));
   end From_Any;

   ------------
   -- To_Any --
   ------------

   function To_Any
     (Item : panel_RI.GameStatus)
     return CORBA.Any
   is
      Result : CORBA.Any :=
        CORBA.Internals.Get_Empty_Any_Aggregate
           (TC_GameStatus);
   begin
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         panel_RI.Helper.To_Any
           (Item.Players_Position));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         panel_RI.Helper.To_Any
           (Item.Ball_Position));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         panel_RI.Helper.To_Any
           (Item.Referee_Position));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Time));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Duration));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Score_team_1));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Score_team_2));
      CORBA.Internals.Add_Aggregate_Element
        (Result,
         CORBA.To_Any
           (Item.Control));
      return Result;
   end To_Any;

   -----------------------------
   -- Deferred_Initialization --
   -----------------------------

   procedure Deferred_Initialization is
   begin
      panel_RI.Helper.Internals.Initialize_Position;
      panel_RI.Helper.Internals.Initialize_Player_Info;
      panel_RI.Helper.Internals.Initialize_IDL_SEQUENCE_panel_RI_Player_Info;
      panel_RI.Helper.Internals.Initialize_PlayerSequence;
      panel_RI.Helper.Internals.Initialize_GameStatus;
   end Deferred_Initialization;

begin
   declare
      use PolyORB.Utils.Strings;
      use PolyORB.Utils.Strings.Lists;
   begin
      PolyORB.Initialization.Register_Module
        (PolyORB.Initialization.Module_Info'
           (Name => +"panel_RI.Helper",
            Conflicts => PolyORB.Utils.Strings.Lists.Empty,
            Depends => +"any"
               & "corba",
            Provides => PolyORB.Utils.Strings.Lists.Empty,
            Implicit => False,
            Init => Deferred_Initialization'Access,
            Shutdown => null));
   end;
end panel_RI.Helper;
