pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with CORBA.Object;
with PolyORB.Std;
with CORBA;
pragma Elaborate_All (CORBA);

package panel_RI.Viewer_RI is

   type Ref is
     new CORBA.Object.Ref with null record;

   Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI/Viewer_RI:1.0";

   procedure publish_status
     (Self : Ref;
      Status : panel_RI.GameStatus);

   publish_status_Repository_Id : constant PolyORB.Std.String :=
     "IDL:panel_RI/Viewer_RI/publish_status:1.0";

   function Is_A
     (Self : Ref;
      Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean;

private
   function Is_A
     (Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean;

end panel_RI.Viewer_RI;
