pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with CORBA;
pragma Elaborate_All (CORBA);
with PolyORB.Any;
with PolyORB.Types;
with PolyORB.Sequences.Unbounded.CORBA_Helper;
pragma Elaborate_All (PolyORB.Sequences.Unbounded.CORBA_Helper);

package panel_RI.Helper is

   TC_Position : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.Position;

   function To_Any
     (Item : panel_RI.Position)
     return CORBA.Any;

   TC_Player_Info : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.Player_Info;

   function To_Any
     (Item : panel_RI.Player_Info)
     return CORBA.Any;

   TC_IDL_SEQUENCE_panel_RI_Player_Info : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence;

   function To_Any
     (Item : panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence)
     return CORBA.Any;

   TC_PlayerSequence : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.PlayerSequence;

   function To_Any
     (Item : panel_RI.PlayerSequence)
     return CORBA.Any;

   TC_GameStatus : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.GameStatus;

   function To_Any
     (Item : panel_RI.GameStatus)
     return CORBA.Any;

   
   package Internals is

      type Ptr_�_Position is
        access all panel_RI.Position;

      type Content_�_Position is
        new PolyORB.Any.Aggregate_Content with record
            V : Ptr_�_Position;
         end record;

      function Get_Aggregate_Element
        (Acc : not null access Content_�_Position;
         Tc : PolyORB.Any.TypeCode.Object_Ptr;
         Index : PolyORB.Types.Unsigned_Long;
         Mech : not null access PolyORB.Any.Mechanism)
        return PolyORB.Any.Content'Class;

      function Get_Aggregate_Count
        (Acc : Content_�_Position)
        return PolyORB.Types.Unsigned_Long;

      procedure Set_Aggregate_Count
        (Acc : in out Content_�_Position;
         Count : PolyORB.Types.Unsigned_Long);

      function Unchecked_Get_V
        (Acc : not null access Content_�_Position)
        return PolyORB.Types.Address;

      function Clone
        (Acc : Content_�_Position;
         Into : PolyORB.Any.Content_Ptr := null)
        return PolyORB.Any.Content_Ptr;

      procedure Finalize_Value
        (Acc : in out Content_�_Position);

      function Wrap
        (X : access panel_RI.Position)
        return PolyORB.Any.Content'Class;

      procedure Initialize_Position;

      type Ptr_�_Player_Info is
        access all panel_RI.Player_Info;

      type Content_�_Player_Info is
        new PolyORB.Any.Aggregate_Content with record
            V : Ptr_�_Player_Info;
         end record;

      function Get_Aggregate_Element
        (Acc : not null access Content_�_Player_Info;
         Tc : PolyORB.Any.TypeCode.Object_Ptr;
         Index : PolyORB.Types.Unsigned_Long;
         Mech : not null access PolyORB.Any.Mechanism)
        return PolyORB.Any.Content'Class;

      function Get_Aggregate_Count
        (Acc : Content_�_Player_Info)
        return PolyORB.Types.Unsigned_Long;

      procedure Set_Aggregate_Count
        (Acc : in out Content_�_Player_Info;
         Count : PolyORB.Types.Unsigned_Long);

      function Unchecked_Get_V
        (Acc : not null access Content_�_Player_Info)
        return PolyORB.Types.Address;

      function Clone
        (Acc : Content_�_Player_Info;
         Into : PolyORB.Any.Content_Ptr := null)
        return PolyORB.Any.Content_Ptr;

      procedure Finalize_Value
        (Acc : in out Content_�_Player_Info);

      function Wrap
        (X : access panel_RI.Player_Info)
        return PolyORB.Any.Content'Class;

      procedure Initialize_Player_Info;

      function IDL_SEQUENCE_panel_RI_Player_Info_Element_Wrap
        (X : access panel_RI.Player_Info)
        return PolyORB.Any.Content'Class;

      function Wrap
        (X : access panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.Sequence)
        return PolyORB.Any.Content'Class;

      package IDL_SEQUENCE_panel_RI_Player_Info_Helper is
        new panel_RI.IDL_SEQUENCE_panel_RI_Player_Info.CORBA_Helper
           (Element_From_Any => panel_RI.Helper.From_Any,
            Element_To_Any => panel_RI.Helper.To_Any,
            Element_Wrap => panel_RI.Helper.Internals.IDL_SEQUENCE_panel_RI_Player_Info_Element_Wrap);

      procedure Initialize_IDL_SEQUENCE_panel_RI_Player_Info;

      procedure Initialize_PlayerSequence;

      type Ptr_�_GameStatus is
        access all panel_RI.GameStatus;

      type Content_�_GameStatus is
        new PolyORB.Any.Aggregate_Content with record
            V : Ptr_�_GameStatus;
         end record;

      function Get_Aggregate_Element
        (Acc : not null access Content_�_GameStatus;
         Tc : PolyORB.Any.TypeCode.Object_Ptr;
         Index : PolyORB.Types.Unsigned_Long;
         Mech : not null access PolyORB.Any.Mechanism)
        return PolyORB.Any.Content'Class;

      function Get_Aggregate_Count
        (Acc : Content_�_GameStatus)
        return PolyORB.Types.Unsigned_Long;

      procedure Set_Aggregate_Count
        (Acc : in out Content_�_GameStatus;
         Count : PolyORB.Types.Unsigned_Long);

      function Unchecked_Get_V
        (Acc : not null access Content_�_GameStatus)
        return PolyORB.Types.Address;

      function Clone
        (Acc : Content_�_GameStatus;
         Into : PolyORB.Any.Content_Ptr := null)
        return PolyORB.Any.Content_Ptr;

      procedure Finalize_Value
        (Acc : in out Content_�_GameStatus);

      function Wrap
        (X : access panel_RI.GameStatus)
        return PolyORB.Any.Content'Class;

      procedure Initialize_GameStatus;

   end Internals;

end panel_RI.Helper;
