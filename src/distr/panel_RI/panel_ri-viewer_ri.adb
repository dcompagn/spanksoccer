pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with PolyORB.Any.NVList;
with PolyORB.Types;
with panel_RI.Helper;
with PolyORB.Any;
with PolyORB.Requests;
with PolyORB.CORBA_P.Interceptors_Hooks;
with PolyORB.CORBA_P.Exceptions;

package body panel_RI.Viewer_RI is

   publish_status_Arg_Name_Status_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Status");

   publish_status_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   -----------------------------
   -- publish_status_Result_� --
   -----------------------------

   function publish_status_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (publish_status_Result_�);
   begin
      return (Name => publish_status_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Void),
      Arg_Modes => 0);
   end publish_status_Result_�;

   --------------------
   -- publish_status --
   --------------------

   procedure publish_status
     (Self : Ref;
      Status : panel_RI.GameStatus)
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Arg_CC_Status_� : aliased PolyORB.Any.Content'Class :=
        panel_RI.Helper.Internals.Wrap
           (Status'Unrestricted_Access);
      Arg_Any_Status_� : constant CORBA.Any :=
        CORBA.Internals.Get_Wrapper_Any
           (panel_RI.Helper.TC_GameStatus,
            Arg_CC_Status_�'Unchecked_Access);
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        publish_status_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Fill the Argument list
      PolyORB.Any.NVList.Add_Item
        (Argument_List_�,
         publish_status_Arg_Name_Status_�,
         PolyORB.Any.Copy_Any
           (PolyORB.Any.Any
              (Arg_Any_Status_�)),
         PolyORB.Any.ARG_IN);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "publish_status",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�,
         Req_Flags => PolyORB.Requests.Sync_With_Transport);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
   end publish_status;

   ----------
   -- Is_A --
   ----------

   function Is_A
     (Self : Ref;
      Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean
   is
   begin
      return (False
         or else (Is_A
           (Logical_Type_Id)
            or else CORBA.Object.Is_A
              (CORBA.Object.Ref
                 (Self),
               Logical_Type_Id)));
   end Is_A;

   ----------
   -- Is_A --
   ----------

   function Is_A
     (Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean
   is
   begin
      return ((CORBA.Is_Equivalent
        (Logical_Type_Id,
         panel_RI.Viewer_RI.Repository_Id)
         or else CORBA.Is_Equivalent
           (Logical_Type_Id,
            "IDL:omg.org/CORBA/Object:1.0"))
         or else False);
   end Is_A;

end panel_RI.Viewer_RI;
