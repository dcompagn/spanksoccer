pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  panel.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with CORBA;
pragma Elaborate_All (CORBA);
with CORBA.Object;

package panel_RI.Viewer_RI.Helper is

   TC_Viewer_RI : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return panel_RI.Viewer_RI.Ref;

   function To_Any
     (Item : panel_RI.Viewer_RI.Ref)
     return CORBA.Any;

   function Unchecked_To_Ref
     (The_Ref : CORBA.Object.Ref'Class)
     return panel_RI.Viewer_RI.Ref;

   function To_Ref
     (The_Ref : CORBA.Object.Ref'Class)
     return panel_RI.Viewer_RI.Ref;

   
   package Internals is

      procedure Initialize_Viewer_RI;

   end Internals;

end panel_RI.Viewer_RI.Helper;
