--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

package body Game.Utils is

   function Position_T_To_RI_Position (position_T_in : Position_T)
                                       return panel_RI.Position is

      temp_position : panel_RI.Position;

   begin

      temp_position.X := CORBA.Long(position_T_in.X);
      temp_position.Y := CORBA.Long(position_T_in.Y);

      return temp_position;

   end Position_T_To_RI_Position;

   function Player_Wrapper_T_To_RI_Player_Info (player_T_in : Player_Wrapper_T)
                                                return panel_RI.Player_Info is
      temp_player_info : panel_RI.Player_Info;

   begin

      temp_player_info.ID := CORBA.Unsigned_Short(player_T_in.ID);
      temp_player_info.Name := CORBA.String(player_T_in.Name);
      temp_player_info.Energy := CORBA.Unsigned_Short(player_T_in.Energy);
      temp_player_info.Foul := CORBA.Unsigned_Short(player_T_in.Foul);
      temp_player_info.Distance := CORBA.Float(player_T_in.Distance);
      temp_player_info.Player_Position := Position_T_To_RI_Position(player_T_in.Position);

      return temp_player_info;

   end Player_Wrapper_T_To_RI_Player_Info;

   function Player_Sequence_Warpper_T_To_RI_PlayerSequence (player_sequence_T_in : Player_Sequence_Wrapper_T )
                                                            return panel_RI.PlayerSequence is

      temp_PlayerSequence : panel_RI.PlayerSequence;
      index: Positive;

   begin

      index := 1;

      while index <= Player_sequence_T_in'Length loop
         panel_RI.Append(temp_PlayerSequence, Player_Wrapper_T_To_RI_Player_Info(player_sequence_T_in(index)));
         index := index + 1;
      end loop;

      return temp_PlayerSequence;

   end Player_Sequence_Warpper_T_To_RI_PlayerSequence;

   function file_exists(name :String ) return Boolean is
      use Ada.Text_IO;
      input_file:File_Type;
   begin
      Open(input_file, in_file, name);
      --  if it worked, close the file and return true
      Close(input_file);
      return True;
   exception
      when Name_Error =>
         --  couldn't open the file
         return False;
   end file_exists;

   function write_to_file( IOR : String; Name_file_in :String ) return Boolean is
      use Ada.Text_IO;
      InputFile : File_Type;
   begin
   Open(File => InputFile,
     Mode => Ada.Text_IO.Out_File,
        Name => Name_file_in);

      Put(File => InputFile, Item => IOR);

      Close(File => InputFile);

      return true;
   exception
      when Ada.IO_Exceptions.Name_Error => return False;
   end write_to_file;

end Game.Utils;
