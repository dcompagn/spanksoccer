--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with panel_RI;
with Game.Logic; use Game.Logic;
with CORBA;
with Ada.Text_IO;
with Ada.IO_Exceptions;


-- Questo package fornisce alcune funzioni di utilita' per la distribuzione
package Game.Utils is

   -- Data una Position_T converte in panel_RI.Position (CORBA)
   function Position_T_To_RI_Position (position_T_in : Position_T)
                                       return panel_RI.Position;

   -- Dato un Player_Wrapper_T converte in panel_RI.RI_Player_Info (CORBA)
   function Player_Wrapper_T_To_RI_Player_Info (player_T_in : Player_Wrapper_T)
                                                return panel_RI.Player_Info;

   -- Dato un array Player_Sequence_Warpper_T converte in
   -- panel_RI.RI_PlayerSequence CORBA
   function Player_Sequence_Warpper_T_To_RI_PlayerSequence
     (player_sequence_T_in : Player_Sequence_Wrapper_T )
      return panel_RI.PlayerSequence;

    -- Controlla l'esistenza del file "name" e ritorna true se esiste
   function file_exists(name :String ) return Boolean;

   -- Scrive una stringa IOR sul file "Name_file_in"
   function write_to_file( IOR : String; Name_file_in :String ) return Boolean;

end Game.Utils;
