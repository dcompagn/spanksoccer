--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.0                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;
with control_RI.manager_RI.Skel;
with CORBA.ORB;

pragma Warnings (Off, control_RI.manager_RI.Skel);
with Game.Logic.Mediator; use Game.Logic.Mediator;
with Game.Status; use Game.Status;
with Game.Logic.Features;
with Game; use Game;
with Game.Logic.Action; use Game.Logic.Action;

package body control_RI.manager_RI.impl is

function init_game
     (Self : access Object;
      player_number : CORBA.Long;
      tactic : CORBA.Long)
      return CORBA.Boolean is

      n_players : Positive := Positive(player_number);
      k_tactic : Natural := Natural(tactic);

      Players_Feature : Players_Feature_T;
   begin
      Put_Line("Numero giocatori : " & Positive'Image(n_players));

      case k_tactic is
      when 0 =>
         -- Non puo' essere fatto nella create mediator?
         Players_Feature := Game.Logic.Features.Load_Features("features_std.xml");
         Status_T.Start_Match(Players_Number => n_players * 2,
                             Players_Feature => Players_Feature,
                             Scheme => Game.Logic.Stdr);
      when 1 =>
         Players_Feature := Game.Logic.Features.Load_Features("features_off.xml");
         Status_T.Start_Match(Players_Number => n_players * 2,
                             Players_Feature => Players_Feature,
                             Scheme => Game.Logic.Offn);
      when 2 =>
         Players_Feature := Game.Logic.Features.Load_Features("features_def.xml");
         Status_T.Start_Match(Players_Number => n_players * 2,
                             Players_Feature => Players_Feature,
                             Scheme => Game.Logic.Defn);
      when others =>
         Players_Feature := Game.Logic.Features.Load_Features("features_std.xml");
         Status_T.Start_Match(Players_Number => n_players * 2,
                             Players_Feature => Players_Feature,
                             Scheme => Game.Logic.Stdr);
      end case;

      Put_Line("Start");
      return True;
   end init_game;

   procedure start (Self : access Object) is

   begin
      null;
   end start;

   procedure substitute
     (Self : access Object;
      player_id : CORBA.Long) is

      id_subst : Positive := Positive(player_id);
   begin
      if id_subst >= Positive(Player_ID_T'First) and id_subst <= Positive(Player_ID_T'Last) then
         Status_T.Manager(Create_Replace_Player(Natural(id_subst)));
         Put_Line("Sostituzione ID: " & Positive'Image(id_subst));
      else
         Put_Line("Sostituzione ID non valido");
      end if;
   end substitute;


   procedure shutdown
     (Self :  access Object) is
   begin
      Status_T.Shut_Down;
      CORBA.ORB.shutdown(false);
   end shutdown;


end control_RI.manager_RI.impl;
