pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with CORBA.Object;
with PolyORB.Std;
with CORBA;
pragma Elaborate_All (CORBA);

package control_RI.subscriber_RI is

   type Ref is
     new CORBA.Object.Ref with null record;

   Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/subscriber_RI:1.0";

   function subscribe
     (Self : Ref;
      IOR : CORBA.String)
     return CORBA.Long;

   subscribe_Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/subscriber_RI/subscribe:1.0";

   procedure unsubcribe
     (Self : Ref;
      Panel_ID : CORBA.Long);

   unsubcribe_Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/subscriber_RI/unsubcribe:1.0";

   function Is_A
     (Self : Ref;
      Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean;

private
   function Is_A
     (Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean;

end control_RI.subscriber_RI;
