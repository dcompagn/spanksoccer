pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with CORBA;
pragma Elaborate_All (CORBA);
with CORBA.Object;

package control_RI.manager_RI.Helper is

   TC_manager_RI : CORBA.TypeCode.Object;

   function From_Any
     (Item : CORBA.Any)
     return control_RI.manager_RI.Ref;

   function To_Any
     (Item : control_RI.manager_RI.Ref)
     return CORBA.Any;

   function Unchecked_To_Ref
     (The_Ref : CORBA.Object.Ref'Class)
     return control_RI.manager_RI.Ref;

   function To_Ref
     (The_Ref : CORBA.Object.Ref'Class)
     return control_RI.manager_RI.Ref;

   
   package Internals is

      procedure Initialize_manager_RI;

   end Internals;

end control_RI.manager_RI.Helper;
