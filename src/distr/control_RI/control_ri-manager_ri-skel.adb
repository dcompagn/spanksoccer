pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with control_RI.manager_RI.Impl;
with CORBA;
pragma Elaborate_All (CORBA);
with PolyORB.Any;
with CORBA.NVList;
with CORBA.ServerRequest;
with PolyORB.CORBA_P.IR_Hooks;
with CORBA.Object;
with CORBA.Object.Helper;
with PolyORB.CORBA_P.Domain_Management;
with control_RI_manager_RI_Hash;
with PolyORB.Std;
with PortableServer;
with CORBA.ORB;
with PolyORB.CORBA_P.Exceptions;
with PolyORB.Qos.Exception_Informations;
with PolyORB.Utils.Strings;
with PolyORB.Utils.Strings.Lists;
with PolyORB.Initialization;

package body control_RI.manager_RI.Skel is

   init_game_Arg_Name_player_number_� : constant CORBA.Identifier :=
     CORBA.To_CORBA_String
        ("player_number");

   init_game_Arg_Name_tactic_� : constant CORBA.Identifier :=
     CORBA.To_CORBA_String
        ("tactic");

   substitute_Arg_Name_player_id_� : constant CORBA.Identifier :=
     CORBA.To_CORBA_String
        ("player_id");

   N_Operations : constant PolyORB.Std.Natural :=
     9;

   type String_Ptr is
     access PolyORB.Std.String;

   Invoke_Db : array (0 .. (N_Operations
      - 1)) of String_Ptr :=
     (others => null);

   procedure Register_Procedure
     (Operation_Name : PolyORB.Std.String);

   ------------------------
   -- Register_Procedure --
   ------------------------

   procedure Register_Procedure
     (Operation_Name : PolyORB.Std.String)
   is
      Index_� : PolyORB.Std.Natural;
      Invoke_Name_Access : String_Ptr;
   begin
      Index_� :=
        control_RI_manager_RI_Hash.Hash
           (Operation_Name);
      if (Invoke_Db
        (Index_�)
         /= null)
      then
         raise Program_Error;
      end if;
      Invoke_Name_Access :=
        new PolyORB.Std.String'
           (Operation_Name);
      Invoke_Db
        (Index_�) :=
        Invoke_Name_Access;
   end Register_Procedure;

   procedure Invoke
     (Self : PortableServer.Servant;
      Request : CORBA.ServerRequest.Object_Ptr);

   ------------
   -- Invoke --
   ------------

   procedure Invoke
     (Self : PortableServer.Servant;
      Request : CORBA.ServerRequest.Object_Ptr)
   is
      Operation_� : constant PolyORB.Std.String :=
        CORBA.To_Standard_String
           (CORBA.ServerRequest.Operation
              (Request.all));
      Argument_List_� : CORBA.NVList.Ref;
      Index_� : PolyORB.Std.Natural;
      Invoke_Name_Access : String_Ptr;

      procedure Invoke_init_game;

      ----------------------
      -- Invoke_init_game --
      ----------------------

      procedure Invoke_init_game is
         Argument_player_number_� : CORBA.Long;
         pragma Warnings (Off, Argument_player_number_�);
         Arg_CC_player_number_� : aliased PolyORB.Any.Content'Class :=
           CORBA.Wrap
              (Argument_player_number_�'Unrestricted_Access);
         Arg_Any_player_number_� : constant CORBA.Any :=
           CORBA.Internals.Get_Wrapper_Any
              (CORBA.TC_Long,
               Arg_CC_player_number_�'Unchecked_Access);
         Argument_tactic_� : CORBA.Long;
         pragma Warnings (Off, Argument_tactic_�);
         Arg_CC_tactic_� : aliased PolyORB.Any.Content'Class :=
           CORBA.Wrap
              (Argument_tactic_�'Unrestricted_Access);
         Arg_Any_tactic_� : constant CORBA.Any :=
           CORBA.Internals.Get_Wrapper_Any
              (CORBA.TC_Long,
               Arg_CC_tactic_�'Unchecked_Access);
         Result_� : CORBA.Boolean;
         pragma Warnings (Off, Result_�);
         Arg_CC_Result_�_� : aliased PolyORB.Any.Content'Class :=
           CORBA.Wrap
              (Result_�'Unrestricted_Access);
         Arg_Any_Result_�_� : constant CORBA.Any :=
           CORBA.Internals.Get_Wrapper_Any
              (CORBA.TC_Boolean,
               Arg_CC_Result_�_�'Unchecked_Access);
      begin
         CORBA.NVList.Add_Item
           (Argument_List_�,
            init_game_Arg_Name_player_number_�,
            Arg_Any_player_number_�,
            CORBA.ARG_IN);
         CORBA.NVList.Add_Item
           (Argument_List_�,
            init_game_Arg_Name_tactic_�,
            Arg_Any_tactic_�,
            CORBA.ARG_IN);
         --  Processing request
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         --  Call Implementation
         Result_� :=
           control_RI.manager_RI.Impl.init_game
              (control_RI.manager_RI.Impl.Object'Class
                 (Self.all)'Access,
               Argument_player_number_�,
               Argument_tactic_�);
         --  Setting the result
         CORBA.ServerRequest.Set_Result
           (Request,
            Arg_Any_Result_�_�);
         CORBA.NVList.Internals.Clone_Out_Args
           (Argument_List_�);
      end Invoke_init_game;

      procedure Invoke_start;

      ------------------
      -- Invoke_start --
      ------------------

      procedure Invoke_start is
      begin
         --  Processing request
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         --  Call Implementation
         control_RI.manager_RI.Impl.start
           (control_RI.manager_RI.Impl.Object'Class
              (Self.all)'Access);
         CORBA.NVList.Internals.Clone_Out_Args
           (Argument_List_�);
      end Invoke_start;

      procedure Invoke_substitute;

      -----------------------
      -- Invoke_substitute --
      -----------------------

      procedure Invoke_substitute is
         Argument_player_id_� : CORBA.Long;
         pragma Warnings (Off, Argument_player_id_�);
         Arg_CC_player_id_� : aliased PolyORB.Any.Content'Class :=
           CORBA.Wrap
              (Argument_player_id_�'Unrestricted_Access);
         Arg_Any_player_id_� : constant CORBA.Any :=
           CORBA.Internals.Get_Wrapper_Any
              (CORBA.TC_Long,
               Arg_CC_player_id_�'Unchecked_Access);
      begin
         CORBA.NVList.Add_Item
           (Argument_List_�,
            substitute_Arg_Name_player_id_�,
            Arg_Any_player_id_�,
            CORBA.ARG_IN);
         --  Processing request
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         --  Call Implementation
         control_RI.manager_RI.Impl.substitute
           (control_RI.manager_RI.Impl.Object'Class
              (Self.all)'Access,
            Argument_player_id_�);
         CORBA.NVList.Internals.Clone_Out_Args
           (Argument_List_�);
      end Invoke_substitute;

      procedure Invoke_shutdown;

      ---------------------
      -- Invoke_shutdown --
      ---------------------

      procedure Invoke_shutdown is
      begin
         --  Processing request
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         --  Call Implementation
         control_RI.manager_RI.Impl.shutdown
           (control_RI.manager_RI.Impl.Object'Class
              (Self.all)'Access);
         CORBA.NVList.Internals.Clone_Out_Args
           (Argument_List_�);
      end Invoke_shutdown;

      procedure Invoke_Uis_a;

      ------------------
      -- Invoke_Uis_a --
      ------------------

      procedure Invoke_Uis_a is
         Type_Id_� : CORBA.String;
         Arg_Name_Type_Id_� : constant CORBA.Identifier :=
           CORBA.To_CORBA_String
              ("Type_Id_�");
         Argument_Type_Id_� : constant CORBA.Any :=
           CORBA.To_Any
              (Type_Id_�);
         Result_� : CORBA.Boolean;
      begin
         CORBA.NVList.Add_Item
           (Argument_List_�,
            Arg_Name_Type_Id_�,
            Argument_Type_Id_�,
            CORBA.ARG_IN);
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         Type_Id_� :=
           CORBA.From_Any
              (Argument_Type_Id_�);
         Result_� :=
           Is_A
              (CORBA.To_Standard_String
                 (Type_Id_�));
         CORBA.ServerRequest.Set_Result
           (Request,
            CORBA.To_Any
              (Result_�));
      end Invoke_Uis_a;

      procedure Invoke_Uinterface;

      -----------------------
      -- Invoke_Uinterface --
      -----------------------

      procedure Invoke_Uinterface is
      begin
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         CORBA.ServerRequest.Set_Result
           (Request,
            CORBA.Object.Helper.To_Any
              (CORBA.Object.Ref
                 (PolyORB.CORBA_P.IR_Hooks.Get_Interface_Definition
                    (CORBA.To_CORBA_String
                       (Repository_Id)))));
      end Invoke_Uinterface;

      procedure Invoke_Udomain_managers;

      -----------------------------
      -- Invoke_Udomain_managers --
      -----------------------------

      procedure Invoke_Udomain_managers is
      begin
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         CORBA.ServerRequest.Set_Result
           (Request,
            PolyORB.CORBA_P.Domain_Management.Get_Domain_Managers
              (Self));
      end Invoke_Udomain_managers;

      procedure Invoke_Unon_existent;

      --------------------------
      -- Invoke_Unon_existent --
      --------------------------

      procedure Invoke_Unon_existent is
      begin
         CORBA.ServerRequest.Arguments
           (Request,
            Argument_List_�);
         CORBA.ServerRequest.Set_Result
           (Request,
            CORBA.To_Any
              (CORBA.Boolean'
                 (False)));
      end Invoke_Unon_existent;

      procedure Dispatch;

      --------------
      -- Dispatch --
      --------------

      procedure Dispatch is
      begin
         case Index_� is
            when 0 =>
               Invoke_init_game;
            when 1 =>
               Invoke_start;
            when 2 =>
               Invoke_substitute;
            when 3 =>
               Invoke_shutdown;
            when 5 =>
               Invoke_Uinterface;
            when 6 =>
               Invoke_Udomain_managers;
            when 7
               | 8 =>
               Invoke_Unon_existent;
            when 4 =>
               Invoke_Uis_a;
            pragma Warnings (Off);
            when others =>
               raise Program_Error;
            pragma Warnings (On);

         end case;
      end Dispatch;

   begin
      CORBA.ORB.Create_List
        (0,
         Argument_List_�);
      Index_� :=
        control_RI_manager_RI_Hash.Hash
           (Operation_�);
      Invoke_Name_Access :=
        Invoke_Db
           (Index_�);
      begin
         if (Operation_�
            = Invoke_Name_Access.all)
         then
            Dispatch;
         else
            CORBA.Raise_Bad_Operation
              (CORBA.Default_Sys_Member);
         end if;
      exception
         when E : others =>
            CORBA.ServerRequest.Set_Exception
              (Request,
               PolyORB.CORBA_P.Exceptions.System_Exception_To_Any
                 (E));
            PolyORB.Qos.Exception_Informations.Set_Exception_Information
              (Request.all,
               E);
      end;
   end Invoke;

   function Servant_Is_A
     (Obj : PortableServer.Servant)
     return Boolean;

   ------------------
   -- Servant_Is_A --
   ------------------

   function Servant_Is_A
     (Obj : PortableServer.Servant)
     return Boolean
   is
   begin
      return (Obj.all
         in control_RI.manager_RI.Impl.Object'Class);
   end Servant_Is_A;

   -----------------------------
   -- Deferred_Initialization --
   -----------------------------

   procedure Deferred_Initialization is
   begin
      PortableServer.Internals.Register_Skeleton
        (control_RI.manager_RI.Repository_Id,
         Servant_Is_A'Access,
         Is_A'Access,
         Invoke'Access);
      Register_Procedure
        ("init_game");
      Register_Procedure
        ("start");
      Register_Procedure
        ("substitute");
      Register_Procedure
        ("shutdown");
      Register_Procedure
        ("_is_a");
      Register_Procedure
        ("_interface");
      Register_Procedure
        ("_domain_managers");
      Register_Procedure
        ("_non_existent");
      Register_Procedure
        ("_not_existent");
   end Deferred_Initialization;

begin
   declare
      use PolyORB.Utils.Strings;
      use PolyORB.Utils.Strings.Lists;
   begin
      PolyORB.Initialization.Register_Module
        (PolyORB.Initialization.Module_Info'
           (Name => +"control_RI.manager_RI.Skel",
            Conflicts => PolyORB.Utils.Strings.Lists.Empty,
            Depends => PolyORB.Utils.Strings.Lists.Empty,
            Provides => PolyORB.Utils.Strings.Lists.Empty,
            Implicit => False,
            Init => Deferred_Initialization'Access,
            Shutdown => null));
   end;
end control_RI.manager_RI.Skel;
