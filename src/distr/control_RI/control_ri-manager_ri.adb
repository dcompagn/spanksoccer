pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with PolyORB.Any.NVList;
with PolyORB.Any;
with PolyORB.Types;
with PolyORB.Requests;
with PolyORB.CORBA_P.Interceptors_Hooks;
with PolyORB.CORBA_P.Exceptions;

package body control_RI.manager_RI is

   init_game_Arg_Name_player_number_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("player_number");

   init_game_Arg_Name_tactic_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("tactic");

   init_game_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   ------------------------
   -- init_game_Result_� --
   ------------------------

   function init_game_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (init_game_Result_�);
   begin
      return (Name => init_game_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Boolean),
      Arg_Modes => 0);
   end init_game_Result_�;

   ---------------
   -- init_game --
   ---------------

   function init_game
     (Self : Ref;
      player_number : CORBA.Long;
      tactic : CORBA.Long)
     return CORBA.Boolean
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Result_� : CORBA.Boolean;
      pragma Warnings (Off, Result_�);
      Arg_CC_Result_�_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (Result_�'Unrestricted_Access);
      Arg_CC_player_number_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (player_number'Unrestricted_Access);
      Arg_Any_player_number_� : constant CORBA.Any :=
        CORBA.Internals.Get_Wrapper_Any
           (CORBA.TC_Long,
            Arg_CC_player_number_�'Unchecked_Access);
      Arg_CC_tactic_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (tactic'Unrestricted_Access);
      Arg_Any_tactic_� : constant CORBA.Any :=
        CORBA.Internals.Get_Wrapper_Any
           (CORBA.TC_Long,
            Arg_CC_tactic_�'Unchecked_Access);
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        init_game_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Fill the Argument list
      PolyORB.Any.NVList.Add_Item
        (Argument_List_�,
         init_game_Arg_Name_player_number_�,
         PolyORB.Any.Any
           (Arg_Any_player_number_�),
         PolyORB.Any.ARG_IN);
      PolyORB.Any.NVList.Add_Item
        (Argument_List_�,
         init_game_Arg_Name_tactic_�,
         PolyORB.Any.Any
           (Arg_Any_tactic_�),
         PolyORB.Any.ARG_IN);
      --  Setting the result value
      PolyORB.Any.Set_Value
        (PolyORB.Any.Get_Container
           (Result_Nv_�.Argument).all,
         Arg_CC_Result_�_�'Unrestricted_Access);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "init_game",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
      --  Return value
      return Result_�;
   end init_game;

   start_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   --------------------
   -- start_Result_� --
   --------------------

   function start_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (start_Result_�);
   begin
      return (Name => start_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Void),
      Arg_Modes => 0);
   end start_Result_�;

   -----------
   -- start --
   -----------

   procedure start
     (Self : Ref)
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        start_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "start",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�,
         Req_Flags => PolyORB.Requests.Sync_With_Transport);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
   end start;

   substitute_Arg_Name_player_id_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("player_id");

   substitute_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   -------------------------
   -- substitute_Result_� --
   -------------------------

   function substitute_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (substitute_Result_�);
   begin
      return (Name => substitute_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Void),
      Arg_Modes => 0);
   end substitute_Result_�;

   ----------------
   -- substitute --
   ----------------

   procedure substitute
     (Self : Ref;
      player_id : CORBA.Long)
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Arg_CC_player_id_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (player_id'Unrestricted_Access);
      Arg_Any_player_id_� : constant CORBA.Any :=
        CORBA.Internals.Get_Wrapper_Any
           (CORBA.TC_Long,
            Arg_CC_player_id_�'Unchecked_Access);
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        substitute_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Fill the Argument list
      PolyORB.Any.NVList.Add_Item
        (Argument_List_�,
         substitute_Arg_Name_player_id_�,
         PolyORB.Any.Copy_Any
           (PolyORB.Any.Any
              (Arg_Any_player_id_�)),
         PolyORB.Any.ARG_IN);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "substitute",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�,
         Req_Flags => PolyORB.Requests.Sync_With_Transport);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
   end substitute;

   shutdown_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   -----------------------
   -- shutdown_Result_� --
   -----------------------

   function shutdown_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (shutdown_Result_�);
   begin
      return (Name => shutdown_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Void),
      Arg_Modes => 0);
   end shutdown_Result_�;

   --------------
   -- shutdown --
   --------------

   procedure shutdown
     (Self : Ref)
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        shutdown_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "shutdown",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�,
         Req_Flags => PolyORB.Requests.Sync_With_Transport);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
   end shutdown;

   ----------
   -- Is_A --
   ----------

   function Is_A
     (Self : Ref;
      Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean
   is
   begin
      return (False
         or else (Is_A
           (Logical_Type_Id)
            or else CORBA.Object.Is_A
              (CORBA.Object.Ref
                 (Self),
               Logical_Type_Id)));
   end Is_A;

   ----------
   -- Is_A --
   ----------

   function Is_A
     (Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean
   is
   begin
      return ((CORBA.Is_Equivalent
        (Logical_Type_Id,
         control_RI.manager_RI.Repository_Id)
         or else CORBA.Is_Equivalent
           (Logical_Type_Id,
            "IDL:omg.org/CORBA/Object:1.0"))
         or else False);
   end Is_A;

end control_RI.manager_RI;
