pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------

package control_RI.manager_RI.Skel is

   procedure Deferred_Initialization;

end control_RI.manager_RI.Skel;
