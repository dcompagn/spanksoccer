--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.0                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;
with control_RI.subscriber_RI.Skel;
with CORBA.ORB;

pragma Warnings (Off, control_RI.subscriber_RI.Skel);
with Game.Broker; use Game.Broker;

package body control_RI.subscriber_RI.impl is

   function subscribe (Self : access Object;
                       IOR  : CORBA.String) return CORBA.Long is

      temp_IOR              : Ada.Strings.Unbounded.Unbounded_String := Ada.Strings.Unbounded.To_Unbounded_String(CORBA.to_standard_string(IOR));
      temp_ID               : Integer;

   begin
      Put_Line("Arrivato un IOR e chiamo risorsa protetta");
      Subscription_Container.Add(temp_IOR, temp_ID);
      return CORBA.Long(temp_ID);
   end subscribe;

   procedure unsubcribe (Self     : access Object;
                         Panel_ID : CORBA.Long) is

      null_string                 : Unbounded_String := To_Unbounded_String("Null");
   begin
      Subscription_Container.Delete(Integer(Panel_ID));
   end unsubcribe;

end control_RI.subscriber_RI.impl;
