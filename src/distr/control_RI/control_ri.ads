pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with PolyORB.Std;

package control_RI is

   Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI:1.0";

end control_RI;
