pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with PolyORB.Any.NVList;
with PolyORB.Any;
with PolyORB.Types;
with PolyORB.Requests;
with PolyORB.CORBA_P.Interceptors_Hooks;
with PolyORB.CORBA_P.Exceptions;

package body control_RI.subscriber_RI is

   subscribe_Arg_Name_IOR_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("IOR");

   subscribe_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   ------------------------
   -- subscribe_Result_� --
   ------------------------

   function subscribe_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (subscribe_Result_�);
   begin
      return (Name => subscribe_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Long),
      Arg_Modes => 0);
   end subscribe_Result_�;

   ---------------
   -- subscribe --
   ---------------

   function subscribe
     (Self : Ref;
      IOR : CORBA.String)
     return CORBA.Long
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Result_� : CORBA.Long;
      pragma Warnings (Off, Result_�);
      Arg_CC_Result_�_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (Result_�'Unrestricted_Access);
      Arg_CC_IOR_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (IOR'Unrestricted_Access);
      Arg_Any_IOR_� : constant CORBA.Any :=
        CORBA.Internals.Get_Wrapper_Any
           (CORBA.TC_String,
            Arg_CC_IOR_�'Unchecked_Access);
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        subscribe_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Fill the Argument list
      PolyORB.Any.NVList.Add_Item
        (Argument_List_�,
         subscribe_Arg_Name_IOR_�,
         PolyORB.Any.Any
           (Arg_Any_IOR_�),
         PolyORB.Any.ARG_IN);
      --  Setting the result value
      PolyORB.Any.Set_Value
        (PolyORB.Any.Get_Container
           (Result_Nv_�.Argument).all,
         Arg_CC_Result_�_�'Unrestricted_Access);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "subscribe",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
      --  Return value
      return Result_�;
   end subscribe;

   unsubcribe_Arg_Name_Panel_ID_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Panel_ID");

   unsubcribe_Result_Name_� : constant PolyORB.Types.Identifier :=
     PolyORB.Types.To_PolyORB_String
        ("Result");

   -------------------------
   -- unsubcribe_Result_� --
   -------------------------

   function unsubcribe_Result_� return PolyORB.Any.NamedValue is
      pragma Inline (unsubcribe_Result_�);
   begin
      return (Name => unsubcribe_Result_Name_�,
      Argument => CORBA.Internals.Get_Empty_Any
        (CORBA.TC_Void),
      Arg_Modes => 0);
   end unsubcribe_Result_�;

   ----------------
   -- unsubcribe --
   ----------------

   procedure unsubcribe
     (Self : Ref;
      Panel_ID : CORBA.Long)
   is
      Argument_List_� : PolyORB.Any.NVList.Ref;
      Arg_CC_Panel_ID_� : aliased PolyORB.Any.Content'Class :=
        CORBA.Wrap
           (Panel_ID'Unrestricted_Access);
      Arg_Any_Panel_ID_� : constant CORBA.Any :=
        CORBA.Internals.Get_Wrapper_Any
           (CORBA.TC_Long,
            Arg_CC_Panel_ID_�'Unchecked_Access);
      Request_� : aliased PolyORB.Requests.Request;
      Result_Nv_� : PolyORB.Any.NamedValue :=
        unsubcribe_Result_�;
   begin
      if CORBA.Object.Is_Nil
        (CORBA.Object.Ref
           (Self))
      then
         CORBA.Raise_Inv_Objref
           (CORBA.Default_Sys_Member);
      end if;
      --  Create the Argument list
      PolyORB.Any.NVList.Create
        (Argument_List_�);
      --  Fill the Argument list
      PolyORB.Any.NVList.Add_Item
        (Argument_List_�,
         unsubcribe_Arg_Name_Panel_ID_�,
         PolyORB.Any.Any
           (Arg_Any_Panel_ID_�),
         PolyORB.Any.ARG_IN);
      --  Creating the request
      PolyORB.Requests.Setup_Request
        (Req => Request_�,
         Target => CORBA.Object.Internals.To_PolyORB_Ref
           (CORBA.Object.Ref
              (Self)),
         Operation => "unsubcribe",
         Arg_List => Argument_List_�,
         Result => Result_Nv_�);
      --  Invoking the request (synchronously or asynchronously)
      PolyORB.CORBA_P.Interceptors_Hooks.Client_Invoke
        (Request_�'Access,
         PolyORB.Requests.Flags
           (0));
      --  Raise exception, if needed
      PolyORB.CORBA_P.Exceptions.Request_Raise_Occurrence
        (Request_�);
   end unsubcribe;

   ----------
   -- Is_A --
   ----------

   function Is_A
     (Self : Ref;
      Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean
   is
   begin
      return (False
         or else (Is_A
           (Logical_Type_Id)
            or else CORBA.Object.Is_A
              (CORBA.Object.Ref
                 (Self),
               Logical_Type_Id)));
   end Is_A;

   ----------
   -- Is_A --
   ----------

   function Is_A
     (Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean
   is
   begin
      return ((CORBA.Is_Equivalent
        (Logical_Type_Id,
         control_RI.subscriber_RI.Repository_Id)
         or else CORBA.Is_Equivalent
           (Logical_Type_Id,
            "IDL:omg.org/CORBA/Object:1.0"))
         or else False);
   end Is_A;

end control_RI.subscriber_RI;
