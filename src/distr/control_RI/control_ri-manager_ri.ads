pragma Style_Checks ("NM32766");
---------------------------------------------------
--  This file has been generated automatically from
--  control.idl
--  by IAC (IDL to Ada Compiler) GPL 2011-20110419 (rev. 172016).
---------------------------------------------------
--  Do NOT hand-modify this file, as your
--  changes will be lost when you re-run the
--  IDL to Ada compiler.
---------------------------------------------------
with CORBA.Object;
with PolyORB.Std;
with CORBA;
pragma Elaborate_All (CORBA);

package control_RI.manager_RI is

   type Ref is
     new CORBA.Object.Ref with null record;

   Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/manager_RI:1.0";

   function init_game
     (Self : Ref;
      player_number : CORBA.Long;
      tactic : CORBA.Long)
     return CORBA.Boolean;

   init_game_Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/manager_RI/init_game:1.0";

   procedure start
     (Self : Ref);

   start_Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/manager_RI/start:1.0";

   procedure substitute
     (Self : Ref;
      player_id : CORBA.Long);

   substitute_Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/manager_RI/substitute:1.0";

   procedure shutdown
     (Self : Ref);

   shutdown_Repository_Id : constant PolyORB.Std.String :=
     "IDL:control_RI/manager_RI/shutdown:1.0";

   function Is_A
     (Self : Ref;
      Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean;

private
   function Is_A
     (Logical_Type_Id : PolyORB.Std.String)
     return CORBA.Boolean;

end control_RI.manager_RI;
