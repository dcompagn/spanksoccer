--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.0                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with CORBA;
with PortableServer;

package control_RI.manager_RI.impl is

   type Object is new PortableServer.Servant_Base with null record;

   type Object_Acc is access Object;

   function init_game
     (Self : access Object;
      player_number : CORBA.Long;
      tactic : CORBA.Long)
     return CORBA.Boolean;

   procedure start
     (Self : access Object);

   procedure substitute
     (Self : access Object;
      player_id : CORBA.Long);

   procedure shutdown
     (Self :  access Object);

end control_RI.manager_RI.impl;
