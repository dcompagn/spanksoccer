--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Calendar;
with Ada.Numerics.Elementary_Functions;
with Ada.Numerics.Discrete_Random;

package body Game.Logic.Player is

   function Create(Name : Unbounded_String;
                   Team : Team_T;
                   Role : Role_T;
                   Abilities: Ability_T;
                   Energy: Positive;
                   Influence_Zone_Radius:Positive) return Player_Feature_Ref_T is
   begin

      return new Player_Feature_T'(
         Name => Name,
         Team => Team,
         Role => Role,
         Abilities => Abilities,
         Energy => Energy,
         Influence_Zone_Radius => Influence_Zone_Radius);

   end;

   function Create_Abilities(Shoot_power : Positive;
                             Velocity : Positive;
                             Tackle : Positive;
                             Pass_Probability : Positive) return Ability_T is
   begin
      return (Shoot_power => Shoot_power,
              Velocity => Velocity,
              Tackle => Tackle,
              Pass_Probability => Pass_Probability);
   end;

   function Calculate_Goalkeeper_Action (This : Player_Feature_Ref_T;
                                         Game_Status : Game_Status_T)
                                         return Action_T is
      Compass : Compass_T;
      Coord : Position_T := (X =>0, Y=>0);
      Displacement : Displacement_T := (X =>0, Y=>0);
   begin
      if Game_Status.Ball_Is_My then
         -- Se ho la palla, la calcio verso un target casuale
         return Create_Kick(Game_Status.My_Id,
                            Game_Status.Near_Targets
                              (Generate_Number(Game_Status.Near_Targets'Last)),
                            This.all.Abilities.Shoot_power);
      else
         if Game_Status.Nearest_To_Ball then
            -- Se sono il giocatore piu' vicino alla palla
            if Is_Visible(Game_Status.My_Position,
                          Game_Status.Ball_position,
                          Threshold)
            then
               -- e la palla e' in un intorno minimo, prendo la palla
               return Create_Take_Ball(Game_Status.My_Id);
            else
               -- altrimenti mi muovo verso la sua posizione
               Displacement := Follow_Target(Game_Status.Ball_position,
                                             Game_Status.My_Position,
                                             This.all.Abilities.Velocity);
               return Create_Move(Game_Status.My_Id, Displacement);
            end if;
         else
            -- Se non sono il giocatore piu' vicino alla palla mi sposto
            -- sull'asse Y seguendo la palla, al limite del raggio di influenza
            Coord.Y := Game_Status.Ball_position.Y;
            Compass :=
              Relative_Compass(Game_Status.My_Init_Position,
                               Game_Status.My_Position,
                               This.all.Influence_Zone_Radius);
            if Coord.Y > Game_Status.My_Position.Y then
               Displacement.Y :=
                 Minimum(Compass.S,
                         Coord.Y - Game_Status.My_Position.Y);
               Displacement.Y :=
                 Minimum(Displacement.Y,
                   Generate_Number(Velocity_To_Coordinate
                     (This.all.Abilities.Velocity)));
            else -- caso <= vale
               Displacement.Y :=
                 Maximum(Compass.N, Coord.Y - Game_Status.My_Position.Y);
               Displacement.Y :=
                 Maximum(Displacement.Y,
                   -1 * Generate_Number(Velocity_To_Coordinate
                     (This.all.Abilities.Velocity)));
            end if;

            Displacement.X :=
              Follow_Target(Game_Status.My_Init_Position,
                            Game_Status.My_Position,
                            This.all.Abilities.Velocity).X;

            return Create_Move(Game_Status.My_Id,Displacement);
         end if;
      end if;
   end Calculate_Goalkeeper_Action;


   function Calculate_Attack_Action (This : Player_Feature_Ref_T;
                                     Game_Status : Game_Status_T)
                                     return Action_T is
      Compass : Compass_T;
      Coord : Position_T := (X =>0, Y=>0);
      Displacement : Displacement_T := (X =>0, Y=>0);
   begin

      --delay Standard.Duration(2.0 * Float(Velocity_To_Coordinate(This.all.Abilities.Velocity))
      --  / Float(Sleep_Factor));

      -- Se il ruolo e' portiere utilizzo la funzione adeguata
      if This.all.Role = Goalkeeper then -- sono portiere
         return Calculate_Goalkeeper_Action(This, Game_Status);
      end if;

      -- Se ho il possesso palla
      if Game_Status.Ball_Is_My then

         Compass := Relative_Compass(Game_Status.My_Init_Position,
                                     Game_Status.My_Position,
                                     This.all.Influence_Zone_Radius);
         -- Se sono del Team1
         if This.Team = Team1 then
            -- Logica che gestisce la rimessa laterale
            if Game_Status.My_Position.Y < Kick_Y_Direction_Bound then
               -- Se sono a bordo campo nord
               return Create_Kick
                 (Game_Status.My_Id,
                  (Game_Status.My_Position.X,
                   Generate_Number(This.all.Abilities.Shoot_power)),
                  This.all.Abilities.Shoot_power);
            end if;
            -- Se sono a bordo campo sud
            if Game_Status.My_Position.Y
              > Board_Size_Y - Kick_Y_Direction_Bound
            then
               return Create_Kick
                 (Game_Status.My_Id,
                  (Game_Status.My_Position.X,
                   Board_Size_Y - Generate_Number(This.all.Abilities.Shoot_power)),
                  This.all.Abilities.Shoot_power);
            end if;
            -- Se sono alla posizione piu' avanzata che posso raggiungere
            -- calcio verso un target, eventualmente in porta
            if Compass.E <= 0 then
               return Create_Kick
                 (Game_Status.My_Id,
                  Game_Status.Near_Targets(Targets_T'First),
                  This.all.Abilities.Shoot_power);
            else
               -- altrimenti scelgo se passare o andare avanti
               if Generate_Number(100) < This.all.Abilities.Pass_Probability
               then
                  return Create_Kick
                    (Game_Status.My_Id,
                     Game_Status.Near_Targets
                       (Generate_Number(Game_Status.Near_Targets'Last)),
                     This.all.Abilities.Shoot_power);
               else
                  Displacement :=
                    (X => Minimum
                       (Forward_Factor(This.all.Team)
                        * Generate_Number(Velocity_To_Coordinate
                          (This.all.Abilities.Velocity)),
                        Compass.E),
                     Y => 0); -- su Y si pu� mettere Init.Y - Pos.Y
                  return Create_Move(Game_Status.My_Id, Displacement);
               end if;
            end if;
         else -- Se sono del Team2
            -- Rimessa laterale nord
            if Game_Status.My_Position.Y < Kick_Y_Direction_Bound then
               return Create_Kick
                 (Game_Status.My_Id,
                  (Game_Status.My_Position.X,
                   Generate_Number(This.all.Abilities.Shoot_power)),
                  This.all.Abilities.Shoot_power);
            end if;
            -- Rimessa laterale sud
            if Game_Status.My_Position.Y
              > Board_Size_Y - Kick_Y_Direction_Bound
            then
               return Create_Kick
                 (Game_Status.My_Id,
                  (Game_Status.My_Position.X,
                   Board_Size_Y - Generate_Number(This.all.Abilities.Shoot_power)),
                  This.all.Abilities.Shoot_power);
            end if;
            -- Se sono alla posizione piu' avanzata che posso raggiungere
            -- calcio verso un target, eventualmente in porta
            if Compass.W >= 0 then
               return Create_Kick(Game_Status.My_Id,
                                  Game_Status.Near_Targets(Targets_T'First),
                                  This.all.Abilities.Shoot_power);
            else
               -- scelgo se passare o andare avanti
               if Generate_Number(100) < This.all.Abilities.Pass_Probability
               then
                  return Create_Kick
                    (Game_Status.My_Id,
                     Game_Status.Near_Targets
                       (Generate_Number(Game_Status.Near_Targets'Last)),
                     This.all.Abilities.Shoot_power);
               else
                  Displacement :=
                    (Maximum(Forward_Factor(This.all.Team) * Generate_Number
                     (Velocity_To_Coordinate(This.all.Abilities.Velocity)),
                     Compass.W),
                     Y => 0);
                  return Create_Move(Game_Status.My_Id, Displacement);
               end if;
            end if;
         end if;

      else
         -- Se non ho la palla, ma sono il giocatore piu' vicino
         -- vado a prenderla
         if Game_Status.Nearest_To_Ball then
            Displacement := Follow_Target(Game_Status.Ball_position,
                                          Game_Status.My_Position,
                                          This.all.Abilities.Velocity);
            -- Se e' in un intorno piccolo
            if Is_Visible(Game_Status.My_Position,
                          Game_Status.Ball_position,
                          Threshold)
            then
               -- la prendo
               return Create_Take_Ball(Game_Status.My_Id);
            else
               -- altrimenti mi muovo
               return Create_Move(Game_Status.My_Id, Displacement);
            end if;
         else
            -- se non sono il giocatore piu' vicino torno verso la
            -- posizione iniziale
            Displacement := Follow_Target(Game_Status.My_Init_Position,
                                          Game_Status.My_Position,
                                          This.all.Abilities.Velocity);
         end if;

         return Create_Move(Game_Status.My_Id, Displacement);

      end if;
   end Calculate_Attack_Action;

   function Calculate_Defence_Action (This : Player_Feature_Ref_T;
                                      Game_Status : Game_Status_T)
                                      return Action_T is
      Compass : Compass_T;
      Coord : Position_T := (X =>0, Y=>0);
      Displacement : Displacement_T := (X =>0, Y=>0);
      Rand_Value : Positive;
   begin
      if This.Role = Goalkeeper then
         -- Se il ruolo e' portiere utilizzo la funzione adeguata
         return Calculate_Goalkeeper_Action(This, Game_Status);
      else -- altrimenti
         -- Se sono il giocatore piu' vicino alla palla
         if Game_Status.Nearest_To_Ball then
            -- e la palla e' in un intorno minimo
            if Is_Visible(Game_Status.My_Position,
                          Game_Status.Ball_position,
                          Threshold)
            then
               Rand_Value := Generate_Number(4);
               -- scelgo casualmente una delle 4 scelte (tackle, fallo, prendo
               -- palla, o spostamento nullo) naturalmente saranno poi validate
               -- dall'intemediario.
               if Rand_Value = 1 then
                  return Create_Tackle(Game_Status.My_Id,
                                       Game_Status.Ball_Possesor);
                  -- si assume che Near_Player(1) sia il portatore di palla
                  -- (si pu� migliorare con random) l'intermediario poi
                  -- verifica gli ID
               elsif Rand_Value = 2 then
                  return Create_Foul(Game_Status.My_Id,
                                     Game_Status.Ball_Possesor,
                                     Game_Status.My_Position);
               elsif Rand_Value = 3 then
                  return Create_Take_Ball(Game_Status.My_Id);
               else
                  return Create_Move(Game_Status.My_Id, (0,0));
               end if;
            else
               -- altrimenti seguo la palla
               Displacement := Follow_Target(Game_Status.Ball_position,
                                             Game_Status.My_Position,
                                             This.all.Abilities.Velocity);
               return Create_Move(Game_Status.My_Id, Displacement);
            end if;
         else
            -- se invece non sono il giocatore pi� vicino, torno verso la
            -- posizione iniziale
            Displacement := Follow_Target(Game_Status.My_Init_Position,
                                          Game_Status.My_Position,
                                          This.all.Abilities.Velocity);
            return Create_Move(Game_Status.My_Id, Displacement);
         end if;
      end if;
   end Calculate_Defence_Action;

   function Calculate_Reposition_Action (This : Player_Feature_Ref_T;
                                         Game_Status : Game_Status_T)
                                         return Action_T is
      Coord : Position_T := (X =>0, Y=>0);
      Displacement : Displacement_T := (X =>0, Y=>0);
   begin
      -- In caso di gioco fermo seguo il target predefinito che sara' calcolato
      -- correttamente dalla Get_Status_For in base alla situazione di gioco
      Displacement := Follow_Target(Game_Status.Near_Targets(Targets_T'First),
                                    Game_Status.My_Position,
                                    This.all.Abilities.Velocity);
      return Create_Move(Game_Status.My_Id, Displacement);
   end Calculate_Reposition_Action;


   function Get_Team(This : Player_Feature_Ref_T) return Team_T is
   begin
      return This.all.Team;
   end;

   function Get_Role(This : Player_Feature_Ref_T) return Role_T is
   begin
      return This.all.Role;
   end;

   function Get_Radius(This : Player_Feature_Ref_T) return Positive is
   begin
      return This.all.Influence_Zone_Radius;
   end;

   function Get_Shoot_power(This : Player_Feature_Ref_T) return Positive is
   begin
      return This.all.Abilities.Shoot_power;
   end;

   function Feature_To_Sleep(This : Player_Feature_Ref_T) return Positive is
   	V : Positive := This.all.Abilities.Velocity;
   begin
      if V >= 16 then
         return 2;
      else
         if V < 16 and V >= 11 then
            return 3;
         else
            if V < 11 and V >= 8 then
               return 4;
            else
               if V < 8 and V >= 5 then
                  return 6;
               else
                  return 12;
               end if;
            end if;
         end if;
      end if;
   end Feature_To_Sleep;

   function Get_Tackle(This : Player_Feature_Ref_T) return Positive is
   begin
      return This.all.Abilities.Tackle;
   end Get_Tackle;

   function Get_Name(This : Player_Feature_Ref_T) return Unbounded_String is
   begin
      return This.all.Name;
   end Get_Name;

   function Get_Energy(This : Player_Feature_Ref_T) return Positive is
   begin
      return This.all.Energy;
   end Get_Energy;

   function Get_Velocity (This : Player_Feature_Ref_T) return Positive is
   begin
      return This.all.Abilities.Velocity;
   end Get_Velocity;

   function Get_Pass_Probability(This : Player_Feature_Ref_T) return Positive is
   begin
      return This.all.Abilities.Pass_Probability;
   end Get_Pass_Probability;

end Game.Logic.Player;
