--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Input_Sources.File; use Input_Sources.File;
with Sax.Readers;        use Sax.Readers;
with DOM.Readers;        use DOM.Readers;
with DOM.Core;           use DOM.Core;
with DOM.Core.Documents; use DOM.Core.Documents;
with DOM.Core.Nodes;     use DOM.Core.Nodes;
with DOM.Core.Attrs;     use DOM.Core.Attrs;
with Ada.Text_IO;        use Ada.Text_IO;
with Game.Logic.Player;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Game.Logic.Features is

   -- aggiungere parametro nome file xml da console
   function Load_Features(File_Path : String) return Players_Feature_T is
      Input : File_Input;
      Reader : Tree_Reader;
      Doc : Document;
      List : Node_List;
      N : Node;
      Name, Team, Role, Shoot, Velocity, Tackle, Energy, Pass, Radius : Attr;
      Count_Odd : Player_ID_T := Player_ID_T'First;
      Count_Even : Player_ID_T := Player_ID_T'First + 1;
      Features : Players_Feature_T;
   begin



      Set_Public_Id (Input, "Features file");
      Open (File_Path, Input);

      Set_Feature (Reader, Validation_Feature, False);
      Set_Feature (Reader, Namespace_Feature, False);

      Parse (Reader, Input);
      Close (Input);

      Doc := Get_Tree (Reader);

      List := Get_Elements_By_Tag_Name (Doc, "player");

      for Index in 1 .. Length (List) loop
         N := Item (List, Index - 1);
         Name := Get_Named_Item (Attributes (N), "name");
         Team := Get_Named_Item (Attributes (N), "team");
         Role := Get_Named_Item (Attributes (N), "role");
         Shoot := Get_Named_Item (Attributes (N), "shoot");
         Velocity := Get_Named_Item (Attributes (N), "velocity");
         Tackle := Get_Named_Item (Attributes (N), "tackle");
         Energy := Get_Named_Item (Attributes (N), "energy");
         Pass := Get_Named_Item (Attributes (N), "pass_prob");
         Radius := Get_Named_Item (Attributes (N), "radius");
         -- Creo l'oggetto giocatore
         --Ada.Text_IO.Put("Create :" &Player_ID_T'Image(ID)&"-->");
            --Put_Line ("Player: name=""" & Value (Name)
            --    &""" team="""& Value (Team)
            --    &""" role="""& Value (Role)
            --    &""" shoot="""& Value (Shoot)
            --    &""" velocity="""& Value (Velocity)
            --    &""" tackle="""& Value (Tackle)
            --    &""" energy="""& Value (Energy)
            --    &""" radius="""& Positive'Image(Positive'Value(Value(Radius)))&"""");

         -- L'array e' popolato diversamente nelle posizioni pari e dispari.
         -- Per convenzione il Team1 occupa le posizioni dispari, mentre il
         -- Team2 quelle pari.
         if Team_T'Value(Value(Team)) = Team1 then
            if Count_Odd <= Player_ID_T'Last then

               Features(Count_Odd) := Game.Logic.Player.Create
                 (Name => To_Unbounded_String(Value(Name)),
                  Team => Team_T'Value(Value(Team)),
                  Role => Role_T'Value(Value(Role)),
                  Abilities => Game.Logic.Player.Create_Abilities
                    (Shoot_power => Positive'Value(Value(Shoot)),
                     Velocity => Natural'Value(Value(Velocity)),
                     Tackle => Positive'Value(Value(Tackle)),
                     Pass_Probability => Positive'Value(Value(Pass))),
                  Energy => Positive'Value(Value(Energy)),
                  Influence_Zone_Radius => Positive'Value(Value(Radius)));

               if Count_Odd + 2 <= Player_ID_T'Last then
                  Count_Odd := Count_Odd + 2;
               end if;

            end if;

         else

            if Count_Even <= Player_ID_T'Last then
               Features(Count_Even) := Game.Logic.Player.Create
                 (Name => To_Unbounded_String(Value(Name)),
                  Team => Team_T'Value(Value(Team)),
                  Role => Role_T'Value(Value(Role)),
                  Abilities => Game.Logic.Player.Create_Abilities
                    (Shoot_power => Positive'Value(Value(Shoot)),
                     Velocity => Natural'Value(Value(Velocity)),
                     Tackle => Positive'Value(Value(Tackle)),
                     Pass_Probability => Positive'Value(Value(Pass))),
                  Energy => Positive'Value(Value(Energy)),
                  Influence_Zone_Radius => Positive'Value(Value(Radius)));

               if Count_Even + 2 <= Player_ID_T'Last then
                  Count_Even := Count_Even + 2;
               end if;

            end if;

         end if;

      end loop;

      Free (List);
      Free (Reader);

      return Features;

   end Load_Features;

end Game.Logic.Features;
