--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game.Clock; use Game.Clock;
with Game.Logic; use Game.Logic;
with Game.Logic.Player; use Game.Logic.Player;
with Game.Logic.Mediator; use Game.Logic.Mediator;
with Game.Logic.Action; use Game.Logic.Action;
with Ada.Calendar;

-- Questo package contiene la risorsa protetta presso la quale i task Player(s),
-- Referee, Ball, e Broker sono sincronizzati.
-- Tutte le informazioni riguardanti lo stato di gioco, sul piano
-- logico-architetturale, sono contenute all'interno di un oggetto intermediario
-- che fornisce i metodi per manipolarlo.
package Game.Status is

   protected Status_T is

      -- Entry che gestisce l'avvio della partita per i task Referee, Ball,
      -- Broker. Rilascia i task accodati successivamente all'invocazione
      -- di Start_Match.
      -- Param:
      -- Number_Of_Player: Numero di giocatori per il match corrente
      -- Start_Time : Timestamp di inizio match
      -- Shut_Down : Segnale di terminazione
      entry Waiting_For_Start_Signal_To_Other(Number_Of_Player: out Natural;
                                              Start_Time: out Ada.Calendar.Time;
                                              Shut_Down: out Boolean);

      -- Entry che gestisce l'avvio della partita per i task Player. Rilascia
      -- i task accodati successivamente all'invocazione di Start_Match.
      -- Parametri:
      -- ID : Identificativo del giocatore
      -- Sleep : Tempo di sospensione, calcolato secondo la sua abilita'
      -- Start_Time : Timestamp di inizio match
      -- Shut_Down : Segnale di terminazione
      entry Waiting_For_Start_Signal_To_Player(ID: out Player_ID_T;
                                               Sleep: out Positive;
                                               Start_Time: out Ada.Calendar.Time;
                                               Shut_Down: out Boolean);

      -- Procedura che avvia il match. Abilita i task in attesa dell'inizio
      -- dopo aver inizializzato correttamente lo stato.
      procedure Start_Match(Players_Number : in Natural;
                            Players_Feature : in Players_Feature_T;
                            Scheme: in Scheme_T);

      -- Entry che gestisce la terminazione, permette la terminazione ordinata
      -- del match, prevenendo ogni comportamento anomalo di riavvio.
      -- Attraverso questa entry ogni task esprime la volonta' di terminare ma
      -- rimane ivi accodato fintanto che tutti gli altri task non hanno
      -- terminato.
      entry Ready_To_End(Shut_Down: out Boolean);

      -- Procedura invocata dall'arbitro per garantire la terminazione dei
      -- giocatori bloccati in "Player_Mode_Off_With_Target" e
      -- "Player_Mode_Off_Without_Target" se l'arbitro termina prima di aver
      -- rilasciato queste guardie, oppure se un fallo avviene durante l'ultimo
      -- iperperiodo.
      procedure Release_Players;

      -- Comunica ai task l'arresto immediato dell'esecuzione e garantisce
      -- la terminazione corretta dell'intera simulazione.
      procedure Shut_Down;

      -- Entry che permette ai task di accedere in mututa esclusione allo
      -- stato, ed eseguire la mossa calcolata per il proprio turno, quando
      -- lo stato di gioco e' normale (modalita' "On", nessuna interruzione,
      -- fallo, goal).
      -- 1) garantisce la coerenza dello stato
      -- 2) permette di ridurre i context switch attraverso il proxy model
      entry Player_Mode_On (Player_ID : Player_ID_T;
                           Logic_Clock: in Logic_Clock_T;
                           Shut_Down: out Boolean);

      -- Entry che permette ai task di accedere in mututa esclusione allo
      -- stato, per riposizionarsi dopo che l'arbitro ha interrotto il gioco.
      -- Una volta riposizionato, il giocatore e' riaccodato nella guardia
      -- "Player_Mode_Off_With_Target" o "Player_Mode_Off_Without_Target" a
      -- seconda che esso sia o no un target per l'azione, in attesa che
      -- l'arbitro faccia riprendere il gioco.
      -- Evita l'attesa attiva da parte dei giocatori, ma richiede allineamento
      -- del clock.
      entry Player_Mode_Off (Player_ID : Player_ID_T;
                            Logic_Clock: in out Logic_Clock_T;
                            Shut_Down: out Boolean);

      -- Procedura che permette al referee di eseguire in mutua esclusione
      procedure Referee(Logic_Clock: in Logic_Clock_T; Shut_Down: out Boolean);

      -- Entry che permette alla palla di eseguire in mutua esclusione.
      -- La guardia permette di bloccare l'esecuzione del task Ball quando
      -- un giocatore ne e' in possesso, per eliminare l'attesa attiva.
      -- Per contro, quando rilasciata, deve avvenire il riallineato del clock.
      entry Ball(Logic_Clock : in out Logic_Clock_T; Shut_Down: out Boolean);

      -- Procedura che permette al manager di eseguire in mutua esclusione
      procedure Manager(Action : in Action_T);

      -- Entry che permette al Broker la raccolta dello stato
      entry Broker (Timestamp: out Natural;
                    Ball_Position: out Position_T;
                    Referee_Position: out Position_T;
                    Players_Position: in out Player_Sequence_Wrapper_T;
                    Shut_Down : out Boolean;
                    Duration_match :  out Natural;
                    Score_team_1 : out Natural;
                    Score_team_2 : out Natural;
                    Control : out Integer );

   private

      -- In questa entry sono riaccodati i task provenienti da Ready_To_End
      -- in attesa di terminare il match.
      entry Waiting_For_End_Others(Shut_Down: out Boolean);

      -- In questa entry sono riaccodati i task provenienti da Player_Mode_Off
      -- che sono target per l'azione. Ad esempio il giocatore che deve battere
      -- la punizione, piuttosto che la rimessa laterale, o il calcio d'inizio.
      -- L'esecuzione di questa entry abilita "Player_Mode_Off_Without_Target".
      entry Player_Mode_Off_With_Target(Player_ID : Player_ID_T;
                                        Logic_Clock: in out Logic_Clock_T;
                                        Shut_Down: out Boolean);

      -- In questa entry sono riaccodati i task provenienti da Player_Mode_Off
      -- che NON sono target per l'azione.
      entry Player_Mode_Off_Without_Target(Player_ID : Player_ID_T;
                                           Logic_Clock: in out Logic_Clock_T;
                                           Shut_Down: out Boolean);

      -- Oggetto intemediario, con tutte le informazioni riguardanti
      -- lo stato logico del gioco.
      M : Mediator_T;

      -- Segnale di arresto del core di gioco.
      Shut_Down_Signal : Boolean := False;

   end Status_T;

end Game.Status;
