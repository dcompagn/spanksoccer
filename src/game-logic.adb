--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Numerics.Elementary_Functions;
with Ada.Numerics.Discrete_Random;

package body Game.Logic is

   -- Distanza euclidea tra due punti
   function "-"(A: Position_T; B: Position_T) return Float is
      Displacement  : Displacement_T;
   begin
      Displacement.X := B.X - A.X;
      Displacement.Y := B.Y - A.Y;
      return Displacement_To_Distance(Displacement);
   end "-";

   function "-"(A: Position_T; B: Position_T) return Displacement_T is
   begin
      return (B.X - A.X, B.Y - A.Y);
   end "-";

   function "="(A: Position_T; B: Position_T) return Boolean is
   begin
      return (A.X = B.X and A.Y = B.Y);
   end "=";

   function Create_Status (My_Id: Player_ID_T;
                           Ball_Is_My : Boolean;
                           Ball_Position : Position_T;
                           Ball_Possesor : Natural;
                           Nearest_To_Ball : Boolean;
                           My_Position : Position_T;
                           My_Init_Position : Position_T;
                           Near_Targets : Targets_T) return Game_Status_T is
   begin
      return (My_Id,
              Ball_Is_My,
              Ball_position,
              Ball_Possesor,
              Nearest_To_Ball,
              My_Position,
              My_Init_Position,
              Near_Targets);
   end;

   function Velocity_To_Coordinate(Velocity : Natural) return Natural is
   begin
      return Natural((Float(33) / Float(Sleep_Factor))
              * (Float(Board_Size_X) / Float(120)));
   end Velocity_to_coordinate;

   function Displacement_To_Distance(Displacement : Displacement_T) return Float is
      A, B, C : Float;
   begin
      if Displacement.X = 0 and Displacement.Y = 0 then
         C := 0.0;
      else
         A := Float(Displacement.X);
         B := Float(Displacement.Y);
         C :=  Ada.Numerics.Elementary_Functions.Sqrt(A * A + B * B);
      end if;
      return C;
   end Displacement_To_Distance;

   function Follow_Target(Target_position : Position_T;
                          Current_position : Position_T;
                          Velocity: Positive) return Displacement_T is
      Move, Distance : Displacement_T := (X=>0 , Y=>0);
      S: Integer := 0;
      C : Float;
   begin
      -- calcola la distanza dal target

      -- se e' + allora devo andare ad E
      Distance.X := Target_position.X - Current_position.X;

      -- se e' + allora devo andare a S
      Distance.Y := Target_position.Y - Current_position.Y;

      if Distance.X /= 0 or Distance.Y /= 0 then

         -- di quanto mi posso muovere in base alla mia velocita'?
         S := Integer(Velocity_To_Coordinate(Velocity));

         C := Displacement_To_Distance(Distance);

         -- calcolo la proiezione su x e y della distanza limitata
         if C > Float(S) then
            C := Float(S) / C;
            Move.X := Integer(C * Float(Distance.X));
            Move.Y := Integer(C * Float(Distance.Y));
         else
            Move.X := Distance.X;
            Move.Y := Distance.Y;
         end if;
      end if;

      return Move;
   end Follow_target;

   function Minimum (A, B : Integer) return Integer is
   begin
      if A <= B then
      return A;
      else
         return B;
      end if;
   end Minimum;

   function Maximum (A, B       : Integer) return Integer is
   begin
      if A <= B then
         return B;
      else
         return A;
      end if;
   end Maximum;

   function Generate_Number (MaxValue : Integer) return Integer is
      subtype Random_Type is Integer range 1 .. MaxValue;
      package Random_Pack is new Ada.Numerics.Discrete_Random(Random_Type);
      G : Random_Pack.Generator;
   begin
      Random_Pack.Reset (G);
      return Random_Pack.Random (G);
   end Generate_Number;

   function Relative_Compass(Initial_position : Position_T;
                             Actual_position: Position_T;
                             Radius: Positive)
                             return Compass_T is
      T : Compass_T;
      -- origine del quadrato dell'areaa di influenza come angolo superiore sx
      Origin : Position_T :=
        (X => Initial_position.X - Radius, Y => Initial_position.Y - Radius);
   begin
      T.S := (Origin.Y + Radius * 2) - Actual_position.Y;  -- positivo sulle y
      T.N := T.S - Radius * 2;   			   -- negativo sulle y
      T.E := (Origin.X + Radius * 2) - Actual_position.X;  -- positivo sulle x
      T.W := T.E - Radius * 2; 				   -- negativo sulle x
      return T;
   end Relative_Compass;

   function Forward_Factor(Team : Team_T) return Integer is
   begin
      if Team = Team1 then
         return Direction_Team1;
      else
         return Direction_Team2;
      end if;
   end;

   function Is_Visible(My_Position : Position_T; Object_Pos : Position_T; Radius : Positive) return Boolean is
      Area: Compass_T := Relative_Compass(My_Position, Object_Pos, Radius);
   begin
      return Area.S >=0 and Area.N<=0 and Area.W <= 0 and Area.E >= 0;
   end Is_Visible;

end Game.Logic;
