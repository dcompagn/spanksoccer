--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Text_IO;
with Ada.Calendar;
use Ada.Calendar;
with Ada.Numerics.Discrete_Random;
with Game.Logic; use Game.Logic;
with Game.Logic.Mediator; use Game.Logic.Mediator;
with Ada.Strings.Unbounded;

-- Contiene lo stato di gioco
package body Game.Status is

   protected body Status_T is

      entry Player_Mode_On(Player_ID : Player_ID_T;
                           Logic_Clock: in Logic_Clock_T;
                           Shut_Down: out Boolean)
        -- Se il gioco e' in modalita' normale (On) oppure shut_down
        when M.Game_Mode=On or Shut_Down_Signal is

         Player_Status : Game_Status_T;
         Action : Action_T;
         A_T : Action_Type_T;
         Player : Player_Feature_Ref_T := M.Players_Feature(Player_ID);

      begin
         Shut_Down := Shut_Down_Signal;

         -- Aggiorno il clock contenuto nello stato rispetto a quello del
         -- giocatore corrente
         Update_Current_Clock(M,Logic_Clock);

         -- Sposto la posizione iniziale simulando il movimento della squadra in
         -- attacco o in difesa
         Move_Init_Position(M,Player_ID);

         -- Calcolo lo stato secondo il giocatore corrente
         Player_Status := Get_Game_Status_For(M, Player_ID);

         -- Per sempificare la logica invoco la funzione corretta di
         -- calcolo mossa in base allo stato (attacco o difesa) del giocatore.
         if M.Attack_Team = Get_Team(Player) then
            Action := Calculate_Attack_Action(Player, Player_Status);
         else
            Action := Calculate_Defence_Action(Player, Player_Status);
         end if;

         -- Imposto i dati critici per la mossa (ID, timestamp) per ragioni
         -- di sicurezza
         Set_Data(Action, Get_Absolute_Counter(Logic_Clock), Player_ID);

         Ada.Text_IO.Put("Clock "&Natural'Image(Get_Absolute_Counter(Logic_Clock)));
         Ada.Text_IO.Put(", Player "&Player_ID_T'Image(Player_ID));

         -- Valido la mossa.
         if Validate(M, Action) then

            -- Eseguo la mossa (scrittura stato)
            Perform_Action(M, Action);

            A_T := Get_Action_Type(Action);

            -- Log mossa
            Ada.Text_IO.Put(", Action : "
                            &Action_Type_T'Image(Get_Action_Type(Action)));
            case A_T is
               when Move =>
                  Ada.Text_IO.Put_Line(" pos("&
                                       Integer'Image(Get_Displacement(Action).X)&
                                       ","&
                                       Integer'Image(Get_Displacement(Action).Y)&
                                       ")");
               when Foul =>
                  Ada.Text_IO.Put_Line(" target-id("&
                                       Natural'Image(Get_Target_Id(Action))&
                                       ")");
               when Kick =>
                  Ada.Text_IO.Put_Line(" from("&
                                       Integer'Image(M.Ball_Status.Ball_Position.X)&
                                       ","&
                                       Integer'Image(M.Ball_Status.Ball_Position.Y)&
                                       ") to("&
                                       Integer'Image(Get_Target_Pos(Action).X)&
                                       ","&
                                       Integer'Image(Get_Target_Pos(Action).Y)&
                                       ") Strenght="&
                                       Integer'Image(Get_Strenght(Action)));
               when Tackle =>
                  Ada.Text_IO.Put_Line(" target-id("&
                                       Natural'Image(Get_Target_Id(Action))&
                                       ")");
               when others =>
                  Ada.Text_IO.Put_Line("");
            end case;

         else
            Ada.Text_IO.Put_Line("Action "&
                                 Action_Type_T'Image(Get_Action_Type(Action))&
                                 " not valid");
         end if;
      end Player_Mode_On;

      entry Player_Mode_Off(Player_ID : Player_ID_T;
                            Logic_Clock: in out Logic_Clock_T;
                            Shut_Down: out Boolean)
        -- Se il gioco e' in modalita' fermo (Off) oppure shut_down
        when M.Game_Mode=Off or Shut_Down_Signal is

         Player_Status : Game_Status_T;
         Action : Action_T;
         Player : Player_Feature_Ref_T := M.Players_Feature(Player_ID);

      begin
         Shut_Down := Shut_Down_Signal;

         -- Aggiorno il clock contenuto nello stato rispetto a quello del
         -- giocatore corrente
         Update_Current_Clock(M,Logic_Clock);

         -- Sposto la posizione iniziale simulando il movimento della squadra in
         -- attacco o in difesa
         Move_Init_Position(M,Player_ID);

         -- La get_near_target

         if Player_ID = M.Target_Player then
            -- Se Player_ID e' il giocatore target per l'azione ed e' in
            -- prossimita' della posizione target definita, e' riaccodato.
            -- La Get_Near_Target si occupa di definire il target.
            -- NB. Questa condizione potrebbe non essere mai raggiunta (dipende
            -- dalla logica del giocatore), conseguenza starvation.
            if Is_Visible(M.Players_Position(Player_ID),
                          M.Target_Position,
                          Threshold)
            then
               requeue Player_Mode_Off_With_Target;
            else
               -- Altrimenti gli viene data la possibilita' di raggiungerla.
               -- Utilizzo Calculate_Reposition_Action assumendo che si
               -- comporti nel modo atteso, convergendo alla condizione.
               Player_Status := Get_Game_Status_For(M,Player_ID);
               Action := Calculate_Reposition_Action(Player, Player_Status);
               Set_Data(Action, Get_Absolute_Counter(Logic_Clock), Player_ID);
               if Validate(M, Action) then
                  Perform_Action(M, Action);
               end if;
            end if;
         else
            -- Se il giocatore non e' target per l'azione, viene differenziata
            -- la condizione di riposizionamento, a seconda se sia avvenuto
            -- un goal, oppure un fallo.
            -- L'unica differenza consiste nella condizione di riaccodamento.
            case M.Game_Status_Type is
            when Reposition_For_Goal =>
               -- Se il riposizionamento e' conseguenza di un goal, il giocatore
               -- deve raggiungere la propria posizione iniziale (entro la meta'
               -- campo).
               -- NB. Questa condizione potrebbe non essere mai raggiunta
               -- (dipende dalla logica del giocatore), conseguenza starvation.
               if abs(M.Players_Position(Player_ID).X
                      - M.Players_Init_Position(Player_ID).X)
                 < Reposition_Threshold
                 and abs(M.Players_Position(Player_ID).Y
                         - M.Players_Init_Position(Player_ID).Y)
                 < Reposition_Threshold
               then
                  requeue Player_Mode_Off_Without_Target;
               else
                  -- Altrimenti gli viene data la possibilita' di raggiungerla.
                  -- Utilizzo Calculate_Reposition_Action assumendo che si
                  -- comporti nel modo atteso, convergendo alla condizione.
                  Player_Status := Get_Game_Status_For(M,Player_ID);
                  Action := Calculate_Reposition_Action(Player, Player_Status);
                  Set_Data(Action, Get_Absolute_Counter(Logic_Clock), Player_ID);
                  if Validate(M, Action) then
                     Perform_Action(M, Action);
                  end if;
               end if;
            when Others =>
               -- In tutti gli altri casi la condizione impone al giocatore di
               -- avere distanza minima dalla palla = Reposition_Threshold
               -- NB. Questa condizione potrebbe non essere mai raggiunta
               -- (dipende dalla logica del giocatore), conseguenza starvation.
               if not Is_Visible(M.Players_Position(Player_ID),
                                 M.Target_Position,
                                 Reposition_Threshold)
               then
                  requeue Player_Mode_Off_Without_Target;
               else
                  -- Altrimenti gli viene data la possibilita' di raggiungerla.
                  -- Utilizzo Calculate_Reposition_Action assumendo che si
                  -- comporti nel modo atteso
                  Player_Status := Get_Game_Status_For(M,Player_ID);
                  Action := Calculate_Reposition_Action(Player, Player_Status);
                  Set_Data(Action, Get_Absolute_Counter(Logic_Clock), Player_ID);
                  if Validate(M, Action) then
                     Perform_Action(M, Action);
                  end if;
               end if;
            end case;

         end if;

      end Player_Mode_Off;

      entry Player_Mode_Off_With_Target(Player_ID : Player_ID_T;
                                        Logic_Clock: in out Logic_Clock_T;
                                        Shut_Down: out Boolean)

        when M.Release or Shut_Down_Signal is

      begin
         Shut_Down := Shut_Down_Signal;
         if Shut_Down_Signal = False then

            -- Controllo se il player deve essere sostituito, in caso
            -- affermativo il clock logico e' modificato secondo le abilita'
            -- del nuovo giocatore
            Check_For_Player_Replacement(M, Player_ID, Logic_Clock);

            -- In seguito all'attesa su guardia il riferimento temporale potrebbe
            -- non risultare allineato, dunque e' necessario il riallineamento
            -- In caso di sostituzione vale la stessa condizione, poiche' il
            -- clock e' cambiato.
            Update_My_Clock(M,Logic_Clock);

            -- Sposto la posizione iniziale simulando il movimento della squadra in
            -- attacco o in difesa
            Move_Init_Position(M,Player_ID);
         end if;
      end Player_Mode_Off_With_Target;

      entry Player_Mode_Off_Without_Target(Player_ID : Player_ID_T;
                                           Logic_Clock: in out Logic_Clock_T;
                                           Shut_Down: out Boolean)

        when (M.Release and Player_Mode_Off_With_Target'Count = 0)
        or Shut_Down_Signal is

      begin

         Shut_Down := Shut_Down_Signal;

         -- Controllo se il player deve essere sostituito, in caso
         -- affermativo il clock logico e' modificato secondo le abilita'
         -- del nuovo giocatore
         Check_For_Player_Replacement(M, Player_ID, Logic_Clock);

         -- In seguito all'attesa su guardia il riferimento temporale potrebbe
         -- non risultare allineato, dunque e' necessario il riallineamento
         -- In caso di sostituzione vale la stessa condizione, poiche' il
         -- clock e' cambiato.
         Update_My_Clock(M,Logic_Clock);

         -- Sposto la posizione iniziale simulando il movimento della squadra in
         -- attacco o in difesa
         Move_Init_Position(M,Player_ID);

         -- L'ultimo player chiude la guardia (Carraresi). Non e' fondamentale,
         -- perche' si passa in modalita' On.
         if Player_Mode_Off_Without_Target'Count = 0 then
            M.Release := False;
         else
            M.Release := True;
         end if;
      end Player_Mode_Off_Without_Target;

      procedure Referee(Logic_Clock: in Logic_Clock_T; Shut_Down: out Boolean) is

         Foul_Seen : Boolean := False;

      begin
         Shut_Down := Shut_Down_Signal;

         -- Aggiorno il clock contenuto nello stato rispetto a quello del
         -- Referee.
         Update_Current_Clock(M,Logic_Clock);

         Ada.Text_IO.Put("Clock "&Natural'Image(Get_Absolute_Counter(Logic_Clock)));
         Ada.Text_IO.Put(", Referee ");

         case M.Game_Mode is
            when On =>

               -- Aggiorno la posizione del Referee
               Move_Referee(M);

               -- Controllo se vi sono falli pendenti dall'unltima esecuzione
               -- del Referee. In caso affermativo viene cambiata la modalita'
               -- di gioco ad Off.
               Check_For_Foul(M);

            when Off =>

               -- Se tutti i giocatori sono riposizionati ed il clock e'
               -- nell'edge di un iperperiodo, apro la guardia
               -- "Player_Mode_Off_With_Target"
               if (Player_Mode_Off_Without_Target'Count
                   + Player_Mode_Off_With_Target'Count = M.Players_Number
                   and Get_Relative_Period(Logic_Clock) = 0) then

                  Ada.Text_IO.Put_Line(" to game mode ON");

                  -- Torno alla modalita' On
                  Change_Mode_To_On(M);
               else

                  -- Altrimenti aggiorno la posizione del Referee
                  Move_Referee(M);

                  Ada.Text_IO.Put_Line(" on game mode OFF");
               end if;

         end case;

      end Referee;

      entry Ball(Logic_Clock : in out Logic_Clock_T; Shut_Down: out Boolean)
        when M.Ball_Status.Ball_Release
        or Get_Absolute_Counter(M.Current_Clock) >= Game.Match_Duration
        or Shut_Down_Signal is
         Displacement : Displacement_T;
      begin
         Shut_Down := Shut_Down_Signal;

         -- In seguito all'attesa su guardia il riferimento temporale potrebbe
         -- non risultare allineato, dunque la palla e' riallineata
         if Get_Absolute_Counter(M.Current_Clock)
           > Get_Absolute_Counter(Logic_Clock) then

            Align(Logic_Clock,M.Current_Clock);

         else

            -- Aggiorna il clock logico globale se ha un valore inferiore
            Update_Current_Clock(M, Logic_Clock);

            Ada.Text_IO.Put("Clock "&Natural'Image(Get_Absolute_Counter(Logic_Clock)));
            Ada.Text_IO.Put(", Ball pos("&
                            Integer'Image(M.Ball_Status.Ball_Position.X)&
                            ","&Integer'Image(M.Ball_Status.Ball_Position.Y)&")"&
                            " target("&Integer'Image(M.Ball_Status.Ball_Target.X)&
                            ","&Integer'Image(M.Ball_Status.Ball_Target.Y)&")");

            -- Sposto la palla
            Move_Ball(M, False);

            -- Controllo gli eventi causati dalla palla come l'out ed il goal
            Check_Ball_Event(M);

            Ada.Text_IO.Put_Line("");
         end if;

      end Ball;

      procedure Start_Match(Players_Number : in Natural;
                            Players_Feature : in Players_Feature_T;
                            Scheme: in Scheme_T) is
      begin
         if M.Start_Signal = False then
            -- Creo ed inizializzo lo stato di gioco (oggetto Intermediario)
            M := Create(Players_Number, Players_Feature, Scheme);
            Init(M);

            -- Calcolo l'ampiezza dell'iperperiodo come mcm dei tempi di
            -- sospensione di ogni singolo task
            Game.Hyperperiod_Const := M_LCM(M);

            -- Calcolo la durata del match
            Game.Match_Duration := 540 * Game.Sleep_Factor;

            -- Posiziono gli oggetti nel campo
            Place_Object(M);

            -- Abilito l'avvio del match
            M.Start_Signal := True;

            Ada.Text_IO.Put_Line("Test : Match initialized, players: "&
                                 Natural'Image(Players_Number)&
                                 ", hyperperiod: "&
                                 Positive'Image(Game.Hyperperiod_Const)&
                                 " time unit");

            -- Prendo il tempo globale di inizio match
            M.Start_Time := Ada.Calendar.Clock;
         end if;
      end Start_Match;

      entry Waiting_For_Start_Signal_To_Other(Number_Of_Player: out Natural;
                                              Start_Time: out Ada.Calendar.Time;
                                              Shut_Down: out Boolean)
        when M.Start_Signal or Shut_Down_Signal is
      begin
         -- In caso di shutdown attribuisco dei valori di default poiche'
         -- l'intermediario potrebbe non ancora esser stato creato
         -- (prima di Start_Match)
         if Shut_Down_Signal then
            Shut_Down := Shut_Down_Signal;
            Number_Of_Player := 1;
            Start_Time := Ada.Calendar.Clock;
         else
            -- Assegno alle variabili di ritorno i valori correttamente
            -- inizializzati da parte di Start_Match
            M.Active_Task := M.Active_Task + 1;
            Ada.Text_IO.Put_Line("Active Task : "&Natural'Image(M.Active_Task));
            Number_Of_Player := M.Players_Number;
            Start_Time := M.Start_Time;
            Shut_Down := Shut_Down_Signal;
         end if;
      end Waiting_For_Start_Signal_To_Other;

      entry Waiting_For_Start_Signal_To_Player(ID: out Player_ID_T;
                                               Sleep: out Positive;
                                               Start_Time: out Ada.Calendar.Time;
                                               Shut_Down: out Boolean)

        when (M.Start_Signal = True and
                (M.Player_Activation_Counter < M.Players_Number))
        or Shut_Down_Signal = True is

      begin
         -- In caso di shutdown attribuisco dei valori di default poiche'
         -- l'intermediario potrebbe non ancora esser stato creato
         -- (prima di Start_Game)
         if Shut_Down_Signal then
            Shut_Down := Shut_Down_Signal;
            ID := Player_ID_T'First;
            Sleep := Positive'First;
            Start_Time := Ada.Calendar.Clock;
         else
            -- Assegno alle variabili di ritorno i valori correttamente
            -- inizializzati da parte di Start_Match
            M.Active_Task := M.Active_Task + 1;
            M.Player_Activation_Counter := M.Player_Activation_Counter + 1;

            -- Ottengo un ID "libero" per il task
            Get_Player_ID(M, ID);
            Sleep := Feature_To_Sleep(M.Players_Feature(ID));
            Start_Time := M.Start_Time;
            Shut_Down := Shut_Down_Signal;
            Ada.Text_IO.Put_Line("Player : "&Player_ID_T'Image(ID)&
                                 " initialized, Sleep="&Positive'Image(Sleep)&
                                 " Role="&
                                 Role_T'Image(Get_Role(M.Players_Feature(ID))));
         end if;
      end Waiting_For_Start_Signal_To_Player;

      entry Ready_To_End(Shut_Down: out Boolean)
        when True is
      begin
         Shut_Down := Shut_Down_Signal;

         -- Decremento il numero di task attivi
         M.Active_Task := M.Active_Task - 1;

         Ada.Text_IO.Put_Line("Active Task : "&Natural'Image(M.Active_Task));
         requeue Waiting_For_End_Others;
      end Ready_To_End;

      entry Waiting_For_End_Others(Shut_Down: out Boolean)
        when M.Active_Task = 0 or Shut_Down_Signal is
      begin
         Shut_Down := Shut_Down_Signal;

         -- Disabilito il segnale di start match
         M.Start_Signal := False;

         Ada.Text_IO.Put_Line("Task : ended");
      end Waiting_For_End_Others;

      procedure Release_Players is
      begin
         Change_Mode_To_On(M);
      end Release_Players;

      procedure Manager(Action : in Action_T) is
         A : Action_T := Action;
      begin
         -- Inserisce l'azione del Manager all'interno della lista di
         -- azioni "differite". Saranno valutate al momento opportuno
         Set_Data(A, Get_Absolute_Counter(M.Current_Clock), Player_ID_T'First);
         Insert_To_Deferred_Replacement(M, A);
      end Manager;

      procedure Shut_Down is
      begin
      	Shut_Down_Signal := True;
      end Shut_Down;

      entry Broker (Timestamp: out Natural;
                    Ball_Position: out Position_T;
                    Referee_Position: out Position_T;
                    Players_Position: in out Player_Sequence_Wrapper_T;
                    Shut_Down : out Boolean;
                    Duration_match : out Natural;
                    Score_team_1 : out Natural;
                    Score_team_2 : out Natural;
                    Control : out Integer )

        when M.Start_Signal or Shut_Down_Signal is

         Even_Pos : Positive := Players_Position'First + 1;
         Odd_Pos : Positive := Players_Position'First;
         Name : Ada.Strings.Unbounded.Unbounded_String;
         Energy, Foul : Natural;
         Distance : Float;

      begin

         Ada.Text_IO.Put_Line("Broker : Status readed");

         Shut_Down := Shut_Down_Signal;

         -- Raccolgo i dati che saranno poi pubblicati ai pannelli remoti
         -- per la visualizzazione
         Timestamp := Get_Absolute_Counter(M.Current_Clock);
         Duration_match := Match_Duration;
         Score_team_1 := M.Stat.Goal_Team1;
         Score_team_2 := M.Stat.Goal_Team2;
         Ball_Position := M.Ball_Status.Ball_Position;

         Referee_Position := M.Referee_Position;

         Control := Game_Status_Type_T'Pos(M.Game_Status_Type);

         -- Per convenzione inserisco le informazioni dei giocatori del Team1
         -- nelle posizioni dispari mentre quelle del Team2 nelle posizioni pari
         for P in M.Players_Active'Range loop

            if M.Players_Active(P) then

               Name := Get_Name(M.Players_Feature(P));
               Energy := M.Stat.Players_Stat(P).Energy * 100
                 / Get_Energy(M.Players_Feature(P));
               Foul := M.Stat.Players_Stat(P).Foul;
               Distance := M.Stat.Players_Stat(P).Distance;

               if (P rem 2) = 0 then
                  Players_Position(Even_Pos) := (Integer(P),
                                                 Name,
                                                 Energy,
                                                 Foul,
                                                 Distance,
                                                 M.Players_Position(P));
                  Even_Pos := Even_Pos + 2;
               else
                  Players_Position(Odd_Pos) := (Integer(P),
                                                Name,
                                                Energy,
                                                Foul,
                                                Distance,
                                                M.Players_Position(P));
                  Odd_Pos := Odd_Pos + 2;
               end if;

            end if;

         end loop;

      end Broker;

   end Status_T;

end Game.Status;
