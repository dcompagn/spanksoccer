--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game.Status; use Game.Status;
with Game.Logic.Mediator; use Game.Logic.Mediator;
with Game.Logic.Features;
with Ada.Text_IO; use Ada.Text_IO;

package body Game.Test is

   task body Tester is
      --Players_Feature : Players_Feature_T;
   begin
      --Ada.Text_IO.Put_Line("Test Started");
      --delay 2.0;
      --Players_Feature := Game.Logic.Features.Load_Features;
      --Ada.Text_IO.Put_Line("Features Loaded from file");
      --Status_T.Initialize(Players_Number => 22, Players_Feature => Players_Feature);
      --delay 5.0;
      --Status_T.Start_Game;
      --delay 10.0;
      --Status_T.Shut_Down;
      null;
   end Tester;

end Game.Test;
