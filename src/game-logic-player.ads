--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Strings;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Game.Logic.Action; use Game.Logic.Action;

-- Questo package contiene tutto quello che riguarda la logica dei giocatori.
-- In particolare definisce il tipo Player_Features_T, ed implementa le funzioni
-- per il calcolo delle mosse. Viene ribadito il concetto che l'IA, non essendo
-- oggetto di questo prototipo, non e' stata curata e riflette un modello di
-- gioco notevolmente semplificato, ma l'architettura ne permette un successivo
-- raffinamento, anche per singolo giocatore.
-- Per semplificare la comprensione della logica di gioco e' stata suddiva
-- a seconda che il giocatore debba calcolare una mossa in uno stato di
-- attacco, difesa, o a gioco fermo.
package Game.Logic.Player is

   -- Tipo abilita' di un giocatore
   type Ability_T is private;

   -- Tipo che descrive un giocatore. Contiene Ability_T
   type Player_Feature_T is tagged limited private;

   -- Riferimento ad un tipo Player_Feature_T
   type Player_Feature_Ref_T is access Player_Feature_T;

   -- Crea un oggetto di tipo giocatore
   function Create(Name : Unbounded_String;
                   Team : Team_T;
                   Role : Role_T;
                   Abilities: Ability_T;
                   Energy: Positive;
                   Influence_Zone_Radius:Positive) return Player_Feature_Ref_T;

   -- Crea un oggetto di tipo abilita'
   function Create_Abilities(Shoot_power : Positive;
                             Velocity : Positive;
                             Tackle : Positive;
                             Pass_Probability : Positive) return Ability_T;

   -- Calcola la mossa da effettuare a seconda dello stato corrente del gioco.
   -- E' invocato all'interno della RP quindi e' esposto a problemi di deadline
   -- miss.
   -- This : Rappresenta l'oggetto di invocazione
   -- Game_Status : E' la visione dello stato di gioco secondo il giocatore
   -- in questione
   function Calculate_Attack_Action (This : Player_Feature_Ref_T;
                                     Game_Status : Game_Status_T) return Action_T;
   function Calculate_Defence_Action (This : Player_Feature_Ref_T;
                                      Game_Status : Game_Status_T) return Action_T;
   function Calculate_Reposition_Action (This : Player_Feature_Ref_T;
                                         Game_Status : Game_Status_T) return Action_T;

   -- Restituisce il tempo di sospensione unitario in base alla velocita' del
   -- giocatore. Questo valore sara' poi diviso dal fattore velocita' di gioco
   -- (Sleep_Factor) in Game.
   function Feature_To_Sleep(This : Player_Feature_Ref_T) return Positive;

   -- Restituisce il team di appartenenza del giocatore
   function Get_Team(This : Player_Feature_Ref_T) return Team_T;

   -- Restituisce la potenza di tiro del giocatore
   function Get_Shoot_power(This : Player_Feature_Ref_T) return Positive;

   -- Restituisce il ruolo del giocatore
   function Get_Role(This : Player_Feature_Ref_T) return Role_T;

   -- Restituisce il raggio d'azione del giocatore
   function Get_Radius(This : Player_Feature_Ref_T) return Positive;

   -- Restituisce il fattore di contrasto del giocatore
   function Get_Tackle(This : Player_Feature_Ref_T) return Positive;

   -- Restituisce il nome del giocatore
   function Get_Name(This : Player_Feature_Ref_T) return Unbounded_String;

   -- Restituisce l'energia del giocatore
   function Get_Energy(This : Player_Feature_Ref_T) return Positive;

   -- Restituisce la velocita' del giocatore
   function Get_Velocity(This : Player_Feature_Ref_T) return Positive;

   -- Restituisce la probabilita' di passare la palla del giocatore
   function Get_Pass_Probability(This : Player_Feature_Ref_T) return Positive;

private

   -- Questi sono i record relativi ai metodi Get_ descritti sopra.

   type Ability_T is
      record
         Shoot_power : Positive;
         Velocity : Natural;
         Tackle : Positive;
         Pass_Probability : Positive;
      end record;

   type Player_Feature_T is tagged limited
      record
         Name : Unbounded_String;
         Team : Team_T;
         Role : Role_T;
         Abilities: Ability_T;
         Energy: Positive;
         Influence_Zone_Radius: Positive;
      end record;

   -- Funzione accessoria per il calcolo della mossa da parte del portiere
   function Calculate_Goalkeeper_Action (This : Player_Feature_Ref_T; Game_Status : Game_Status_T) return Action_T;

end Game.Logic.Player;
