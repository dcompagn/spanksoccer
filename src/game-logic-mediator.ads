--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game.Clock; use Game.Clock;
with Game.Logic.Action; use Game.Logic.Action;
with Game.Logic.Player; use Game.Logic.Player;
with Ada.Calendar;
with Ada.Containers.Doubly_Linked_Lists;

package Game.Logic.Mediator is

   -- Discrimina le due modalita' di gioco. Usato come valore di guardia.
   type Game_Mode_T is (On, Off);

   -- Tipo array di flag indicizzato sul tipo identificativo giocatore
   type Players_Active_T is array (Player_ID_T) of Boolean;

   -- Tipo array di riferimenti agli oggetti contenenti le caratt. dei giocatori
   -- indicizzato sul tipo identificativo giocatore
   type Players_Feature_T is array (Player_ID_T) of Player_Feature_Ref_T;

   -- Tipo array di posizioni indicizzato sul tipo identificativo giocatore
   type Players_Position_T is array (Player_ID_T) of Position_T;

   -- Tipo array indicizzato sui ruoli
   type Players_Counter_T is array (Role_T) of Natural;

   -- Tipo che contiene lo stato della palla
   type Ball_Status_T is
      record
         Ball_Release   : Boolean; 	-- Indica se la palla e' rilasciata
         Player_With_Ball : Natural; 	-- ID del giocatore che possiede la
         				--   palla, 0 altrimenti
         Ball_Position : Position_T;	-- Posizione attuale
         Ball_Target : Position_T;	-- Posizione da raggiungere
         Energy : Natural;		-- Energia
      end record;

   -- Tipo che colleziona le statistiche per un giocatore. Usato per costruire
   -- Player_Sequence_Wrapper_T in Game.Logic
   type Player_Stat_T is
      record
         Distance : Float; 	-- Distanza percorsa
         Foul : Natural; 	-- Numero di falli commessi
         Energy : Natural; 	-- Energia del giocatore
      end record;

   -- Collezione di valori Player_Stat_T
   type Players_Stat_T is array (Player_ID_T) of Player_Stat_T;

   -- Tipo che contiene le statistiche di gioco
   type Stat_T is
      record
         Goal_Team1 : Natural;		-- Goal per il Team1
         Goal_Team2 : Natural;		-- Goal per il Team2
         Players_Stat : Players_Stat_T; -- Statistiche dei giocatori
      end record;

   -- Tipo lista unbounded che permette di mantenere le azioni con valutazione
   -- posticipata (azioni differite)
   package Action_Queue is new
     Ada.Containers.Doubly_Linked_Lists(Action_T);

   -- Tipo intermediario, contiene la totalita' di informazioni riguardanti
   -- lo stato (globale) di gioco
   type Mediator_T is
      record
         -- Permette di rilasciare i task una volta bloccati nelle entry
         -- "Player_Mode_Off_With_Target" e "Player_Mode_Off_Without_Target"
         Release : Boolean;

         -- Modalita' di gioco
         Game_Mode      : Game_Mode_T;

         -- Team in attacco, e' modificato quando un giocatore prende palla
         -- attraverso Take_Ball
         Attack_Team : Team_T;

         -- Stato corrente di gioco. Permette di discriminare il riposizionamento
         -- dei giocatori durante la modalita' Off di gioco
         Game_Status_Type : Game_Status_Type_T;

         -- Istante di avvio del match
         Start_Time : Ada.Calendar.Time;

         -- Valore del clock logico. Utilizzato come riferimento per allineare
         -- Player e Ball
         Current_Clock  : Logic_Clock_T;

         -- Posizione dell'arbitro
         Referee_Position : Position_T;

         -- Stato della palla
         Ball_Status : Ball_Status_T;

         -- ID del giocatore target individuato per l'azione (es. chi batte il
         -- fallo) e la posizione da raggiungere.
         Target_Player : Player_ID_T;
         Target_Position : Position_T;

         -- Segnale che avvia il match
         Start_Signal   : Boolean;

         -- Numero di task attivi
         Active_Task : Natural;

         -- Numero di giocatori individuato per il match corrente (6, 10, 22)
         Players_Number : Positive;

         -- Numero di giocatori attivi, determina la chiusura della guardia
         -- in Waiting_For_Start_Signal_To_Player
         Player_Activation_Counter : Natural;

         -- Contatore dell'ultimo ID assegnato ad un giocatore
         ID_Counter : Player_ID_T;

         -- Numero di giocatori per ruolo nel Team1 e nel Team2, varia a seconda
         -- dello schema di gioco
         Team1_Players_Counter : Players_Counter_T;
         Team2_Players_Counter : Players_Counter_T;

         -- Schema di gioco. Per semplicita' e' uguale in entrambi i team
         Game_Scheme : Scheme_T;

         -- Array che indica se un giocatore deve essere attivato o meno.
         -- Varia in base allo schema di gioco e permette l'assegnamento degli ID
         Players_Active : Players_Active_T;

         -- Contiene i riferimenti agli oggetti features dei giocatori
         Players_Feature : Players_Feature_T;

         -- Contiene le posizioni attuali dei giocatori
         Players_Position : Players_Position_T;

         -- Contiene le posizioni iniziali dei giocatori (possono shiftare
         -- in base allo stato di gioco)
         Players_Init_Position : Players_Position_T;

         -- Contiene le posizioni di riferimento dei giocatori (non vengono mai
         -- modificate)
         Players_Ref_Position : Players_Position_T;

         -- Azioni con valutazione differita eseguite dai giocatori
         Deferred_Action : Action_Queue.List;

         -- Azioni con valutazione differita eseguite dal manager
         Deferred_Replacement : Action_Queue.List;

         -- Statistiche di gioco
         Stat : Stat_T;

      end record;

   -- Crea un oggetto di tipo intermediario con valori di default
   function Create (Players_Number : Positive;
                    Players_Feature : Players_Feature_T;
                    Game_Scheme : Scheme_T) return Mediator_T;

   -- Inizializza un oggetto di tipo intermediario in base al match corrente
   procedure Init(Mediator : in out Mediator_T);

   -- Calcola l'mcm dei tempi di sospensione di tutti i task.
   -- Determina l'iperperiodo
   function M_LCM(Mediator : Mediator_T) return Integer;

   -- Ritorna un ID libero per il task che lo richiede
   procedure Get_Player_ID (Mediator : in out Mediator_T; ID : out Player_ID_T);

   -- Valida la mossa calcolata dal giocatore prima che essa sia eseguita
   function Validate(Mediator : Mediator_T; Action : Action_T) return Boolean;

   -- Esegue la mossa alterando lo stato di gioco.
   procedure Perform_Action(Mediator : in out Mediator_T; Action : in Action_T);

   -- Cambia il modo di gioco ad On
   procedure Change_Mode_To_On (Mediator : in out Mediator_T);

   -- Cambia il modo di gioco ad Off
   procedure Change_Mode_To_Off (Mediator : in out Mediator_T);

   -- Allinea il valore del clock logico globale se precedente a quello del
   -- task in esecuzione in mutua esclusione sullo stato.
   procedure Update_Current_Clock(Mediator : in out Mediator_T;
                                  Clock: in Logic_Clock_T);

   -- Allinea il clock logico del task se ha un valore inferiore a quello
   -- globale.
   procedure Update_My_Clock(Mediator : in Mediator_T;
                             Clock    : in out Logic_Clock_T);

   -- Ottiene le informazioni di stato per il giocatore, affinghe' possa
   -- calcolare la propria mossa
   function Get_Game_Status_For(Mediator : Mediator_T;
                                ID_Player : Player_ID_T) return Game_Status_T;

   -- Dispone le entita' nel campo
   procedure Place_Object(Mediator : in out Mediator_T);

   -- Inserisce l'azione nella lista delle sostituzioni
   procedure Insert_To_Deferred_Replacement(Mediator : in out Mediator_T;
                                            Action : in Action_T);

   -- Inserisce l'azione nella lista delle azioni differite dei giocatori
   procedure Insert_To_Deferred_Action(Mediator : in out Mediator_T;
                                       Action : in Action_T);

   -- Controlla eventuali falli presenti tra le azioni differite tra due
   -- turni del Referee. Il primo fallo visto annulla eventuali altri falli
   -- pendenti.
   procedure Check_For_Foul(Mediator : in out Mediator_T);

   -- Effettua le sostituzioni pendenti. Modifica le caratteristiche del
   -- giocatore, con side-effect potenziale sul tempo di sospensione del task.
   procedure Check_For_Player_Replacement(Mediator : in out Mediator_T;
                                          Player_ID : in Player_ID_T;
                                          Clock : in out Logic_Clock_T);

   -- Assegna il possesso della palla al giocatore indicato. Viene bloccata
   -- l'entry Ball evitando il polling del relativo task.
   procedure Take_Ball(Mediator     : in out Mediator_T;
                       Player_Id    : in Player_ID_T);

   -- Rilascia il possesso palla, e conseguentemente sblocca l'entry Ball.
   procedure Release_Ball(Mediator : in out Mediator_T);

   -- Dispone la palla nella posizione indicata
   procedure Place_Ball(Mediator : in out Mediator_T; Position : in Position_T);

   -- Muove la palla verso la posizione target proporzionalmente all'energia
   -- posseduta e se non e' ostacolata
   procedure Move_Ball(Mediator : in out Mediator_T; Kicked : in Boolean);

   -- Muove il Referee, proporzionalmente alla sua velocita', verso la palla.
   procedure Move_Referee(Mediator : in out Mediator_T);

   -- Sposta la posizione iniziale del giocatore simulando l'avanzamento in
   -- stato di attacco.
   procedure Move_Init_Position(Mediator : in out Mediator_T;
                                Player_Id : in Player_ID_T);

   -- Verifica eventuali eventi (goal, out) conseguenti al movimento della palla
   procedure Check_Ball_Event(Mediator : in out Mediator_T);

private

   -- Calcola la posizione di riferimento di ogni giocatore in base allo schema
   -- di gioco
   procedure Set_Ref_Player_Position(Mediator : in out Mediator_T);
   -- Copia la posizione di riferimento
   procedure Set_Init_Player_Position(Mediator : in out Mediator_T);
   -- Copia la posizione di riferimento
   procedure Set_Player_Position(Mediator : in out Mediator_T);

   -- Dispone i giocatori nella posizione di inizio match
   procedure Place_Players(Mediator : in out Mediator_T);

   -- Ritorna il numero di giocatori di uno stesso ruolo
   function Player_For_Role(Players : Positive;
                            Role: Role_T;
                            Scheme: Scheme_T) return Natural;

   -- Ritorna i possibili target nel raggio d'azione del giocatore
   function Get_Near_Target_Of(Mediator : Mediator_T;
                               ID_Player : Player_ID_T) return Targets_T;

   -- Ritorna True se e' il giocatore del proprio team piu' vicino alla palla.
   -- E' una buona invariante per evitare la starvation del gioco in quanto
   -- esiste sempre un giocatore con questa proprieta'. Inoltre minimizza gli
   -- spostamenti dei giocatori e l'effetto sovraffollamento sulla palla.
   function Nearest_To_Ball(Mediator : Mediator_T;
                            ID_Player : Player_ID_T) return Boolean;

   -- Crea un oggetto Ball_Status
   function Create_Ball_Status return Ball_Status_T;

   -- Ritorna True se il giocatore possiede la palla
   function Have_Ball(Mediator : Mediator_T;
                      ID_Player : Player_ID_T) return Boolean;

   -- Rileva eventuali ostacoli durante lo spostamento del giocatore.
   -- Ritorna lo spostamento fino all'eventuale punto di scontro.
   function Player_Overlap_Detection (Mediator : Mediator_T;
                                      ID_Player : Player_ID_T;
                                      Displacement : Displacement_T)
                                      return Displacement_T;

   -- Rileva eventuali ostacoli durante lo spostamento della palla.
   -- Ritorna lo spostamento fino all'eventuale punto di scontro.
   function Ball_Overlap_Detection (Mediator : Mediator_T;
                                    Displacement : Displacement_T)
                                    return Displacement_T;

   -- Rileva eventuali ostacoli durante lo spostamento dell'arbitro.
   -- Ritorna lo spostamento fino all'eventuale punto di scontro.
   function Referee_Overlap_Detection (Mediator : Mediator_T;
                                       Displacement : Displacement_T)
                                       return Displacement_T;

   -- Funzione ausiliari che permettono di rilevare sovrapposizioni
   -- tra due posizioni
   function Angle_Left (P0 : Position_T;
                        P1 : Position_T;
                        P2 : Position_T) return Integer;

   -- Funzione ausiliari che permettono di rilevare sovrapposizioni
   -- tra due posizioni. Usa Angle_Left
   function Check_Overlap(P0 : Position_T;
                          D : Displacement_T;
                          P2 : Position_T) return Boolean;

   -- Ritorna la posizione immediatamente precedente al punto di sovrapposizione
   function Pred_Displacement (Displacement : Displacement_T)
                               return Displacement_T;

   -- Crea un oggetto di statistiche giocatore
   function Create_Player_Stat return Player_Stat_T;

   -- Crea un oggetto di tipo statistiche match
   function Create_Stat return Stat_T;

   -- Ritorna True se il giocatore e' completamente stanco
   function Is_Empty_Player_Energy(Mediator : Mediator_T;
                                   ID : Player_ID_T) return Boolean;

   -- Decrementa l'energia del giocatore
   procedure Dec_Player_Energy(Mediator : in out Mediator_T;
                               ID : in Player_ID_T);

   -- Incrementa l'energia del giocatore
   procedure Inc_Player_Energy(Mediator : in out Mediator_T;
                               ID : in Player_ID_T);

   -- Somma la distanza percorsa del giocatore alle statistiche
   procedure Add_Player_Distance(Mediator : in out Mediator_T;
                                 ID : in Player_ID_T; Distance : Float);

   -- Incrementa il numero di falli del giocatore
   procedure Inc_Player_Foul(Mediator : in out Mediator_T;
                             ID : in Player_ID_T);

end Game.Logic.Mediator;
