--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

package body Game.Clock is

   function Create (Value: Positive; MCM: Positive) return Logic_Clock_T is
   begin
      return (0, Value, MCM);
   end;

   function Get_Absolute_Counter (Clock: Logic_Clock_T) return Natural is
   begin
      return Clock.Tick_Counter;
   end;

   function Get_Relative_Period (Clock: Logic_Clock_T) return Natural is
   begin
      return Clock.Tick_Counter rem Clock.MCM_Value;
   end;

   procedure Tick (Clock: in out Logic_Clock_T) is
   begin
      Clock.Tick_Counter := Clock.Tick_Counter + Clock.Increment_Value;
   end;

   procedure Align (Clock: in out Logic_Clock_T; ToClock: in Logic_Clock_T) is
   begin
      Clock.Tick_Counter := (ToClock.Tick_Counter / Clock.Increment_Value)
        * Clock.Increment_Value;
   end;

   function Get_Sleep(Clock : Logic_Clock_T) return Positive is
   begin
      return Clock.Increment_Value;
   end Get_Sleep;


end Game.Clock;
