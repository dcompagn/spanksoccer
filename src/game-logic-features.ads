--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Game.Logic.Mediator; use Game.Logic.Mediator;

-- Questo package e' responsabile della lettura delle features dei giocatori
-- da un file Xml, attraverso la libreria Xmlada. La lettura avviene attraverso
-- il DOM.
-- E' importante che il file Xml contenga il numero sufficiente di giocatori
-- per l'avvio della partita, considerando anche il ruolo per schema di gioco.
package Game.Logic.Features is

   -- Carica le features indicate dal File_Path e le ritorna in un array
   function Load_Features(File_Path : String) return Players_Feature_T;

end Game.Logic.Features;
