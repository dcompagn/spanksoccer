--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Calendar;
use Ada.Calendar;
with Game.Status;
use Game.Status;
with Game.Clock; use Game.Clock;
with Ada.Text_IO;
with Game.Logic;

package body Game.Referee is

   -- Il comportamento di questo task e' del tutto simile a quello
   -- di un Player, preservandone cosi' le proprieta' di correttezza
   -- e terminazione. La principale differenza consiste nel fatto che il task
   -- non e' soggetto ad avoidance syncr. sulla RP percio' il comportamento e'
   -- puramente periodico.
   task body The_Referee is

      -- Istante temporale unico e globale di avvio partita
      -- Rappresenta l'istante temporale al quale il task sara' re-inserito
      -- nella coda dei pronti. Inizialmente e' l'istante temporale unico e
      -- globale di avvio partita
      Wake_Up : Time;

      -- Rappresenta il tempo di sospensione del task in unita', e' utilizzato
      -- per calcolare il tempo di sospensione
      Sleep : Positive := Game.Logic.Referee_Sleep;

      -- Rappresenta il tempo di sopensione effettivo con tipo Duration,
      -- calcolato come il rapporto tra Sleep e Sleep_Factor
      Sleep_Time : Standard.Duration :=
        Standard.Duration(Float(Sleep)/Float(Sleep_Factor));

      -- Valore di clock logico. Non e' mai riallineato per questo task.
      -- Permette di stabilire precisamente se l'istante attuale e' l'edge di
      -- un iperperiodo, affinche' si possa determinare la condizione di
      -- apertura delle guardie sulle entry "Player_Mode_Off_With_Target" e
      -- "Player_Mode_Off_Without_Target". (evita l'attesa attiva dei giocatori)
      Logic_Clock : Logic_Clock_T;

      -- Valore che indica se la condizione di terminazione del match � stata
      -- raggiunta, permette di uscire dal loop pi� interno
      End_Match: Boolean := False;

      -- Valore che permette di terminare il task istantaneamente, ottenuto
      -- dallo stato globale, True in caso di chiusura del pannello manager
      Shut_Down: Boolean := False;

      -- Non utilzzato, aggiunto per compatibilit� con l'entry del task Broker
      Players_Number : Positive;
   begin

      -- I due loop innestati permettono di riavviare un match qualora sia
      -- giunto a terminazione, simmetricamente a tutti i task
      loop

         End_Match := False;

         -- Inizializzazione del clock logico
         Logic_Clock := Create(Sleep,Hyperperiod_Const);

         -- Il task e' posto in attesa del segnale di start match. Questo e'
         -- un punto di sinconizzazione su Status_T
         Status_T.Waiting_For_Start_Signal_To_Other(Players_Number,
                                                    Wake_Up,
                                                    Shut_Down);

         if not Shut_Down then

            loop

               -- La terminazione del match avviene non appena il clock logico
               -- assume un valore superiore alla Match_Duration.
               if Shut_Down = True or Get_Absolute_Counter(Logic_Clock) > Game.Match_Duration then

                  Ada.Text_IO.Put_Line("REFEREE FINISH : Clock "&Integer'Image(Get_Absolute_Counter(Logic_Clock)));

                  -- Puo' accadere tuttavia che qualche task giocatore o palla
                  -- siano in attesa di sblocco della guardia quando la
                  -- condizione di fine match sia verificata. In particolare
                  -- puo' accadere che nell'ultimo iperperiodo il Referee esegua
                  -- prima di un giocatore in attesa di essere riaccodato nella
                  -- entry "Player_Mode_Off_With_Target" o
                  -- "Player_Mode_Off_Without_Target", arrivando da una modalita'
                  -- di gioco "Off". Se il referee terminasse senza accorgimenti
                  -- si giungerebbe ad una situazione di deadlock, pertanto
                  -- questa invocazione permette di rilasciare tutte le guardie.
                  -- Si noti che anche la palla potenzialmente potrebbe non
                  -- terminare, pertanto nella guardia e' espressa la stessa
                  -- condizione di terminazione attraverso il valore del clock
                  -- (vedi guardia).
                  Status_T.Release_Players;

                  End_Match := True;

               else

                  -- Azione del task eseguita in mutua esclusione sullo stato
                  -- globale. Causa potenziale accodamento, quindi prerilascio.
                  Status_T.Referee(Logic_Clock, Shut_Down);

                  -- Avanzamento dell'orologio
                  Tick(Logic_Clock);

                  -- Calcolo del tempo assoluto di risveglio del task.
                  -- In questo caso non si ha drift e neppure riallineamento.
                  Wake_Up := Wake_Up + Sleep_Time;

                  -- Sospensione (punto di prerilascio)
                  delay until Wake_Up;

               end if;

               -- Condizione di terminazione
               exit when End_Match;
            end loop;

            -- Accodamento sulla barriera di fine match
            Status_T.Ready_To_End(Shut_Down);

         end if;

         -- Condizione di terminazione task
         exit when Shut_Down;
      end loop;

   end The_Referee;

end Game.Referee;
