--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

-- Questo packege definisce il tipo del clock logico. Ogni task ne possiede uno,
-- e permette di calcolare l'istante di risveglio, gestire l'allineamento
-- temporale, ed il verificarsi della condizione di terminazione di un match.
-- Si noti che e' incrementato del tempo di sospensione relativo ad ogni task,
-- determinandone cosi' la granularita'. Ad ogni istante i clock di ogni task
-- saranno distanti al piu' della granularita' massima tra tutti i clok, ovvero
-- di un iperperiodo.
package Game.Clock is

   -- Dichiarazione del tipo
   type Logic_Clock_T is private;

   -- Crea un oggetto di tipo oclock logico
   function Create (Value: Positive; MCM: Positive) return Logic_Clock_T;

   -- Ritorna il valore corrente assoluto del clock
   function Get_Absolute_Counter (Clock: Logic_Clock_T) return Natural;

   -- Ritorna 0 se l'istante corrisponde all'iperperiodo
   function Get_Relative_Period (Clock: Logic_Clock_T) return Natural;

   -- Incrementa il clock del valore di sleep con il quale e' stato inizializzato
   procedure Tick (Clock: in out Logic_Clock_T);

   -- Allinea due orologi proporzionalmente alla loro granularita'
   procedure Align (Clock: in out Logic_Clock_T; ToClock: in Logic_Clock_T);

   -- Ritorna il valore con il quale e' incrementato
   function Get_Sleep(Clock : Logic_Clock_T) return Positive;

private

   -- Tick_Counter : valore attuale del contatore
   -- Increment_Value : valore di incremento
   -- MCM_Value rappresenta : MCM tra tutti i tempi di sospensione dei task,
   -- ovvero alla durata dell'iperperiodo
   type Logic_Clock_T is
      record
         Tick_Counter : Natural;
         Increment_Value: Positive;
         MCM_Value: Positive;
      end record;

end Game.Clock;
