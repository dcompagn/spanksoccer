--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Calendar; use Ada.Calendar;

package body Game.Logic.Action is

   function Create_Kick(Player_Id: Player_ID_T; Target_Pos: Position_T; Strenght: Positive) return Action_T is
   begin
      return Create(Kick, Player_Id, (0,0), Target_Pos, Strenght, 1);
   end;

   function Create_Move(Player_Id: Player_ID_T; Displacement : Displacement_T) return Action_T is
   begin
      return Create(Move, Player_Id, Displacement, (0,0), Positive'First, Natural'First);
   end;

   function Create_Tackle(Player_Id : Player_ID_T; Target_Id: Natural) return Action_T is
   begin
      return Create(Tackle, Player_Id, (0,0), (0,0), Positive'First, Target_Id);
   end;

   function Create_Foul (Player_Id : Player_ID_T; Target_Id: Natural; Target_Pos : Position_T) return Action_T is
   begin
      return Create(Foul, Player_Id, (0,0), Target_Pos, Positive'First, Target_Id);
   end;

   function Create_Take_Ball(Player_Id : Player_ID_T) return Action_T is
   begin
      return Create(Take_Ball, Player_Id, (0,0), (0,0), Positive'First, Natural'First);
   end;

   function Create_Replace_Player(Target_Id: Natural) return Action_T is
   begin
      return Create(Replace_Player, Player_ID_T'First, (0,0), (0,0), Positive'First, Target_Id);
   end;


   function Create(Action_Type : Action_Type_T;
                   Player_Id :Player_ID_T;
                   Displacement: Displacement_T;
                   Target_Pos: Position_T;
                   Strenght: Positive;
                   Target_Id: Natural) return Action_T is
   begin
      return (Timestamp => Natural'First, Action_Type => Action_Type,
              Player_Id => Player_Id, Displacement => Displacement,
              Target_Pos => Target_Pos, Strenght => Strenght, Target_Id => Target_Id);
   end Create;

   procedure Set_Data(This : in out Action_T; Timestamp: Natural; Player_Id: Player_ID_T) is
   begin
      This.Timestamp := Timestamp;
      This.Player_Id := Player_Id;
   end Set_Data;

   function Get_Id(This : Action_T) return Player_ID_T is
   begin
      return This.Player_Id;
   end;

   function Get_Action_Type(This: Action_T) return Action_Type_T is
   begin
      return This.Action_Type;
   end Get_Action_Type;

   function Get_Displacement(This: Action_T) return Displacement_T is
   begin
      return This.Displacement;
   end Get_Displacement;

   function Get_Target_Pos(This: Action_T) return Position_T is
   begin
      return This.Target_Pos;
   end Get_Target_Pos;

   function Get_Strenght(This: Action_T) return Positive is
   begin
      return This.Strenght;
   end Get_Strenght;

   function Get_Target_Id(This: Action_T) return Natural is
   begin
      return This.Target_Id;
   end Get_Target_Id;

end Game.Logic.Action;
