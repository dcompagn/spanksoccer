--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

-- Questo package definisce il tipo mossa. Una mossa e' l'espressione di volonta'
-- di un giocatore di modificare il proprio stato in un certo istante.
-- Puo' essere legale o non legale a seconda dello stato corrente e dalle
-- caratteristiche del giocatore, e puo' essere modificata dall'intermediario.
-- In genere il valore di utilita' di una mossa non decresce con il tempo.
-- E' emessa dai giocatori, o dal manager (sostituzioni), per essere validata e
-- attuata dall'intermediario, modificando lo stato di gioco.
package Game.Logic.Action is

   -- Tipologie di mosse
   type Action_Type_T is (Move, Kick, Foul, Tackle, Take_Ball, Replace_Player);

   type Action_T is private;

   -- Crea una mossa di tipo "tiro"
   -- Player_Id : identifica il giocatore
   -- Target_Pos : e' la posizione verso la quale calciare
   -- Strenght : e' la potenza di tiro
   function Create_Kick(Player_Id: Player_ID_T;
                        Target_Pos: Position_T;
                        Strenght: Positive) return Action_T;

   -- Crea una mossa di tipo "movimento"
   -- Player_Id : identifica il giocatore
   -- Displacement : indica lo scostamento dalla posizione attuale
   function Create_Move(Player_Id : Player_ID_T;
                        Displacement : Displacement_T) return Action_T;

   -- Crea una mossa di tipo "contrasto"
   -- Player_Id : identifica il giocatore
   function Create_Tackle(Player_Id : Player_ID_T;
                          Target_Id: Natural) return Action_T;

   -- Crea una mossa di tipo "fallo volontario"
   -- Player_Id : identifica il giocatore
   function Create_Foul(Player_Id : Player_ID_T;
                        Target_Id: Natural;
                        Target_Pos : Position_T) return Action_T;

   -- Crea una mossa di tipo "prendi palla". In questo caso e' stato scelto di
   -- rendere esplicito il fatto che un giocatore voglia acquisire o no la
   -- palla. Naturalmente ha senso soltanto se questa e' in un intorno, definito
   -- dalla variabile Threshold in Game.Logic, del giocatore.
   -- Player_Id : identifica il giocatore
   function Create_Take_Ball(Player_Id : Player_ID_T) return Action_T;

   -- Crea una mossa di tipo "sostituzione giocatore". Per semplicita' non e'
   -- scelto il giocatore che prendera' il posto di quello sostituito, poiche'
   -- non e' stato implementato un pool di riserve.
   -- Target_Id : identifica il giocatore oggetto della sostituzione
   function Create_Replace_Player(Target_Id: Natural) return Action_T;

   -- Permette di impostare alcuni parametri della mossa.
   -- Utilizzata dall'intemediario per garantire la correttezza dei dati,
   -- in caso di Player maliziosi.
   procedure Set_Data(This : in out Action_T;
                      Timestamp: Natural;
                      Player_Id: Player_ID_T);

   -- Ritorna l'Id del giocatore
   function Get_Id(This : Action_T) return Player_ID_T;

   -- Ritorna il tipo azione
   function Get_Action_Type(This: Action_T) return Action_Type_T;

   -- Ritorna lo scostamento
   function Get_Displacement(This: Action_T) return Displacement_T;

   -- Ritorna la posizione target della mossa
   function Get_Target_Pos(This: Action_T) return Position_T;

   -- Ritorna la potenza
   function Get_Strenght(This: Action_T) return Positive;

   -- Ritorna il player target della mossa
   function Get_Target_Id(This: Action_T) return Natural;

private

   -- Il tipo azione contiene tutti i potenziali campi descrittivi di una mossa
   type Action_T is
      record
         Timestamp: Natural;
         Action_Type : Action_Type_T;
         Player_Id: Player_ID_T;
         Displacement: Displacement_T;
         Target_Pos: Position_T;
         Strenght: Positive;
         Target_Id: Natural; -- pu� essere 0
      end record;

   -- Crea un oggetto generico di tipo mossa
   function Create(Action_Type : Action_Type_T;
                   Player_Id : Player_ID_T;
                   Displacement: Displacement_T;
                   Target_Pos: Position_T;
                   Strenght: Positive;
                   Target_Id: Natural) return Action_T;

end Game.Logic.Action;
