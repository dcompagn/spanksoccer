--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

with Ada.Calendar; use Ada.Calendar;
with Game.Clock; use Game.Clock;
with Game.Status; use Game.Status;
with Ada.Text_IO;
with Game.Logic;

package body Game.Ball is

   -- Il comportamento di questo task e' del tutto simile a quello
   -- di un Player, preservandone cosi' le proprieta' di correttezza
   -- e terminazione
   task body The_Ball is

      -- Istante temporale unico e globale di avvio partita
      Start_Time : Time;

      -- Rappresenta l'istante temporale al quale il task sara' re-inserito
      -- nella coda dei pronti
      Wake_Up : Time;

      -- Rappresenta il tempo di sospensione del task in unita', e' utilizzato
      -- per calcolare il tempo di sospensione
      Sleep : Positive := 1;

      -- Rappresenta il tempo di sopensione effettivo con tipo Duration,
      -- calcolato come il rapporto tra Sleep e Sleep_Factor
      Sleep_Time : Standard.Duration :=
        Standard.Duration(Float(Sleep) / Float(Sleep_Factor));

      -- Rappresenta il clock logico, ad ogni turno e' incrementato
      -- o riallineato, e permette di eliminare il drift cumulativo conseguente
      -- al riallineamento. E' utilizzato per determinare il tempo di
      -- sospensione assoluto del task.
      Logic_Clock : Logic_Clock_T;

      -- Valore che indica se la condizione di terminazione del match � stata
      -- raggiunta, permette di uscire dal loop pi� interno
      End_Match : Boolean := False;

      -- Valore che permette di terminare il task istantaneamente, ottenuto
      -- dallo stato globale, True in caso di chiusura del pannello manager
      Shut_Down : Boolean := False;

      -- Non utilzzato, aggiunto per compatibilit� con l'entry del task Broker
      Players_Number : Positive;
   begin

      -- I due loop innestati permettono di riavviare un match qualora sia
      -- giunto a terminazione, simmetricamente a tutti i task
      loop

         End_Match := False;

         -- Inizializzazione del clock logico
         Logic_Clock := Create(Sleep, Hyperperiod_Const);

         -- Il task e' posto in attesa del segnale di start match. Questo e'
         -- un punto di sinconizzazione su Status_T
         Status_T.Waiting_For_Start_Signal_To_Other(Players_Number,
                                                    Start_Time,
                                                    Shut_Down);

         if not Shut_Down then

            loop

               -- La terminazione del match avviene non appena il clock logico
               -- assume un valore superiore alla Match_Duration per garantire
               -- lo sblocco delle guardie, ed il rilascio dei giocatori in
               -- attesa su di esse, da parte del Referee.
               if Shut_Down
                 or Get_Absolute_Counter(Logic_Clock) > Game.Match_Duration then

                  End_Match := True;

                  -- Output concorrente (rischioso)
                  Ada.Text_IO.Put_Line("BALL FINISH : Clock "
                     &Integer'Image(Get_Absolute_Counter(Logic_Clock)));
               else

                  -- Azione del task eseguita in mutua esclusione sullo stato
                  -- globale. Causa potenziale accodamento, quindi prerilascio.
                  Status_T.Ball(Logic_Clock, Shut_Down);

                  -- Avanzamento dell'orologio
                  Tick(Logic_Clock);

                  -- Calcolo dell'istante assoluto di risveglio del task
                  Wake_Up := Start_Time +
                    Standard.Duration(Get_Absolute_Counter(Logic_Clock)) * Sleep_Time;

                  -- Sospensione (punto di prerilascio)
                  delay until Wake_Up;

               end if;

               -- Condizione di terminazione
               exit when End_Match;
            end loop;

            -- Accodamento sulla barriera di fine match
            Status_T.Ready_To_End(Shut_Down);

         end if;

         -- Condizione di terminazione del task
         exit when Shut_Down;
      end loop;

   end The_Ball;

end Game.Ball;
