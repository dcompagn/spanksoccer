--------------------------------------------------------
--  Copyright (C) 2012                                --
--  Davide Compagnin - Mario Leone                    --
--                                                    --
--  SpankSoccer 1.1                                   --
--                                                    --
--  Concurrent and Distributed Systems class project  --
--  Master Degree in Computer Science                 --
--  Academic year 11/12                               --
--  Dept. of Pure and Applied Mathematics             --
--  University of Padua, Italy                        --
--                                                    --
--  This program is free software; you can            --
--  redistribute it and/or modify it under the terms  --
--  of the GNU General Public License as published    --
--  by the Free Software Foundation; either           --
--  version 2 of the License, or (at your option)     --
--  any later version.                                --
--------------------------------------------------------

package body Game is

   function GCD(a, b : Integer) return Integer is
   begin
      if a*b = 0 then
         return 0;
      end if;
      if a = b then
         return a;
      elsif a > b then
         return GCD(a - b, b);
      else
         return GCD(a, b - a);
      end if;
   end GCD;

   function LCM(a, b : Integer) return Integer is
   begin
      if (a = 0) or (b = 0) then
         return 0;
      else
         return Abs(a*b) / GCD(a, b);
      end if;
   end LCM;

end Game;
