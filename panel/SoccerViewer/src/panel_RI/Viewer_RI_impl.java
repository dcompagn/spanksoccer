package panel_RI;


//The Java class Viewer_RIPOA is the skeleton file for the server-side mapping,
//providing basic CORBA functionality for the server


//this class rappresent the servant

public class Viewer_RI_impl extends Viewer_RIPOA{
	
	
	private GameStatus[] buffer = new GameStatus[128];
	private int first, next = 0;
	

	@Override
	public synchronized void  publish_status(GameStatus Status) {
		
		if (!IsFull()) {
			buffer[next] = Status;		
			next = (next + 1) %  buffer.length;
		}
		
		/*chiamato il metodo remoto mi stampa lo stato
		for (Position ps : gs.Players_Position) {
			System.out.print("X=" + ps.X);
			System.out.println(" Y=" + ps.Y);
		}
		
		System.out.println("Posizione referee: (" + gs.Referee_Position.X + gs.Referee_Position.Y +")");
		System.out.println("Posizione ball: (" + gs.Ball_Position.X + gs.Ball_Position.Y +")");
		System.out.println("Timestamp: " + gs.Time); */
		
		
		//System.out.println(next);
		//System.out.println(first);
		//System.out.println("----");
	}
	
	
	public synchronized boolean IsEmpty()
	{
		//System.out.println(next);
		//System.out.println(first);
		//System.out.println(first == next);
		return first == next;
	}
	
	public synchronized boolean IsFull()
	{
		return ((next + 1) % buffer.length) == first; 
	}
	
	
	public synchronized GameStatus getStatus() {
		
		GameStatus temp =  buffer[first];
		first = (first + 1) %  buffer.length;
		return temp;
		
	}

}
