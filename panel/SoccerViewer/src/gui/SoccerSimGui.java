package gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;

import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import panel_RI.Viewer_RI_impl;

public class SoccerSimGui implements Runnable {

	// campi privati
	// JFrame is a window and JPanel is the content

	private FieldPanel panel;
	private Viewer_RI_impl _myservant;
	private control_RI.subscriber_RI obj_subscriber; // questo è l'oggetto
														// remoto che devo
														// chiamare per fare la
														// sottoscrizione
	private int _myID; // questo campo contiene l'id che viene assegnato dal
						// core al pannello
						// durante la sottoscrizione
	JFrame frame;

	public SoccerSimGui(Viewer_RI_impl myservant_in,
			control_RI.subscriber_RI subscr_in, int id) {

		_myservant = myservant_in;
		obj_subscriber = subscr_in;
		_myID = id;

		initializeComponents();

	}

	private void initializeComponents() {
		panel = new FieldPanel();
		frame = new JFrame("SpankSoccer Viewer");

		// serve per far terminare l'applicazione
		// correttamente e fare la unsubscribe al servizio
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					obj_subscriber.unsubcribe(_myID);
				} catch (Exception ex) {
					System.out.println("Unsubscribe");
				}
				System.exit(0);
			}
		});

		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}

	private static String readFile(String file) throws IOException {
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String remoteRegistryIOR = br.readLine();
		br.close();
		fr.close();
		return remoteRegistryIOR;
	}

	public static void main(String[] args) {

		String myIOR;

		// enable anti-aliased text:
		System.setProperty("awt.useSystemAAFontSettings","on");
		
		try {

			// crea l'oggetto remoto (server) in modo che il core possa invocare
			ORB orb = ORB.init(args, null);
			POA rootPOA = POAHelper.narrow(orb
					.resolve_initial_references("RootPOA"));
			rootPOA.the_POAManager().activate();

			Viewer_RI_impl myservant = new Viewer_RI_impl();
			org.omg.CORBA.Object obj = rootPOA.servant_to_reference(myservant);

			// mi genera la stringa IOR
			myIOR = orb.object_to_string(obj);
			System.out.println(myIOR); // da IOR a stringa su stdout

			// questa è la parte client

			// legge l'IOR del core da file
			org.omg.CORBA.Object subscriber_obj = orb.string_to_object(readFile(args[0]));
			// org.omg.CORBA.Object subscriber_obj = orb.string_to_object
			// (readFile("/media/panda/Soccer/IOR_subscriber.txt"));
			// crea l’istanza dello stub dell’oggetto remoto
			// tramite servizio di narrowing reso dall’Helper di Message
			control_RI.subscriber_RI subscriber_proxy = control_RI.subscriber_RIHelper
					.narrow(subscriber_obj);
			// invoca il servizio all'oggetto remoto mandando l'IOR per la
			// sottoscrizione
			int id_panel = subscriber_proxy.subscribe(myIOR);
			// fine parte client
			System.out.println("Id assegnatomi: " + id_panel);
			Runnable paintThread = new SoccerSimGui(myservant,
					subscriber_proxy, id_panel);
			Thread t1 = new Thread(paintThread);
			t1.start();

			orb.run();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {

		while (true) {
			if (!_myservant.IsEmpty()) {

				panel.updateField(_myservant.getStatus());
				frame.repaint();
			}
			try {
				Thread.sleep(10); // senza questo delay non mi va
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
