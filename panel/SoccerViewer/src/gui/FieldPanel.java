package gui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.*;
import java.io.IOException;
import javax.imageio.ImageIO;


import java.net.URL;

public class FieldPanel extends JPanel {

	/**
	 * campo serialVersionUID autogenerato per evitare il warning
	 */
	private static final long serialVersionUID = 1L;

	private Image fieldImage;
	private Image ballImage;
	private Image eastPlayerImage;
	private Image westPlayerImage;
	private Image refereeImage;
	private Image tooltipImage;
	private Image greenImage;
	private Image yellowImage;
	private Image redImage;
	private JLabel score_t1;
	private JLabel score_t2;
	private JLabel time;
	private JLabel game_status;
	
	private JLabel t1;
	private JLabel t2;
	
	private JLabel pLabels[] = new JLabel[22];
	
	private JPanel stat;
	private JPanel playerInfo;

	private panel_RI.GameStatus status=null;
	

	public FieldPanel() {
		getImages(); // popola i campi dati con le immagini
		initializeProperties(); // inizializza le dimensioni del pannello
	}

	private void initializeProperties() {
		
		
		this.setLayout(new BorderLayout());
		
		stat =  new JPanel(new GridLayout(0,4));
		playerInfo = new JPanel(new GridLayout(24,1));	
		time = new JLabel("Tempo: ");
		score_t1 = new JLabel("Team1: ");
		score_t2 = new JLabel("Team2: ");
		game_status = new JLabel("Stato di gioco: ");
		
		stat.add(score_t1);
		stat.add(score_t2);
		stat.add(time);
		stat.add(game_status);
		
		t1 = new JLabel("Team 1"); playerInfo.add(t1);
		
		for (int i = 0; i < pLabels.length; i++) {
			if (i % 2 == 0) {  
				pLabels[i] = new JLabel("");
				playerInfo.add(pLabels[i]); 
			}
		};
		
		t2 = new JLabel("Team 2"); playerInfo.add(t2);
		
		for (int i = 0; i < pLabels.length; i++) {
			if (i % 2 != 0) {   
				pLabels[i] = new JLabel("");		    	
				playerInfo.add(pLabels[i]);
		    	
			}
		};		
		
		playerInfo.setAlignmentX(Component.LEFT_ALIGNMENT);
		playerInfo.setBorder(BorderFactory.createTitledBorder("Stat"));
		playerInfo.setPreferredSize(new Dimension(140,fieldImage.getHeight(null)));
		
		this.add(stat, BorderLayout.SOUTH);
		this.add(playerInfo, BorderLayout.EAST);
		
	    Dimension size = new Dimension(fieldImage.getWidth(null)+140, fieldImage.getHeight(null)+20);
		setPreferredSize(size);
		//setMinimumSize(size);
		//setMaximumSize(size);
		setSize(size);
		//setLayout(null);
		
	}

	private void getImages() {
		try {
			fieldImage = ImageIO.read(new URL(getClass().getResource("images/field.png"), "field.png"));
			ballImage = ImageIO.read(new URL(getClass().getResource("images/ball.png"), "ball.png"));
			eastPlayerImage = ImageIO.read(new URL(getClass().getResource("images/eastPlayer.png"), "eastPlayer.png"));
			westPlayerImage = ImageIO.read(new URL(getClass().getResource("images/westPlayer.png"), "westPlayer.png"));
			refereeImage = ImageIO.read(new URL(getClass().getResource("images/referee.png"), "referee.png"));
			tooltipImage = ImageIO.read(new URL(getClass().getResource("images/tooltip.png"), "tooltip.png"));
			greenImage = ImageIO.read(new URL(getClass().getResource("images/green.png"), "green.png"));
			yellowImage = ImageIO.read(new URL(getClass().getResource("images/yellow.png"), "yellow.png"));
			redImage = ImageIO.read(new URL(getClass().getResource("images/red.png"), "red.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	 public void paintComponent(Graphics g) {  //metodo che disegna l' UI
	        g.drawImage(fieldImage, 0, 0, null);
	        
	        if (this.status!=null)
	        {		        
		        drawPlayers(g);
		        drawReferee(g);
		        drawBall(g);
		        drawStat();
	        }
	    }

	 private void drawStat() {
		time.setText("Tempo: " + Integer.toString(status.Time) + "/ " + Integer.toString(status.Duration));
		score_t1.setText("Team1: " + Integer.toString(status.Score_team_1));
		score_t2.setText("Team2: " + Integer.toString(status.Score_team_2));
		if (status.Control == 0)
			game_status.setText("Stato di gioco: Normale");
		else if (status.Control == 1)
				game_status.setText("Stato di gioco: Fallo avvenuto");
		else if (status.Control == 2) 
				game_status.setText("Stato di gioco: Goal avvenuto");
		else if (status.Control == 3) 
				game_status.setText("Stato di gioco: Out avvenuto");
		else game_status.setText("Stato di gioco: Error");
    	for(int i=0;i<status.Players_Position.length;i++){
    		pLabels[i].setText(String.valueOf("ID: " + status.Players_Position[i].ID) + ", F: " + String.valueOf(status.Players_Position[i].Foul)+", D:"+String.valueOf(status.Players_Position[i].Distance).substring(0, String.valueOf(status.Players_Position[i].Distance).indexOf('.')));
    	}
	 }

	 
	 private void drawBall(Graphics g) {
	      g.drawImage(ballImage,status.Ball_Position.X,status.Ball_Position.Y,null);
	        
	    }
	    
	    private void drawReferee(Graphics g) {
	    	g.drawImage(refereeImage,status.Referee_Position.X,status.Referee_Position.Y,null);
	    }
	    
	    
	    private void drawPlayers(Graphics g) {
	    	int number_of_players = status.Players_Position.length;
	    	for(int i=0;i<number_of_players;i++){
	    		if (i % 2 == 0) 
		    		g.drawImage(westPlayerImage, status.Players_Position[i].Player_Position.X, status.Players_Position[i].Player_Position.Y - 10, null);
		    		else
		    		g.drawImage(eastPlayerImage, status.Players_Position[i].Player_Position.X, status.Players_Position[i].Player_Position.Y - 10, null);
	    		g.drawImage(tooltipImage, status.Players_Position[i].Player_Position.X - 9, status.Players_Position[i].Player_Position.Y - 32, null);
	    		Graphics2D gL = (Graphics2D)g;
	    	    gL.setFont(new Font("Sans", Font.BOLD, 9)); 
	    	    gL.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    	    gL.setColor(Color.white);
	    		//gL.drawString(String.valueOf(status.Players_Position[i].ID)+","+String.valueOf(status.Players_Position[i].Name).trim().substring(0, 2)+","+String.valueOf(status.Players_Position[i].Energy), status.Players_Position[i].Player_Position.X - 8, status.Players_Position[i].Player_Position.Y - 22);
	    		gL.drawString(String.valueOf(status.Players_Position[i].ID)+","+String.valueOf(status.Players_Position[i].Name).trim().substring(0, Math.min(3,String.valueOf(status.Players_Position[i].Name).trim().length())), status.Players_Position[i].Player_Position.X - 8, status.Players_Position[i].Player_Position.Y - 22);
	    		if (status.Players_Position[i].Energy > 50)
	    			gL.drawImage(greenImage, status.Players_Position[i].Player_Position.X + 26, status.Players_Position[i].Player_Position.Y - 31, null);
	    		else
	    			if (status.Players_Position[i].Energy > 15)
	    				gL.drawImage(yellowImage, status.Players_Position[i].Player_Position.X + 26, status.Players_Position[i].Player_Position.Y - 31, null);	    			
	    			else
	    				gL.drawImage(redImage, status.Players_Position[i].Player_Position.X + 26, status.Players_Position[i].Player_Position.Y - 31, null);
	    	}
	    }
	    
	    public void updateField(panel_RI.GameStatus status_in) {
	        this.status = status_in; 
	    }
}
