package UI;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;


public class Soccer_Manager_UI {
	
	private JPanel mainPanel;
    private JPanel buttonPanel;
    private JButton startButton;
    private JLabel westTeamScore;
    private JButton sostituisciButton;
    private JPanel chooseTeamPanel;
    private JComboBox numberOfPlayerComboBox;
    private JComboBox formationComboBox;
    private JComboBox substitutionComboBox;
    private JTextArea loggerTextField;
    
    
    private int formation=0;
    private int numbers_of_players =0;
    private int id_player_substitution=0;
    
    private String IORcore;
    control_RI.manager_RI remote_obj;
    private org.omg.CORBA.ORB orb;
    private org.omg.CORBA.Object obj;
    
    private JFrame mainframe; // come campo privato perche lo creo nel main
    							// e lo passo al costruttore perche attacco l'eventlistener
    
    private boolean gamestart=false;
    
    

	
	public Soccer_Manager_UI(control_RI.manager_RI remote_obj, JFrame mainframe_in )
	{
		this.remote_obj = remote_obj;
		mainframe = mainframe_in;
		
		initializeComponents();
        addActionListeners();
        initializeComboBoxes();
        
	}
	
	
	
	private void initializeComboBoxes() {

		
		

        String[] n_players = {"5", "7", "11"};
        numberOfPlayerComboBox.addItem(n_players[0]);
        numberOfPlayerComboBox.addItem(n_players[1]);
        numberOfPlayerComboBox.addItem(n_players[2]);
        numberOfPlayerComboBox.setSelectedIndex(0);
        numbers_of_players = Integer.parseInt(numberOfPlayerComboBox.getSelectedItem().toString());
        

        String[] modulus = {"Standard", "Aggressiva", "Difensiva"};
        formationComboBox.addItem(modulus[0]);
        formationComboBox.addItem(modulus[1]);
        formationComboBox.addItem(modulus[2]);
        formationComboBox.setSelectedIndex(0);
        formation = formationComboBox.getSelectedIndex();
        
	}

	private void addActionListeners() {
        startButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
            	
            	//scrive sulla casella dei log
            	loggerTextField.append("Premuto Start \n");               
            	try {                	              

        			if (gamestart = remote_obj.init_game(numbers_of_players, formation))
        			{
        				//Thread.sleep(2000);
        				//se la chiamata va a buon fine .. cioè ritorna true
        				//fa parire il gioco
        				//remote_obj.start(); 
        				
                    	//disabilita il tasto start stesso e le impostazioni iniziali
                    	
                    	//startButton.setEnabled(false);
                    	//numberOfPlayerComboBox.setEnabled(false);
                    	//formationComboBox.setEnabled(false);
                    	
                    	//abilito il tasto sostituzioni
                    	 sostituisciButton.setEnabled(true);
                         
                         //abilito la combobox sostituzioni con il numero di giocatori
                         for (int i=1; i<22+1; i++) {
                         	substitutionComboBox.addItem(String.valueOf(i));
                         }
                         
                         substitutionComboBox.setEnabled(true);
                         substitutionComboBox.setSelectedIndex(0);
                         id_player_substitution = substitutionComboBox.getSelectedIndex()+1;
                    	
        			}
        			
        			else 
        			{
        				loggerTextField.append("init_game() call failed \n");
        			}
        			
        		} 
                catch (Exception e1) { 
                	loggerTextField.append("Error \n");
        		}
                
            }
        });


        numberOfPlayerComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	 numbers_of_players = Integer.parseInt(numberOfPlayerComboBox.getSelectedItem().toString());
            	 System.out.println("cambiato numero di giocatori");
            }
        });

        formationComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	formation = formationComboBox.getSelectedIndex();
            	 System.out.println("cambiata formazione");
            }
        });

        substitutionComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	id_player_substitution = substitutionComboBox.getSelectedIndex()+1;
            	 System.out.println("cambiata cmbbox sostituzione" + String.valueOf(id_player_substitution) );
            }
        });
        
        sostituisciButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	loggerTextField.append("chiamata sostituzione");
            	 
            	 
            	 //chimata remota per sostituzione
            	 try {
					remote_obj.substitute(id_player_substitution);
				} catch (Exception ex) {
					loggerTextField.append("Substitution error...");
				}
            }
        });
        
        mainframe.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(WindowEvent winEvt) {
            	if (remote_obj!= null) {
	            	try {
						remote_obj.shutdown();
					} catch (Exception ex) {
						loggerTextField.append("Shutdown error...");
					}
	            	finally {System.exit(0); }
	            }            	            
            	else System.exit(0);
            }
        });
    }

	
	private static String readFile(String file ) throws IOException {
		 FileReader fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);
         String remoteRegistryIOR = br.readLine();
         br.close();
         fr.close();
         return remoteRegistryIOR;
	 }

	
	private void initializeComponents() {
		
		GridLayout layout = new GridLayout();
    	layout.setRows(1);
    	layout.setColumns(1);
    	layout.setHgap(-1);
    	layout.setVgap(-1);
    	mainPanel = new JPanel();
    	
    	mainPanel.setLayout(layout);
    	
    	mainPanel.setBounds(20, 20, 804, 540);
    	mainPanel.setEnabled(true);
    	
    
    	
    	
    	// setup Button Panel (middle Panel)
    	FlowLayout buttonPanelLayout = new FlowLayout(FlowLayout.CENTER);
    	buttonPanelLayout.setHgap(10);
    	buttonPanelLayout.setVgap(5);
    	
    	buttonPanel = new JPanel(buttonPanelLayout);
    	
    	chooseTeamPanel = new JPanel(buttonPanelLayout);
    	
    	westTeamScore = new JLabel("Formazione iniziale: ");
    	buttonPanel.add(westTeamScore);
    	
    	formationComboBox= new JComboBox();
    	buttonPanel.add(formationComboBox);
    	
    	
    	JLabel chooseEast = new JLabel("Numero di giocatori");
    	buttonPanel.add(chooseEast);
    	
    	numberOfPlayerComboBox = new JComboBox();
    	buttonPanel.add(numberOfPlayerComboBox);
    	
    	startButton = new JButton("Start");
    	buttonPanel.add(startButton);
    	

    	// setup bottom Panel 
    	chooseTeamPanel = new JPanel(buttonPanelLayout);
    	
    	
    	
    	JLabel chooseWest = new JLabel("Sostituzioni: ");
    	chooseTeamPanel.add(chooseWest);
    	
    	substitutionComboBox = new JComboBox();
    	substitutionComboBox.setEnabled(false);
    	chooseTeamPanel.add(substitutionComboBox);
    	
    	
    	
    	sostituisciButton = new JButton("Sostituisci");
    	chooseTeamPanel.add(sostituisciButton);
    	sostituisciButton.setEnabled(false);
    	
    	    	
    	
    	JSplitPane buttonSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, buttonPanel, chooseTeamPanel);
    	
    	loggerTextField = new JTextArea();
    	loggerTextField.setEditable(false);
    
    
    	
    	JSplitPane mainSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, buttonSplit, loggerTextField);
    	
    	mainSplit.setDividerSize(0);
    	mainSplit.setOrientation(0);
    	mainSplit.setEnabled(false);
    	
    	mainPanel.add(mainSplit);
    	

	}
	
	public static void main(String[] args) {
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init (args,null);
		
		//chiamata remota tramite l'oggetto remoto remote_obj
        try {
        	
        	//lego il file contenente IOR del core
        	String IORcore = readFile(args[0]);
        	//String IORcore = readFile("/home/davide/Documenti/SCD/Soccer/IOR_manager.txt");
        	//String IORcore = readFile("/media/panda/Soccer/IOR_manager.txt");
        	org.omg.CORBA.Object obj = orb.string_to_object(IORcore);
        	control_RI.manager_RI remote_obj = control_RI.manager_RIHelper.narrow(obj);
        	
        	JFrame frame = new JFrame("SpankSoccer Manager");
    		Soccer_Manager_UI sc = new Soccer_Manager_UI(remote_obj,frame);
            frame.setContentPane(sc.mainPanel);
            
            frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // alla chiusura fa gestire l'evento
            //all'actionlistener
            frame.pack();
            frame.setVisible(true); 
        	
		} catch (IOException e2) {
			System.out.println("File reading error \n");
		}
        catch (Exception e1) { 
        	System.out.println("Error \n");
		}
		
		
		
	}
	
}
