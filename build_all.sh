#!/bin/bash
echo Building CORBA remote interfaces..
iac -c -s idl/panel.idl -o src/distr/panel_RI/
iac -c -s idl/control.idl -o src/distr/control_RI/
idlj -f all -td panel/SoccerViewer/src/ idl/panel.idl
idlj -td panel/SoccerViewer/src/ idl/control.idl
idlj -td panel/soccer_manager/ idl/control.idl

echo Building Core..
mkdir obj
gnatmake -P soccer.gpr

echo Building panel Manager..
mkdir panel/soccer_manager/bin
cd panel/soccer_manager
javac -Xlint:unchecked UI/Soccer_Manager_UI.java -sourcepath . -d bin

cd ..

echo Building panel Viewer..
cd SoccerViewer
mkdir bin
javac -Xlint:unchecked src/gui/SoccerSimGui.java -sourcepath src -d bin
mkdir bin/gui/images
cp src/gui/images/* bin/gui/images

cd ../..

echo All Done
