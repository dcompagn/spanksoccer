\chapter{Questioni di concorrenza e distribuzione}

\section{Analisi del problema}


Una partita di calcio è una realtà intrinsecamente
parallela.
Il problema proposto si addice perfettamente ad essere modellato usando
delle tecniche di programmazione concorrente e distribuita, quindi la scelta
è ricaduta sull'uso di un linguaggio di programmazione
che supporta direttamente la concorrenza in quanto permette di esprimere
in maniera più naturale problemi come quello di simulare una partita di calcio.

Questo capitolo ha il compito di illustrare alcune problematiche di concorrenza
e distribuzione più importante in cui ci siamo imbattuti.


\subsection{Competizione fra giocatori}
\label{corsa_risorsa}
La questione più naturale che nasce simulando una partita
di calcio è la competizione fra giocatori, per esempio, per prendere la palla o per arrivare prima 
in una determinata posizione. Sarebbe gradibile avere anche durante la simulazione
un grado di non determinismo desiderabile che renda interessante la corsa verso
una risorsa.

Il primo problema da affrontare è quindi riuscire a modellare
questa situazione di \textit{corsa} verso una risorsa e sappiamo
bene che attraverso la programmazione di task concorrenti questa
situazione è facilmente raggiungibile.





\subsection{Azioni dei giocatori e parallelismo reale}
Nella realtà, in un particolare istante,
le parti coinvolte nella partita di calcio prendono delle decisioni
in maniera parallela guardando lo stato del gioco.

La programmazione concorrente è lo strumento quindi per raggiungere
il parallelismo desiderato in questa situazione.



\subsection{Gestione di eventi di gioco particolari}
\label{eventi_particolari}

Durante la partita possono verificarsi eventi di gioco particolari che dovrebbero
essere gestiti.
Con eventi particolari intendiamo ad esempio al fischio di un fallo da parte dell'arbitro,
un fuorigioco, una rimessa laterale o un goal. Questi sono tutti eventi particolari
che alterano lo stato normale di gioco.
Gestire questi eventi porta a delle problematiche non di poco conto. 
Sotto verranno illustrati alcuni esempi di problemi e alcune possibile soluzioni
che possono essere adottate.

\begin{ex}

Si pensi alla situazione in cui l'arbitro fischi un fallo oppure un fuorigioco
oppure una rimessa laterale. Tipicamente il gioco riprenderà se e solo se un qualsiasi
giocatore, diciamo A, rispettivamente batterà il fallo, il fuorigioco o la rimessa laterale.
Quindi è presente, in alcuni casi, almeno un momento in cui i giocatori, escluso A,
aspetteranno la ripresa del gioco, ovvero attendono un azione da parte di A.
\end{ex}

La domanda che nasce spontanea è se i giocatori debbano comunque
eseguire (contendendo la CPU) durante l'attesa, in questo caso effettuando una mossa "nulla",
causando il problema del polling, oppure bloccarsi attendendo la ripresa della partita.
Sono possibili quindi queste due soluzioni tra le quali quella desiderabile sarebbe la seconda.


\begin{ex}
Si pensi alla situazione in cui si fischia un fallo. Il giocatore incaricato di batterlo
deve aspettare il fischio di ripresa dell'arbitro. L'arbitro normalmente
attenderà che i giocatori della squadra avversaria siano pronti, ad esempio sistemino
la barriera. Solo dopo il giocatore potrà battere il fallo.
\end{ex}

Le situazioni illustrate negli esempi dovrebbero quindi essere modellate, ed inoltre
il pooling dovrebbe essere reso minimo nella simulazione.

In entrambi gli esempi si noti, riteniamo desiderabile non designare a priori 
un particolare giocatore con l'incarico di rimettere la palla in gioco, ma questo
dovrebbe essere scelto in maniera casuale.


\subsection{Gestione del tempo}
Il tempo è forse uno dei fattori più caratterizzanti in un sistema concorrente. Su di esso devono
essere definite delle invarianti fondamentali  atte a garantire la correttezza della simulazione.

Illustriamo adesso alcuni problemi che abbiamo incontrato durante la modellazione.

Il primo problema è stato quello del drift temporale. Ogni sospensione del task introduce 
potenzialmente una piccola quantità di jitter causata da diversi fattori, 
alcuni dei quali intrinseci ed irrimovibili ad esempio la precisione del clock macchina,
altri evitabili come il tempo di computazione che intercorre tra due sospensioni, 
il tempo d’attesa dovuto alla coda di risorsa protetta, ecc..

In tutti questi casi i tempi di risveglio dei task avrebbero subito un drift dovuto appunto 
all’accumulo di drift causando un effetto indesiderato il quale non ci avrebbe garantito in modo deterministico il comportamento corretto della simulazione.


Un altro problema che ci siamo posti è stato simulare le diverse abilità dei giocatori 
facendo uso di tempi di sospensione diversi. Questa scelta da un lato ha permesso di 
ridurre sensibilmente la quantità di codice dall’altro ha complicato 
la ricerca di invarianti, come sarà mostrato nella sezione legata alla correttezza.

Infine è emerso un altro problema causato dalla riduzione dell’attesa attiva, 
noto anche come polling dei task descritto nella sezione \ref{eventi_particolari}.
La scelta di sospendere l’esecuzione di alcuni task in alcuni stati di gioco 
ha introdotto non solo il problema di calcolare correttamente i tempi di risveglio,
riallineando opportunamente il riferimento temporale di ogni singolo task coinvolto,
ma anche quello di garantire che non venisse introdotto indirettamente alcun 
drift causato dal riallineamento.
Durante l’analisi spiegheremo dettagliatemente come sono stati risolti, 
in modo elegante, questi problemi.


\subsection{Calcolo delle mosse dei giocatori}
\label{mosse_parallele}

Il problema di calcolo delle mosse dei giocatori è stato visto come un problema
da risolvere sfruttando tecniche di intelligenza artificiale. Abbiamo ritenuto
che questa parte si allontanasse dall'essere categorizzata come una sfida per un 
corso di Sistemi Concorrenti e Distribuiti.

Quello invece che rientra nella categoria sfide di Concorrenza è la possibilità
desiderabile che il calcolo della mossa avenisse in maniera concorrente fra più giocatori,
quindi potenzialmente parallelizzabile o addirittura distribuibile (si veda \ref{distribuzione}).

Si pensi al caso in cui si fa uso pesante di intelligenza artificiale. Un calcolo
di una mossa potrebbe risultare lungo in termini di computazione e quindi parallelizzare
il lavoro sembrerebbe desiderabile.

Inoltre tale questione è un problema centrale in termini di possibilità di espansione
del sistema. Un possibile utente che voglia scrivere l'intelligenza per la propria
squadra non dovrebbe essere privato di questa possibilità.


\subsection{Distribuzione del sistema}
\label{distribuzione}


Da quanto si evince nella sezione \ref{Specifiche} il sistema deve avere caratteristiche di 
distribuzione.

Durante lo studio di queste problematiche sono emersi diversi problemi come ad esempio 
quale parte del sistema distribuire.

Il minimo requisito del sistema in termini di distribuzione
in un certo senso è stato fissato dal docente, richiedendo che la minima sfida 
da affrontare dovrebbe essere quella di prevedere almeno la distribuzione
della parte di visualizzazione.

Quando si progetta un sistema distribuito alcune dei vantaggi ovvi che dovrebbero
emergere sono la scalabilità e la trasparenza del sistema distribuito:
E' necessario usare un middleware di distribuzione oppure il problema può essere 
risolto in altro modo?

Nel nostro sistema 
ci siamo soffermati particolarmente nella questione di scalabilità in termini di dimensioni,
ovvero la possibilità che il sistema sia scalabile in numero di panelli di visualizzazione.


Abbiamo affrontato il problema sul quale fosse il modo più pulito
per prevedere la scalabilità in questo senso, quali pattern architetturali
potevano venire incontro alle nostre esigenze.

Discuteremo sul fatto se è direttamente il core di gioco a preoccuparsi 
di inviare le azioni ai pannelli distribuiti o se è necessaria l'introduzione
di altre entità.

L'introduzione di tali entità porta conseguentemente implicazioni in termini di concorrenza
nel luogo in cui avviene la simulazione. Per questo è importante studiare
alcune problematiche e soluzioni in termini di distribuzione per poi 
procedere allo studiodelle  problematiche di concorrenza introdotte e nuove.


Un altro problema è legato alla questione di distribuzione della parte visuale
del match in concomitanza con la gestione di azioni di gioco particolari.
Ciò viene illustrato con un esempio:


\begin{ex}
Si pensi al caso in cui si gestiscano gli eventi particolari descritti in \ref{eventi_particolari}.
In alcuni istanti il sistema potrebbe trovarsi in uno stato particolare
in cui nessuna mossa viene effettuata. Potrebbe essere desiderabile che finché
la simulazione non ritorni in uno stato "normale" non avvenga nessun tipo di comunicazione.
Quindi dovrebbe essere il pannello di visualizzazione a chiedere la partita al core o viceversa?
Se si adottasse il secondo caso il core sarebbe autonomamente a conoscenza dello stato
della simulazione e potrebbe comportarsi di conseguenza.
\end{ex}

Infine nel corso della relazione verrà anche discusso come poter distribuire
il calcolo delle mosse di gioco e i problemi che ne derivano di conseguenza.



