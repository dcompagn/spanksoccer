#!/bin/bash
echo Starting Core..
xterm -e "obj/main" &

cd panel/soccer_manager/bin
xterm -e "java UI.Soccer_Manager_UI ../../../IOR_manager.txt" &

cd ../../
cd SoccerViewer/bin
xterm -e "java gui.SoccerSimGui ../../../IOR_subscriber.txt" &

echo Ready
